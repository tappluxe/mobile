package tappluxe.b28.action.aws.com.tappluxeapplication.validators;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by benjo.uriarte on 2/21/2017.
 */
public class EmailValidatorTest {

    @Test
    public void isValid_correct_all() throws Exception {
        assertTrue("e@e.e, true", true);
    }

    @Test
    public void isValid_incorrect_empty() throws Exception {
        assertFalse("", false);
    }

    @Test
    public void isValid_incorrect_noCharsBeforeAtSign() throws Exception {
        assertFalse("@e.e", false);
    }

    @Test
    public void isValid_incorrect_noAtSign() throws Exception {
        assertFalse("e.e", false);
    }

    @Test
    public void isValid_incorrect_noCharsAfterAtSign() throws Exception {
        assertFalse("e@.e", false);
    }

    @Test
    public void isValid_incorrect_noDotSign() throws Exception {
        assertFalse("e@e", false);
    }

    @Test
    public void isValid_incorrect_noCharsAfterDotSign() throws Exception {
        assertFalse("e@e.", false);
    }
}