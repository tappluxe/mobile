package tappluxe.b28.action.aws.com.tappluxeapplication.ui.device;

import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by benjo.uriarte on 3/22/2017.
 */
public class TurnedOnElapsedTimeCalculatorTest {
	TurnedOnElapsedTimeCalculator turnedOnElapsedTimeCalculator;

	@Test
	public void testAMandAM() throws Exception {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm a");
		Date startDate = format.parse("2017-03-20 02:30 AM");
		Date endDate = format.parse("2017-03-22 04:46 AM");

		turnedOnElapsedTimeCalculator = new TurnedOnElapsedTimeCalculator(startDate, endDate);

		assertThat(turnedOnElapsedTimeCalculator.getTurnedOnElapsedTime(),
				is("Turned on for about 2 days, 2 hours, 16 minutes"));
	}

	@Test
	public void testAMandPM() throws Exception {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm a");
		Date startDate = format.parse("2017-03-01 02:30 AM");
		Date endDate = format.parse("2017-03-22 04:46 PM");

		turnedOnElapsedTimeCalculator = new TurnedOnElapsedTimeCalculator(startDate, endDate);

		assertThat(turnedOnElapsedTimeCalculator.getTurnedOnElapsedTime(),
				is("Turned on for about 21 days, 14 hours, 16 minutes"));
	}

	@Test
	public void testPMandPM() throws Exception {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm a");
		Date startDate = format.parse("2017-03-16 02:30 PM");
		Date endDate = format.parse("2017-03-22 04:46 PM");

		turnedOnElapsedTimeCalculator = new TurnedOnElapsedTimeCalculator(startDate, endDate);

		assertThat(turnedOnElapsedTimeCalculator.getTurnedOnElapsedTime(),
				is("Turned on for about 6 days, 2 hours, 16 minutes"));
	}

	@Test
	public void testPMandAM() throws Exception {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm a");
		Date startDate = format.parse("2017-03-16 02:30 PM");
		Date endDate = format.parse("2017-03-22 04:46 AM");

		turnedOnElapsedTimeCalculator = new TurnedOnElapsedTimeCalculator(startDate, endDate);

		assertThat(turnedOnElapsedTimeCalculator.getTurnedOnElapsedTime(),
				is("Turned on for about 5 days, 14 hours, 16 minutes"));
	}

	@Test
	public void testInvalidTurnedOnDate() throws Exception {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm a");
		Date startDate = format.parse("2017-03-22 02:30 PM");
		Date endDate = format.parse("2017-03-22 04:46 AM");

		turnedOnElapsedTimeCalculator = new TurnedOnElapsedTimeCalculator(startDate, endDate);

		assertThat(turnedOnElapsedTimeCalculator.getTurnedOnElapsedTime(),
				is("Invalid input. Turned on date is still in the future."));
	}

	@Test
	public void testDay() throws Exception {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm a");
		Date startDate = format.parse("2017-03-21 02:30 PM");
		Date endDate = format.parse("2017-03-22 04:46 PM");

		turnedOnElapsedTimeCalculator = new TurnedOnElapsedTimeCalculator(startDate, endDate);

		assertThat(turnedOnElapsedTimeCalculator.getTurnedOnElapsedTime(),
				is("Turned on for about 1 day, 2 hours, 16 minutes"));
	}

	@Test
	public void testHour() throws Exception {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm a");
		Date startDate = format.parse("2017-03-21 03:30 PM");
		Date endDate = format.parse("2017-03-22 04:46 PM");

		turnedOnElapsedTimeCalculator = new TurnedOnElapsedTimeCalculator(startDate, endDate);

		assertThat(turnedOnElapsedTimeCalculator.getTurnedOnElapsedTime(),
				is("Turned on for about 1 day, 1 hour, 16 minutes"));
	}

	@Test
	public void testMinute() throws Exception {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm a");
		Date startDate = format.parse("2017-03-21 03:45 PM");
		Date endDate = format.parse("2017-03-22 04:46 PM");

		turnedOnElapsedTimeCalculator = new TurnedOnElapsedTimeCalculator(startDate, endDate);

		assertThat(turnedOnElapsedTimeCalculator.getTurnedOnElapsedTime(),
				is("Turned on for about 1 day, 1 hour, 1 minute"));
	}

	@Test
	public void testNoDay() throws Exception {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm a");
		Date startDate = format.parse("2017-03-22 03:45 PM");
		Date endDate = format.parse("2017-03-22 04:46 PM");

		turnedOnElapsedTimeCalculator = new TurnedOnElapsedTimeCalculator(startDate, endDate);

		assertThat(turnedOnElapsedTimeCalculator.getTurnedOnElapsedTime(),
				is("Turned on for about 1 hour, 1 minute"));
	}

	@Test
	public void testNoDayAndHour() throws Exception {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm a");
		Date startDate = format.parse("2017-03-22 04:44 PM");
		Date endDate = format.parse("2017-03-22 04:46 PM");

		turnedOnElapsedTimeCalculator = new TurnedOnElapsedTimeCalculator(startDate, endDate);

		assertThat(turnedOnElapsedTimeCalculator.getTurnedOnElapsedTime(),
				is("Turned on for about 2 minutes"));
	}

	@Test
	public void testLessThanOneMinute() throws Exception {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm a");
		Date startDate = format.parse("2017-03-22 04:46 PM");
		Date endDate = format.parse("2017-03-22 04:46 PM");

		turnedOnElapsedTimeCalculator = new TurnedOnElapsedTimeCalculator(startDate, endDate);

		assertThat(turnedOnElapsedTimeCalculator.getTurnedOnElapsedTime(),
				is("Turned on for less than 1 minute"));
	}
}