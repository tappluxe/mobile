package tappluxe.b28.action.aws.com.tappluxeapplication.validators;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

/**
 * Created by benjo.uriarte on 2/21/2017.
 */
public class PasswordValidatorTest {
    PasswordValidator pv;

    @Test
    public void validate_correct() throws Exception {
        pv = new PasswordValidator();
        assertThat(pv.isValid("Awsys+123"), is(true));
    }

    @Test
    public void validate_incorrect_empty() throws Exception {
        pv = new PasswordValidator();

        assertThat(pv.isValid(""), is(false));
    }

    @Test
    public void validate_incorrect_lessThanEight() throws Exception {
        pv = new PasswordValidator();

        assertThat(pv.isValid("abcd567"), is(false));
    }

    @Test
    public void validate_incorrect_moreThanSixteen() throws Exception {
        pv = new PasswordValidator();

        assertThat(pv.isValid("abcd5678efgh34567"), is(false));
    }

    @Test
    public void validate_incorrect_noLowercase() throws Exception {
        pv = new PasswordValidator();

        assertThat(pv.isValid("AWSYS+123"), is(false));
    }

    @Test
    public void validate_incorrect_noUppercase() throws Exception {
        pv = new PasswordValidator();

        assertThat(pv.isValid("awsys+123"), is(false));
    }

    @Test
    public void validate_incorrect_noDigit() throws Exception {
        pv = new PasswordValidator();
        assertThat(pv.isValid("Awsys+ab"), is(false));
    }

    @Test
    public void validate_incorrect_noSpecialChar() throws Exception {
        pv = new PasswordValidator();
        assertThat(pv.isValid("Awsys1234"), is(false));
    }
}