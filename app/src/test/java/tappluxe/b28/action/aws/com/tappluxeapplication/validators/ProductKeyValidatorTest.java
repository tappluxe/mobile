package tappluxe.b28.action.aws.com.tappluxeapplication.validators;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

/**
 * Created by benjo.uriarte on 2/21/2017.
 */
public class ProductKeyValidatorTest {
    ProductKeyValidator pkv;

    @Test
    public void isValid_correct() throws Exception {
        pkv = new ProductKeyValidator();

        assertThat(pkv.isValid("637be8fa-692c-4a2f-a504-134628468c8e"), is(true));
    }

    @Test
    public void isValid_incorrect_empty() throws Exception {
        pkv = new ProductKeyValidator();

        assertThat(pkv.isValid(""), is(false));
    }

    @Test
    public void isValid_incorrect_otherSpecChar() throws Exception {
        pkv = new ProductKeyValidator();

        assertThat(pkv.isValid("abcdefa8*abc4-abc4-abc4-abcdefabcde2"), is(false));
    }

    @Test
    public void isValid_incorrect_firstSegMore_withOutSpaces() throws Exception {
        pkv = new ProductKeyValidator();

        assertThat(pkv.isValid("abcdefa89-abc4-abc4-abc4-abcdefabcde2"), is(false));
    }

    @Test
    public void isValid_incorrect_firstSegMore_withSpaces() throws Exception {
        pkv = new ProductKeyValidator();

        assertThat(pkv.isValid("abcdef 8-abc4-abc4-abc4-abcdefabcde2"), is(false));
    }

    @Test
    public void isValid_incorrect_secondSegMore_withOutSpaces() throws Exception {
        pkv = new ProductKeyValidator();

        assertThat(pkv.isValid("abcdefa8-abc45-abc4-abc4-abcdefabcde2"), is(false));
    }

    @Test
    public void isValid_incorrect_secondSegMore_withSpaces() throws Exception {
        pkv = new ProductKeyValidator();

        assertThat(pkv.isValid("abcdefa8-ab 4-abc4-abc4-abcdefabcde2"), is(false));
    }

    @Test
    public void isValid_incorrect_thirdSegMore_withOutSpaces() throws Exception {
        pkv = new ProductKeyValidator();

        assertThat(pkv.isValid("abcdefa8-abc4-abc45-abc4-abcdefabcde2"), is(false));
    }

    @Test
    public void isValid_incorrect_thirdSegMore_withSpaces() throws Exception {
        pkv = new ProductKeyValidator();

        assertThat(pkv.isValid("abcdefa8-abc4-ab 4-abc4-abcdefabcde2"), is(false));
    }

    @Test
    public void isValid_incorrect_fourthSegMore_withOutSpaces() throws Exception {
        pkv = new ProductKeyValidator();

        assertThat(pkv.isValid("abcdefa8-abc4-abc4-abc45-abcdefabcde2"), is(false));
    }

    @Test
    public void isValid_incorrect_fourthSegMore_withSpaces() throws Exception {
        pkv = new ProductKeyValidator();

        assertThat(pkv.isValid("abcdefa8-abc4-abc4-ab 4-abcdefabcde2"), is(false));
    }

    @Test
    public void isValid_incorrect_fifthSegMore_withOutSpaces() throws Exception {
        pkv = new ProductKeyValidator();

        assertThat(pkv.isValid("abcdefa8-abc4-abc4-abc4-abcdefabcde23"), is(false));
    }

    @Test
    public void isValid_incorrect_fifthSegMore_withSpaces() throws Exception {
        pkv = new ProductKeyValidator();

        assertThat(pkv.isValid("abcdefa8-abc4-abc4-abc4-abcdefabcd 2"), is(false));
    }
}