package tappluxe.b28.action.aws.com.tappluxeapplication.validators;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

/**
 * Created by benjo.uriarte on 2/21/2017.
 */
public class MobileNumberValidatorTest {
    MobileNumberValidator mnv;

    @Test
    public void validate_correct() throws Exception {
        mnv = new MobileNumberValidator();
        assertThat(mnv.isValid("12345678901"), is(true));
    }

    @Test
    public void validate_incorrect_empty() throws Exception {
        mnv = new MobileNumberValidator();
        assertThat(mnv.isValid(""), is(false));
    }

    @Test
    public void validate_incorrect_lessThanElevenDigits() throws Exception {
        mnv = new MobileNumberValidator();
        assertThat(mnv.isValid("1234567890"), is(false));
    }

    @Test
    public void validate_incorrect_notOnlyDigits() throws Exception {
        mnv = new MobileNumberValidator();
        assertThat(mnv.isValid("1234567891a"), is(false));
    }
}