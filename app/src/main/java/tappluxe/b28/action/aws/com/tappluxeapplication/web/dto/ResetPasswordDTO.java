package tappluxe.b28.action.aws.com.tappluxeapplication.web.dto;

/**
 * Created by jay.bilocura on 3/14/2017.
 */

public class ResetPasswordDTO {

    private String email;
    private String newPassword;
    private String confirmPassword;

    public ResetPasswordDTO(){
    }

    public ResetPasswordDTO(String email, String newPassword, String confirmPassword){
        this.email = email;
        this.newPassword = newPassword;
        this.confirmPassword = confirmPassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }
}
