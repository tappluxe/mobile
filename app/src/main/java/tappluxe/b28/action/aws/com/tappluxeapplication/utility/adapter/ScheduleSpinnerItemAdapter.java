package tappluxe.b28.action.aws.com.tappluxeapplication.utility.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import tappluxe.b28.action.aws.com.tappluxeapplication.R;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.ScheduleDTO;

/**
 * Created by franc.margallo on 3/22/2017.
 */

public class ScheduleSpinnerItemAdapter extends ArrayAdapter<ScheduleDTO> {
	public ScheduleSpinnerItemAdapter(Activity activity, @NonNull List objects) {
		super(activity, android.R.layout.simple_spinner_item, objects);
	}
}
