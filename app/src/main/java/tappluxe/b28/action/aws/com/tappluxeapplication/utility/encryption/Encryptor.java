package tappluxe.b28.action.aws.com.tappluxeapplication.utility.encryption;

import android.util.Log;

import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;

/**
 * Created by jude.dassun on 3/7/2017.
 */

public class Encryptor {
	private static final String TAG = Encryptor.class.getName();

	public Encryptor() {
	}

	public static KeyPair getKeys(){
		KeyPair kp;
		try {
			KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
			kpg.initialize(1024);
			kp = kpg.genKeyPair();
		} catch (NoSuchAlgorithmException e) {
			kp = null;
			e.printStackTrace();
		}
		return kp;

	}

	public static byte[] Encrypt(Key publicKey, String text){
		byte[] encodedBytes;
		try {
			Cipher c = Cipher.getInstance("RSA");
			c.init(Cipher.ENCRYPT_MODE, publicKey);
			encodedBytes = c.doFinal(text.getBytes());
		} catch (Exception e) {
			encodedBytes = null;
			Log.e(TAG, "AES secret key spec error");
		}
		return encodedBytes;
	}

	public static String Decrypt(Key privateKey, byte[] encodedBytes){
		byte[] decodedBytes;
		try {
			Cipher c = Cipher.getInstance("RSA");
			c.init(Cipher.DECRYPT_MODE, privateKey);
			decodedBytes = c.doFinal(encodedBytes);
		} catch (Exception e) {
			decodedBytes = null;
			Log.e(TAG, "RSA decryption error");
		}
		return new String(decodedBytes);
	}
}
