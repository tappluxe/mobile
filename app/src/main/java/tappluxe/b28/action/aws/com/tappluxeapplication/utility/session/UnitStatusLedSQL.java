package tappluxe.b28.action.aws.com.tappluxeapplication.utility.session;

/**
 * Created by ariel.fabilena on 3/24/2017.
 */

public class UnitStatusLedSQL {
	String unit_id;
	String status;

	public UnitStatusLedSQL(String access_key, String status) {
		this.unit_id = access_key;
		this.status = status;
	}

	public String getUnit_id() {
		return unit_id;
	}

	public void setUnit_id(String unit_id) {
		this.unit_id = unit_id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
