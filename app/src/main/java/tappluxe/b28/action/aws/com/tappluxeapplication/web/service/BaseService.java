package tappluxe.b28.action.aws.com.tappluxeapplication.web.service;

import android.content.Context;
import android.util.Base64;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.session.Session;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.interfaces.WebServiceInterface;

/**
 * Created by jay.bilocura on 3/13/2017.
 */

public class BaseService {
	protected static final String CLIENT_ID = "mobile-app";
	protected static final String CLIENT_PASSWORD = "awsys+123";
	protected static final String SERVER_URL = "http://192.168.42.56:8080";
	protected static final String AUTH_URL = "http://192.168.42.56:8081";
	protected static final String authHeader = "Basic " + Base64.encodeToString((CLIENT_ID + ":" + CLIENT_PASSWORD).getBytes(), Base64.NO_WRAP);
	protected final HttpLoggingInterceptor logging;
	protected final OkHttpClient.Builder httpClientBuilder;
	protected WebServiceInterface webServiceInterface;
	protected final Session session;

	public BaseService(Context activity) {
		session = new Session(activity);
		httpClientBuilder = new OkHttpClient.Builder();
		logging = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
		httpClientBuilder.connectTimeout(120, TimeUnit.SECONDS);
		httpClientBuilder.readTimeout(120, TimeUnit.SECONDS);
		httpClientBuilder.writeTimeout(120, TimeUnit.SECONDS);
		httpClientBuilder.addInterceptor(logging);
		httpClientBuilder.addInterceptor(new Interceptor() {
			@Override
			public okhttp3.Response intercept(Chain chain) throws IOException {
				Request original = chain.request();
				Request request = original.newBuilder().header("Authorization", authHeader).method(original.method(), original.body()).build();
				return chain.proceed(request);
			}
		});
		Retrofit.Builder builder = new Retrofit.Builder().baseUrl(SERVER_URL).addConverterFactory(GsonConverterFactory.create());
		Retrofit retrofit = builder.client(httpClientBuilder.build()).build();
		webServiceInterface = retrofit.create(WebServiceInterface.class);
	}
}
