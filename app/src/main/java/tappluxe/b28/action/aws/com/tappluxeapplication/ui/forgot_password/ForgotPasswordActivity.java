package tappluxe.b28.action.aws.com.tappluxeapplication.ui.forgot_password;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tappluxe.b28.action.aws.com.tappluxeapplication.BaseActivity;
import tappluxe.b28.action.aws.com.tappluxeapplication.R;
import tappluxe.b28.action.aws.com.tappluxeapplication.ui.forgot_password.fragments.ForgotPasswordCodeFragment;
import tappluxe.b28.action.aws.com.tappluxeapplication.ui.forgot_password.fragments.ForgotPasswordEmailFragment;
import tappluxe.b28.action.aws.com.tappluxeapplication.ui.forgot_password.fragments.ResetPasswordFragment;
import tappluxe.b28.action.aws.com.tappluxeapplication.ui.login.Login;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.UserDTO;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.service.UserService;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.service.VerificationService;

/**
 * Created by jay.bilocura on 3/14/2017.
 */

public class ForgotPasswordActivity extends BaseActivity
        implements ForgotPasswordEmailFragment.OnEmailFragmentListener,
            ForgotPasswordCodeFragment.OnCodeFragmentListener,
            ResetPasswordFragment.OnResetPasswordFragmentListener{

    private UserService userService;
    private VerificationService verificationService;

    private ForgotPasswordEmailFragment emailFragment;
    private ForgotPasswordCodeFragment codeFragment;
    private ResetPasswordFragment resetPasswordFragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        setContentView(R.layout.forgot_password_layout);
        userService = new UserService(this);
        verificationService = new VerificationService(this);
        setTitle(getString(R.string.forgot_password_title));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        if (findViewById(R.id.forgot_password_fragment_container) != null) {
            if (savedInstanceState != null) {
                return;
            }

            emailFragment = new ForgotPasswordEmailFragment();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.forgot_password_fragment_container, emailFragment).commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onEmailSubmitted(final String email) {
        userService.forgotPassword(email, new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    codeFragment = ForgotPasswordCodeFragment.newInstance(email);
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.forgot_password_fragment_container,  codeFragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                } else {
                    emailFragment.setEmailError("User with email \"" + email +"\" does not exist.");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                Toast.makeText(getApplicationContext(), "Failure :" + throwable.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onSubmitPassCode(String passcode, final String email) {
        verificationService.checkCode(passcode, email, new Callback<UserDTO>() {
            @Override
            public void onResponse(Call<UserDTO> call, Response<UserDTO> response) {
                if (response.isSuccessful()) {
                    resetPasswordFragment = ResetPasswordFragment.newInstance(email);
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.forgot_password_fragment_container,  resetPasswordFragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                } else {
                    codeFragment.setPassCodeError("Invalid pass code. Code may have expired. Please have a new code resent.");
                }
            }

            @Override
            public void onFailure(Call<UserDTO> call, Throwable throwable) {

            }
        });
    }

    @Override
    public void onResendPassCode(String email) {
        userService.forgotPassword(email, new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "Pass code has been resent.", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), response.message(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("Verify", t.getMessage());
            }
        });
    }

    @Override
    public void onResetPassword(String email, String newPass, String confirmPass) {
        userService.resetPassword(email, newPass, confirmPass, new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    complete();
                } else {
                    Log.e("Reset", "Unsuccesssfully: "+ response.message());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                Log.e("Reset", "Failure: "+ throwable.getMessage());
            }
        });
    }

    public void complete() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(R.string.reset_password_dialog_title);
        dialog.setMessage(R.string.reset_password_dialog_msg);
        dialog.setCancelable(true);
        dialog.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(getApplicationContext(), Login.class);
                startActivity(intent);
                finish();
            }
        });
        dialog.create().show();
    }
}
