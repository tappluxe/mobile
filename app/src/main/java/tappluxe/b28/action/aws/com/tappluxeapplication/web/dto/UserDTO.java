package tappluxe.b28.action.aws.com.tappluxeapplication.web.dto;

import android.util.Log;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import java.io.Serializable;

import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.SimpleCallerUserInterface;

/**
 * Created by ariel.fabilena on 2/9/2017.
 */

public class UserDTO implements Serializable {
    private String email;
    private String oldPassword;
    private String newPassword;
    private String password;
	private String confirmPassword;
    private String firstName;
    private String lastName;
    private String phone;
    private UnitDTO userUnit;
    private Boolean isVerified;

    private Integer countryCode;
    private String mobilePlain;

    public UserDTO(){

    }

    public UserDTO(String email, String password, String confirmPassword, String firstName,
                   String lastName, String phone, UnitDTO userUnit, Boolean isVerified) {
        this.email = email;
        this.password = password;
	    this.confirmPassword = confirmPassword;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phone = phone;
        this.userUnit = userUnit;
        this.isVerified = isVerified;

    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public UnitDTO getUnitDTO() {
        return userUnit;
    }

    public void setUnitDTO(UnitDTO userUnit) {
        this.userUnit = userUnit;
    }

    public Boolean getVerified() {
        return isVerified;
    }

    public void setVerified(Boolean verified) {
        isVerified = verified;
    }

    public String getFullName() {
        return this.firstName + " " + this.getLastName();
    }

    public void resolvePhone() {
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        Phonenumber.PhoneNumber numberProto = null;
        try {
            numberProto = phoneUtil.parse(this.phone, "");
        } catch (NumberParseException e) {
            System.err.println("NumberParseException was thrown: " + e.toString());
        }
        this.countryCode = numberProto.getCountryCode();
        this.mobilePlain = phoneUtil.format(numberProto, PhoneNumberUtil.PhoneNumberFormat.NATIONAL);
    }

    public Integer getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(Integer countryCode) {
        this.countryCode = countryCode;
    }

    public String getMobilePlain() {
        return mobilePlain;
    }

    public void setMobilePlain(String mobilePlain) {
        this.mobilePlain = mobilePlain;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
