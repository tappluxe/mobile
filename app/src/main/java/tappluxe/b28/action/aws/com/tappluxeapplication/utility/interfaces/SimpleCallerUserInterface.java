package tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces;

/**
 * Created by inno.zilmar on 2/14/2017.
 */

public interface SimpleCallerUserInterface {
	void requestStatus(boolean success, String message);
}
