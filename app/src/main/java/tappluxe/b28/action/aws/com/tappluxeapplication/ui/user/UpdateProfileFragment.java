package tappluxe.b28.action.aws.com.tappluxeapplication.ui.user;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import tappluxe.b28.action.aws.com.tappluxeapplication.R;
import tappluxe.b28.action.aws.com.tappluxeapplication.ui.custom.MobileNumberInput;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.SimpleCallerUserInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.UserDTO;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.service.UserService;

/**
 * Created by jay.bilocura on 3/25/2017.
 */

public class UpdateProfileFragment extends Fragment implements SimpleCallerUserInterface {
    private static final String USER = "user";

    private String firstName;
    private String lastName;
    private Integer countryCode;
    private String plainMobileNumber;
    private UserDTO user;
    private UserDTO updatedUser;

    private UserService userService;

    @BindView(R.id.til_first_name) public TextInputLayout tilFirstName;
    @BindView(R.id.til_last_name) public TextInputLayout tilLastName;
    @BindView(R.id.et_first_name) public EditText etFirstName;
    @BindView(R.id.et_last_name) public EditText etLastName;
    @BindView(R.id.mobile_input) public MobileNumberInput mobileNumberInput;

    private OnUpdateProfileFragmentInteractionListener mListener;

    public UpdateProfileFragment() {
    }

    public static UpdateProfileFragment newInstance(UserDTO user) {
        UpdateProfileFragment fragment = new UpdateProfileFragment();
        Bundle args = new Bundle();
        args.putSerializable(USER, user);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            user = (UserDTO) getArguments().getSerializable(USER);
        }
        userService = new UserService(getContext());
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.update_profile_fragment, container, false);
        ButterKnife.bind(this, view);
        firstName = user.getFirstName();
        lastName = user.getLastName();
        countryCode = user.getCountryCode();
        plainMobileNumber = user.getMobilePlain();
        if (firstName != null && !firstName.isEmpty()) {
            etFirstName.setText(firstName);
        }
        if (lastName != null && !lastName.isEmpty()) {
            etLastName.setText(lastName);
        }
        if (countryCode != null) {
            mobileNumberInput.setAsDefault(countryCode);
        }
        if (plainMobileNumber != null && !plainMobileNumber.isEmpty()) {
            mobileNumberInput.setText(plainMobileNumber);
        }
        return view;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnUpdateProfileFragmentInteractionListener) {
            mListener = (OnUpdateProfileFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnUpdateProfileFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @OnTextChanged(R.id.et_first_name)
    public void firstNameTextChange() {
        tilFirstName.setError(null);
        tilFirstName.setErrorEnabled(false);
    }

    @OnTextChanged(R.id.et_last_name)
    public void lastNameTextChange() {
        tilLastName.setError(null);
        tilLastName.setErrorEnabled(false);
    }

    @OnClick(R.id.btn_update_done)
    public void registrationDone() {
        String firstName = etFirstName.getText().toString();
        String lastName = etLastName.getText().toString();
        if(isValidName(firstName, lastName) && mobileNumberInput.isValidMobileNumber()){
            String mobileNumber = mobileNumberInput.getNumber();
            updatedUser = new UserDTO();
            updatedUser.setFirstName(firstName);
            updatedUser.setLastName(lastName);
            updatedUser.setPhone(mobileNumber);
            userService.updateUser(updatedUser, this);
        }

    }

    public boolean isValidName(String firstName, String lastName){
        if(firstName.isEmpty()){
            tilFirstName.setErrorEnabled(true);
            tilFirstName.setError(getText(R.string.empty_first_name_message));
            return false;
        } else if(lastName.isEmpty()){
            tilLastName.setErrorEnabled(true);
            tilLastName.setError(getText(R.string.empty_last_name_message));
            return false;
        }
        return true;
    }

    public boolean hasChanges() {
        return !etFirstName.getText().toString().equals(firstName)
                || !etLastName.getText().toString().equals(lastName)
                || mobileNumberInput.getText().equals(plainMobileNumber)
                || mobileNumberInput.getCountryCode() != countryCode;
    }

    @Override
    public void requestStatus(boolean success, String message) {
        if (success) {
            user.setFirstName(updatedUser.getFirstName());
            user.setLastName(updatedUser.getLastName());
            user.setPhone(updatedUser.getPhone());
            user.resolvePhone();
            Toast.makeText(getContext(), "Profile successfully updated.", Toast.LENGTH_LONG).show();
            mListener.onUpdateProfile(user);
        }
    }

    public interface OnUpdateProfileFragmentInteractionListener {
        void onUpdateProfile(UserDTO user);
    }
}
