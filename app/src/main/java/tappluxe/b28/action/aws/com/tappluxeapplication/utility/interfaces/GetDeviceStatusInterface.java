package tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces;

import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.DeviceDTO;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.DeviceStatusDTO;

/**
 * Created by jay.bilocura on 2/28/2017.
 */

public interface GetDeviceStatusInterface extends SimpleCallerUserInterface {
    void setDeviceStatus(boolean status, String message, DeviceStatusDTO device);
}
