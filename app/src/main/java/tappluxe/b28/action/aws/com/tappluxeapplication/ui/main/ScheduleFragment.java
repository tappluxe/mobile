package tappluxe.b28.action.aws.com.tappluxeapplication.ui.main;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import tappluxe.b28.action.aws.com.tappluxeapplication.R;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.adapter.ScheduleListItemAdapter;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.ListScheduleInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.ListViewParentInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.ScheduleDTO;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.service.ScheduleService;

public class ScheduleFragment extends Fragment implements ListScheduleInterface, ListViewParentInterface {

	@BindView(R.id.lv_view_all_schedules)
	public ListView lvViewSchedules;

	private ScheduleService scheduleService;
	private Call call;
	public final static String GO_TO_SCHEDULE_FRAGMENT = "GO_TO_SCHEDULE_FRAGMENT";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.main_schedule_fragment, container, false);
		ButterKnife.bind(this, view);
		scheduleService = new ScheduleService(this.getContext());
		return view;
	}

	@Override
	public void refresh() {
		cancelCall();
		call = scheduleService.getScheduleList(this);
	}

	public void cancelCall(){
		if (call != null && !call.isCanceled()){
			call.cancel();
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		Log.e(getClass().getName(), "on start called");
		refresh();
	}

	@Override
	public void setListSchedule(final List<ScheduleDTO> schedules) {
		lvViewSchedules.setAdapter(null);
		ScheduleListItemAdapter scheduleListItemAdapter = new ScheduleListItemAdapter(schedules, this.getActivity(), this);
		lvViewSchedules.setAdapter(scheduleListItemAdapter);
		//alarm.setAlarm(this.getActivity(), schedules);
	}

	@Override
	public void onStop() {
		super.onStop();
		cancelCall();
	}

	@Override
	public void getError(String message) {
		if (this.getActivity() != null) {
			try {
				JsonElement element = new JsonParser().parse(message);
				if (element == null) {
					Toast.makeText(this.getActivity(), message, Toast.LENGTH_LONG).show();
				} else {
					JsonObject obj = element.getAsJsonObject();
					if (obj == null) {
						Toast.makeText(this.getActivity(), message, Toast.LENGTH_LONG).show();
					} else {
						JsonElement generalMessage = obj.get("message");
						if(generalMessage != null && !generalMessage.getAsString().isEmpty()){
							Toast.makeText(this.getActivity(), generalMessage.getAsString(), Toast.LENGTH_LONG).show();
						}
						else
							Toast.makeText(this.getActivity(), message, Toast.LENGTH_LONG).show();
					}
				}
			} catch (JsonSyntaxException | IllegalStateException err) {
				Log.e(getClass().getName(), err.getMessage());
				Toast.makeText(this.getActivity(), message, Toast.LENGTH_LONG).show();
			}
		}
	}
}
