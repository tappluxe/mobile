package tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces;

import java.util.List;

import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.UnitDTO;

/**
 * Created by ariel.fabilena on 2/21/2017.
 */

public interface ListUnitInterface{
	void setListUnit(List<UnitDTO> unit);
	void getError(String message);
}
