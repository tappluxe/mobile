package tappluxe.b28.action.aws.com.tappluxeapplication.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by franc.margallo on 2/14/2017.
 */

public class ProductKeyValidator {

    private Pattern pattern;
    private Matcher matcher;

    private static final String PK_PATTERN
            = "^[0-9A-Fa-f]{8}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{12}$";

    public ProductKeyValidator() {

        pattern = Pattern.compile(PK_PATTERN);
    }

    public boolean isValid(final String productKey) {

        matcher = pattern.matcher(productKey);
        return matcher.matches();
    }
}
