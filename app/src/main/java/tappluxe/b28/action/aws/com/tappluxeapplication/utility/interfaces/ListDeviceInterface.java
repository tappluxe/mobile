package tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces;

import java.util.List;

import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.DeviceDTO;

/**
 * Created by ariel.fabilena on 2/15/2017.
 */

public interface ListDeviceInterface{
	void setListDevice(List<DeviceDTO> devices);
	void getError(String message);
}
