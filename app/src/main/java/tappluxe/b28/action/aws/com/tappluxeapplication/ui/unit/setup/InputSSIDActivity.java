package tappluxe.b28.action.aws.com.tappluxeapplication.ui.unit.setup;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.widget.Button;
import android.widget.EditText;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tappluxe.b28.action.aws.com.tappluxeapplication.BaseActivity;
import tappluxe.b28.action.aws.com.tappluxeapplication.R;
import tappluxe.b28.action.aws.com.tappluxeapplication.ui.unit.ViewUnit;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.SimpleCallerUserInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.UnitDTO;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.service.HardwareService;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.service.UnitService;

/**
 * Created by jay.bilocura on 3/21/2017.
 */

public class InputSSIDActivity extends BaseActivity implements SimpleCallerUserInterface{

    public static final String RECONFIGURE = "reconfigure";
    public static final int RECONFIGURE_REQUEST_CODE = 1;
    public boolean reconfigure;
    private UnitDTO unit;
    private HardwareService hardwareService;
    private UnitService unitService;
    private WifiManager wifiManager;
    private String homeSsid;
    private ProgressDialog progressDialog;
    private static final int CONFIGURE_STARTED = 0;
    private static final int CONFIGURE_FINISHED = 1;
    private Thread t;

    @BindView(R.id.til_ssid) public TextInputLayout tilSsid;
    @BindView(R.id.til_ssid_pwd) public TextInputLayout tilSsidPwd;
    @BindView(R.id.et_ssid) public EditText etSsid;
    @BindView(R.id.et_ssid_pwd) public EditText etSsidPwd;
    @BindView(R.id.btn_network_submit) public Button btnSubmit;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.input_ssid_layout);
        setTitle("Network Setup");
        ButterKnife.bind(this);
        Intent intent = getIntent();
        unit = (UnitDTO) intent.getSerializableExtra("unit");
        reconfigure = intent.getBooleanExtra("reconfigure", false);
        homeSsid = intent.getStringExtra("homeSsid");
        hardwareService = new HardwareService();
        unitService = new UnitService(this);
        progressDialog = new ProgressDialog(InputSSIDActivity.this);
        wifiManager = (WifiManager) this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
    }

    public boolean isValidSSID() {
        if(etSsid.getText().toString().isEmpty()) {
            tilSsid.setErrorEnabled(true);
            tilSsid.setError("SSID must not be empty");
            return false;
        }
        return true;
    }

    public boolean isValidPassword() {
        if(etSsidPwd.getText().toString().isEmpty()) {
            tilSsidPwd.setErrorEnabled(true);
            tilSsidPwd.setError("Password must not be empty");
            return false;
        }
        return true;
    }

    private Handler handler = new Handler(){
        @Override public void handleMessage(Message msg) {
            switch (msg.what) {
                case CONFIGURE_STARTED:
                    progressDialog.setTitle("Configuring");
                    progressDialog.setMessage("Please wait...");
                    progressDialog.setCancelable(true  );
                    progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            t.interrupt();
                        }
                    });
                    progressDialog.show();
                    break;
                case CONFIGURE_FINISHED:
                    reconnect();
                    if(progressDialog.isShowing()){
                        progressDialog.dismiss();
                    }
                    complete();
                    break;
            }
        }
    };

    @OnClick(R.id.btn_network_submit)
    public void submitNetwork(){
        if (isValidPassword() && isValidSSID()) {
            Message m = new Message();
            m.what = CONFIGURE_STARTED;
            handler.sendMessage(m);
            final String ssid = etSsid.getText().toString();
            final String password = etSsidPwd.getText().toString();
            hardwareService.setUpSSID(ssid, password, new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()) {
                        reconnect();
                        t = new Thread() {
                            @Override
                            public void run(){
                                ConnectivityManager Cm = (ConnectivityManager) getApplicationContext().getSystemService(CONNECTIVITY_SERVICE);
                                NetworkInfo Ni = Cm.getActiveNetworkInfo();
                                while (Ni == null) {
                                    Ni = Cm.getActiveNetworkInfo();
                                    try {
                                        Thread.sleep(2000);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                }
                                unitService.updateSSID(unit.getId(), ssid, InputSSIDActivity.this);
                            }
                        };
                        t.start();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    btnSubmit.setEnabled(true);
                }
            });
        }
    }

    public void complete() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(R.string.ssid_setup_success_title);
        dialog.setMessage(R.string.ssid_setup_success_msg);
        dialog.setCancelable(false);
        dialog.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (reconfigure) {
                    Intent intent = new Intent();
                    intent.putExtra(RECONFIGURE, etSsid.getText().toString());
                    setResult(RESULT_OK, intent);
                } else {
                    Intent intent = new Intent(getApplicationContext(), ViewUnit.class);
                    unit.setSsid(etSsid.getText().toString());
                    intent.putExtra("unit", unit);
                    startActivity(intent);
                }
                finish();
            }
        });
        dialog.create().show();
    }


    @Override
    public void requestStatus(boolean success, String message) {
        if (success) {
            Message m = new Message();
            m.what = CONFIGURE_FINISHED;
            handler.sendMessage(m);
        }
    }

    public void reconnect(){
        List<WifiConfiguration> list = wifiManager.getConfiguredNetworks();
        for( WifiConfiguration i : list ) {
            if (i.SSID != null && i.SSID.equals(homeSsid)) {
                wifiManager.disconnect();
                wifiManager.enableNetwork(i.networkId, true);
                wifiManager.reconnect();
                break;
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        String ssid = wifiInfo.getSSID();
        if (!ssid.equals(homeSsid)) {
            reconnect();
        }
    }

}
