package tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces;

import android.view.View;
import android.widget.TextView;

import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.SocketChangeDetailsResponseDTO;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.UnitSocketDTO;

/**
 * Created by jude.dassun on 3/1/2017.
 */

public interface TurnOffDeviceInteface {
	void turnOffDeviceResult(View socketSwitch, View progressBar, UnitSocketDTO socket, TextView turnedOnElapsedTime);
	void setError(String message, View socketSwitch, View progressBar);
}
