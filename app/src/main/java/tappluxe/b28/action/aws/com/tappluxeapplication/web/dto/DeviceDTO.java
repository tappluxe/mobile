package tappluxe.b28.action.aws.com.tappluxeapplication.web.dto;

import java.io.Serializable;

/**
 * Created by ariel.fabilena on 2/14/2017.
 */

public class DeviceDTO implements Serializable {

	private String name;
	private Long userUnitSocketId;
	private Long id;

//	public DeviceDTO(String name, Long userUnitSocketId, Long scheduleId, Long id) {
//		this.name = name;
//		this.userUnitSocketId = userUnitSocketId;
//		this.scheduleId = scheduleId;
//		this.id = id;
//	}

	public DeviceDTO(String name, Long userUnitSocketId, Long id) {
		this.name = name;
		this.userUnitSocketId = userUnitSocketId;
		this.id = id;
	}

	public DeviceDTO(String name, Long userUnitSocketId) {
		this.name = name;
		this.userUnitSocketId = userUnitSocketId;
	}

	public DeviceDTO(Long userUnitSocketId){
		this.userUnitSocketId = userUnitSocketId;
	}

	public DeviceDTO(String name) {
		this.name = name;
	}

	public DeviceDTO(){
		this.name = "None";
	}

	//	public DeviceDTO(String name, Long userUnitSocketId, Long scheduleId) {
//		this.name = name;
//		this.userUnitSocketId = userUnitSocketId;
//		this.scheduleId = scheduleId;
//	}

//	public DeviceDTO(String name, Long scheduleId) {
//		this.name = name;
//		this.scheduleId = scheduleId;
//	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getUserUnitSocketId() {
		return userUnitSocketId;
	}

	public void setUserUnitSocketId(Long userUnitSocketId) {
		this.userUnitSocketId = userUnitSocketId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return name;
	}
}
