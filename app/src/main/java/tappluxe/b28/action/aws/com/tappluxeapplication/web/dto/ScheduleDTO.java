package tappluxe.b28.action.aws.com.tappluxeapplication.web.dto;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by benjo.uriarte on 3/7/2017.
 */

public class ScheduleDTO implements Serializable {

	private Long id;
	private String name;
	private String time;
	private String repeat;
	private Integer timeBeforeNotification;
	private Boolean notificationEnabled;
	private Boolean isActive;

	public ScheduleDTO(String name, String time, String repeat, Integer timeBeforeNotification) {
		this.name = name;
		this.time = time;
		this.repeat = repeat;
		this.timeBeforeNotification = timeBeforeNotification;
	}

	public ScheduleDTO() {
		name = "None";
	}

	public ScheduleDTO(Long id, Boolean isActive) {
		this.id = id;
		this.isActive = isActive;
	}

	public ScheduleDTO(Long id, String name, String time, String repeat,
					   Integer timeBeforeNotification, Boolean isActive,
					   Boolean notificationEnabled) {
		this.id = id;
		this.name = name;
		this.time = time;
		this.repeat = repeat;
		this.timeBeforeNotification = timeBeforeNotification;
		this.isActive = isActive;
		this.notificationEnabled = notificationEnabled;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getRepeat() {
		return repeat;
	}

	public void setRepeat(String repeat) {
		this.repeat = repeat;
	}

	public Integer getTimeBeforeNotification() {
		return timeBeforeNotification;
	}

	public void setTimeBeforeNotification(Integer timeBeforeNotification) {
		this.timeBeforeNotification = timeBeforeNotification;
	}

	public Boolean isActive() {
		return isActive;
	}

	public void setActive(Boolean active) {
		isActive = active;
	}

	public Boolean isNotificationEnabled() {
		return notificationEnabled;
	}

	public void setNotificationEnabled(Boolean notificationEnabled) {
		this.notificationEnabled = notificationEnabled;
	}

	protected ScheduleDTO(Parcel in) {
		name = in.readString();
		repeat = in.readString();
		isActive = in.readByte() != 0;
	}

	@Override
	public String toString() {
		return name;
	}
}
