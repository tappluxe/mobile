package tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces;

import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.ScheduleDTO;

/**
 * Created by jude.dassun on 3/9/2017.
 */

public interface GetScheduleInterface {
	void setSchedule(String message, ScheduleDTO unit);
	void setError(String message);
}
