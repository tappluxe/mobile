package tappluxe.b28.action.aws.com.tappluxeapplication.web.service;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import tappluxe.b28.action.aws.com.tappluxeapplication.ui.custom.TappluxeSwitchCompat;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.GetDeviceStatusInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.GetStatusSocketInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.TurnOffDeviceInteface;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.session.Session;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.DeviceStatusDTO;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.SocketChangeDetailsResponseDTO;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.UnitSocketDTO;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.interfaces.WebServiceInterface;

/**
 * Created by jay.bilocura on 2/28/2017.
 */

public class DeviceStatusService extends BaseService {

	private static final String TAG = DeviceStatusService.class.getName();

	public DeviceStatusService(Context activity) {
		super(activity);
	}

	public Call getCurrentStatus(Long deviceId, final GetDeviceStatusInterface activity) {
		Call<DeviceStatusDTO> call = webServiceInterface.getCurrentDeviceStatus(deviceId,
				session.getValue(Session.ACCESS_TOKEN));

		call.enqueue(new Callback<DeviceStatusDTO>() {
			@Override
			public void onResponse(Call<DeviceStatusDTO> call, Response<DeviceStatusDTO> response) {
				if (response.isSuccessful()) {
					String str = response.body().toString();

					Log.e(TAG, "Successfully Fetched Current Device Status.");

					activity.setDeviceStatus(true, "Successfully Fetched Device Status.",
							response.body());
				} else {
					//get error message
					try {
						String str = response.errorBody().string();
						Log.e(TAG, "error code: " + response.code()+ " onResponse: " + str);
						activity.requestStatus(false, str);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

			@Override
			public void onFailure(Call<DeviceStatusDTO> call, Throwable t) {
				Log.e(TAG, t.getClass().getName());
				if(t.getMessage() != null)
					Log.e(TAG, t.getMessage());
				t.printStackTrace();
				activity.requestStatus(false, "Cannot connect to the server.");
			}
		});
		return call;
	}

	public void turnOffDevice(final Long userUnitId, final Long socketId, Boolean state,
							  final View socketSwitch, final View progresssBar, final UnitSocketDTO socket, final TextView turnedOnElapsedTime, final TurnOffDeviceInteface activity) {
		Call<SocketChangeDetailsResponseDTO> call = webServiceInterface.turnOffSocket(userUnitId, socketId, state, session.getValue(Session.ACCESS_TOKEN));

		call.enqueue(new Callback<SocketChangeDetailsResponseDTO>() {
			@Override
			public void onResponse(Call<SocketChangeDetailsResponseDTO> call, Response<SocketChangeDetailsResponseDTO> response) {
				if (response.isSuccessful()) {
					activity.turnOffDeviceResult(socketSwitch, progresssBar, socket, turnedOnElapsedTime);

				} else {
					//get error message
					try {
						String str = response.errorBody().string();
						Log.e(TAG, "error code: " + response.code()+ " onResponse: " + str);
						activity.setError(str, socketSwitch, progresssBar);
					} catch (IOException e) {
						activity.setError(e.toString(), socketSwitch, progresssBar);
						e.printStackTrace();
					}
				}
			}

			@Override
			public void onFailure(Call<SocketChangeDetailsResponseDTO> call, Throwable t) {Log.e(TAG, t.getClass().getName());
				Log.e(TAG, t.getClass().getName());
				if(t.getMessage() != null)
					Log.e(TAG, t.getMessage());
				t.printStackTrace();
				activity.setError("Cannot connect to the server.", socketSwitch, progresssBar);
			}
		});
	}

	public void requestSocketState(final Long userUnitId, final Long socketId, final UnitSocketDTO socket,
	                               final TappluxeSwitchCompat socketSwitch, final TextView turnOnElapsedTime, final GetStatusSocketInterface activity) {
		Call<DeviceStatusDTO> call = webServiceInterface.requestStateSocket(userUnitId, socketId, session.getValue(Session.ACCESS_TOKEN));

		call.enqueue(new Callback<DeviceStatusDTO>() {
			@Override
			public void onResponse(Call<DeviceStatusDTO> call, Response<DeviceStatusDTO> response) {
				if (response.isSuccessful()) {
					activity.requestSocketStatus(response.body().isState(), socket, turnOnElapsedTime, socketSwitch);

				} else {
					//get error message
					try {
						String str = response.errorBody().string();
						Log.e(TAG, "error code: " + response.code()+ " onResponse: " + str);
						activity.socketError(response.errorBody().toString());
					} catch (IOException e) {
						activity.socketError(response.errorBody().toString());
						e.printStackTrace();
					}
				}
			}

			@Override
			public void onFailure(Call<DeviceStatusDTO> call, Throwable t) {Log.e(TAG, t.getClass().getName());
				Log.e(TAG, t.getClass().getName());
				if(t.getMessage() != null)
					Log.e(TAG, t.getMessage());
				t.printStackTrace();
				activity.socketError("Cannot connect to the server.");
			}
		});
	}
//	public void turnOffDeviceResponse(Long userUnitId, Long socketId, Long responseId, final View socketSwitch, final View progressBar, final TurnOffDeviceInteface activity) {
//		Call<SocketChangeDetailsResponseDTO> call = webServiceInterface.turnOffSocketResponse(userUnitId, socketId, responseId, session.getValue(Session.ACCESS_TOKEN));
//
//		call.enqueue(new Callback<SocketChangeDetailsResponseDTO>() {
//			@Override
//			public void onResponse(Call<SocketChangeDetailsResponseDTO> call, Response<SocketChangeDetailsResponseDTO> response) {
//				if (response.isSuccessful()) {
//					Log.d(TAG, "Success");
//					activity.turnOffDeviceResult(true, response.body(), socketSwitch, progressBar);
//				} else {
//					//get error message
//					try {
//						String str = response.errorBody().string();
//						Log.e(TAG, "error code: " + response.code()+ " onResponse: " + str);
//						activity.setError(response.errorBody().string(), socketSwitch, progressBar);
//					} catch (IOException e) {
//						activity.setError(e.toString(), socketSwitch, progressBar);
//						e.printStackTrace();
//					}
//				}
//			}
//
//			@Override
//			public void onFailure(Call<SocketChangeDetailsResponseDTO> call, Throwable t) {
//				Log.e(TAG, t.getClass().getName());
//				if(t.getMessage() == null)
//					Log.e(TAG, t.getMessage());
//				t.printStackTrace();
//				activity.setError("Cannot connect to the server.", socketSwitch, progressBar);
//			}
//		});
//	}

}
