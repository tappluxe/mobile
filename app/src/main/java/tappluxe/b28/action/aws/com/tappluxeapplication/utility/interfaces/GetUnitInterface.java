package tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces;

import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.UnitDTO;

/**
 * Created by ariel.fabilena on 2/21/2017.
 */

public interface GetUnitInterface {
	void setUnit(String message, UnitDTO unit);
	void setError(String message);
}
