package tappluxe.b28.action.aws.com.tappluxeapplication.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by benjo.uriarte on 2/9/2017.
 */

public class PasswordValidator {

    private Pattern pattern;
    private Matcher matcher;

    private static final String PASSWORD_PATTERN
            = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$/^&*();,+~`_|]).{6,20})";

    public PasswordValidator() {

        pattern = Pattern.compile(PASSWORD_PATTERN);
    }

    public boolean isValid(final String password) {

        matcher = pattern.matcher(password);
        return matcher.matches();
    }
}
