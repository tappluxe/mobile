package tappluxe.b28.action.aws.com.tappluxeapplication.web.dto;

import java.io.Serializable;
import java.net.Socket;

/**
 * Created by ariel.fabilena on 3/13/2017.
 */

public class DeviceSocketDTO implements Serializable{
	private DeviceDTO device;
	private UnitSocketDTO socket;

	public DeviceSocketDTO(DeviceDTO device, UnitSocketDTO socket) {
		this.device = device;
		this.socket = socket;
	}

	public DeviceDTO getDevice() {
		return device;
	}

	public void setDevice(DeviceDTO device) {
		this.device = device;
	}

	public UnitSocketDTO getSocket() {
		return socket;
	}

	public void setSocket(UnitSocketDTO socket) {
		this.socket = socket;
	}

}
