package tappluxe.b28.action.aws.com.tappluxeapplication.ui.registration.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import tappluxe.b28.action.aws.com.tappluxeapplication.R;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.SimpleCallerUserInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.validators.EmailValidator;
import tappluxe.b28.action.aws.com.tappluxeapplication.validators.PasswordValidator;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.service.UserService;


public class CredentialsFragment extends Fragment implements SimpleCallerUserInterface {
    private static final String EMAIL = "email";
    private static final String PASSWORD = "password";
    private static final String CONFIRM_PASSWORD = "confirm_password";
    private String email;
    private String password;
    private String confirmPassword;

    private UserService userService;

    @BindView(R.id.til_password) public TextInputLayout tilPassword;
    @BindView(R.id.til_confirm_password) public TextInputLayout tilConfirmPassword;
    @BindView(R.id.til_email) public TextInputLayout tilEmail;
    @BindView(R.id.et_email) public EditText etEmail;
    @BindView(R.id.et_password) public EditText etPassword;
    @BindView(R.id.et_confirm_password) public EditText etConfirmPassword;
    @BindView(R.id.rl_email) public TextView rl_email;
    @BindView(R.id.rl_password) public TextView rl_password;
    @BindView(R.id.rl_c_password) public TextView rl_c_password;


    private OnCredentialsFragmentInteractionListener mListener;

    public CredentialsFragment() {
    }

    public static CredentialsFragment newInstance(String email, String password, String confirmPassword) {
        CredentialsFragment fragment = new CredentialsFragment();
        Bundle args = new Bundle();
        args.putString(EMAIL, email);
        args.putString(PASSWORD, password);
        args.putString(CONFIRM_PASSWORD, confirmPassword);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            email = getArguments().getString(EMAIL);
            password = getArguments().getString(PASSWORD);
            confirmPassword = getArguments().getString(CONFIRM_PASSWORD);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.credentials_fragment, container, false);
        ButterKnife.bind(this, view);
        userService = new UserService(getContext().getApplicationContext());
        if (email != null && !email.isEmpty()) {
            etEmail.setText(email);
        }
        if (password != null && !password.isEmpty()) {
            etPassword.setText(password);
        }
        if (confirmPassword != null && !confirmPassword.isEmpty()) {
            etConfirmPassword.setText(confirmPassword);
        }


        etPassword.addTextChangedListener(new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tilPassword.setError(null);
                tilPassword.setErrorEnabled(false);
            }
            @Override public void afterTextChanged(Editable s) {
                if(!etPassword.getText().toString().trim().equalsIgnoreCase("")){//if not empty; hide
                    rl_password.setVisibility(View.INVISIBLE);
                } else {
                    rl_password.setVisibility(View.VISIBLE);
                }
            }
        });
        etConfirmPassword.addTextChangedListener(new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tilConfirmPassword.setError(null);
                tilConfirmPassword.setErrorEnabled(false);
            }
            @Override public void afterTextChanged(Editable s) {
                if(!etConfirmPassword.getText().toString().trim().equalsIgnoreCase("")){//if not empty; hide
                    rl_c_password.setVisibility(View.INVISIBLE);
                } else {
                    rl_c_password.setVisibility(View.VISIBLE);
                }
            }
        });
        etEmail.addTextChangedListener(new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tilEmail.setError(null);
                tilEmail.setErrorEnabled(false);
            }
            @Override public void afterTextChanged(Editable s) {
                if(!etEmail.getText().toString().trim().equalsIgnoreCase("")){//if not empty; hide
                    rl_email.setVisibility(View.INVISIBLE);
                } else {
                    rl_email.setVisibility(View.VISIBLE);
                }
            }
        });
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnCredentialsFragmentInteractionListener) {
            mListener = (OnCredentialsFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnCredentialsFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        String email = etEmail.getText().toString();
        String password = etPassword.getText().toString();
        String confirmPassword = etConfirmPassword.getText().toString();
        mListener.onSaveCredentials(email, password, confirmPassword);
        super.onDetach();
        mListener = null;
    }

    @OnClick(R.id.btn_credentials_next)
    public void credentialsNext() {
        String email = etEmail.getText().toString();
        String password = etPassword.getText().toString();
        String confirmPassword = etConfirmPassword.getText().toString();
        if(isValidEmail(email) && isValidPassword(password, confirmPassword)) {
            userService.checkEmail(email, this);
        }
    }

    @Override
    public void requestStatus(boolean success, String message) {
        String email = etEmail.getText().toString();
        String password = etPassword.getText().toString();
        String confirmPassword = etConfirmPassword.getText().toString();

        if(success) {
            mListener.onCredentialsDone(email, password, confirmPassword);
        } else {
            tilEmail.setErrorEnabled(true);
            tilEmail.setError(message);
        }
    }

    public boolean isValidEmail(CharSequence email)
    {
        EmailValidator ev = new EmailValidator();

        if(email.toString().isEmpty()) {
            tilEmail.setErrorEnabled(true);
            tilEmail.setError(getText(R.string.empty_email_message));
            return false;
        }
        else if(!ev.isValid(etEmail.getText()))
        {
            tilEmail.setErrorEnabled(true);
            tilEmail.setError(getText(R.string.invalid_email_format_message));
            return false;
        }

        return true;
    }

    public boolean isValidPassword(String password, String confirmPassword) {

        PasswordValidator pv = new PasswordValidator();

        if(password.isEmpty()) {
            tilPassword.setErrorEnabled(true);
            tilPassword.setError(getText(R.string.empty_password_message));
            return false;
        } else if(password.length()
                < getResources().getInteger(R.integer.min_password)) {
            tilPassword.setErrorEnabled(true);
            tilPassword.setError(getText(R.string.invalid_password_less_part1of2_message).toString()
                    + " "
                    + getResources().getInteger(R.integer.min_password)
                    + " "
                    + getText(R.string.invalid_password_less_part2of2_message));
            return false;
        } else if(!pv.isValid(password)) {
            tilPassword.setErrorEnabled(true);
            tilPassword.setError(getText(R.string.invalid_password_do_not_meet_requirements_message));
            return false;
        } else if(!password.equals(confirmPassword)) {
            tilConfirmPassword.setErrorEnabled(true);
            tilConfirmPassword.setError(getText(R.string.invalid_password_do_not_match_message));
            return false;
        }

        return true;
    }

    public interface OnCredentialsFragmentInteractionListener {
        void onCredentialsDone(String email, String password, String confirmPassword);
        void onSaveCredentials(String email, String password, String confirmPassword);
    }
}
