package tappluxe.b28.action.aws.com.tappluxeapplication.utility.adapter.country_util;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import tappluxe.b28.action.aws.com.tappluxeapplication.R;
import tappluxe.b28.action.aws.com.tappluxeapplication.entities.Country;

/**
 * Created by jay.bilocura on 3/9/2017.
 */

public class CountryList {
    private List<Country> countries;

    public CountryList() {
    }


    private static String getJsonFromRaw(Context context, int resource) {
        String json;
        try {
            InputStream inputStream = context.getResources().openRawResource(resource);
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public List<Country> getCountries(Context context) {
        if (countries != null) {
            return countries;
        }
        countries = new ArrayList<Country>();
        try {
            JSONArray countryJson= new JSONArray(getJsonFromRaw(context, R.raw.countries));
            for (int i = 0; i < countryJson.length(); i++) {
                try {
                    JSONObject country = (JSONObject) countryJson.get(i);
                    countries.add(new Country(country.getString("name"), country.getString("iso2"), country.getInt("dialCode")));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return countries;
    }

    public int indexOfIso(String iso) {
        for (int i = 0; i < this.countries.size(); i++) {
            if (this.countries.get(i).getIsoCode().toUpperCase().equals(iso.toUpperCase())) {
                return i;
            }
        }
        return -1;
    }

    public Country getCountry(int index) {
        return this.countries.get(index);
    }

    public Country getCountryWithCode(int countryCode) {
        for (Country country: countries) {
            if (country.getDialCode() == countryCode){
                return country;
            }
        }
        return null;
    }

}
