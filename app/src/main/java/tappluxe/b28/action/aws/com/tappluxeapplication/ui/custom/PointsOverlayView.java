package tappluxe.b28.action.aws.com.tappluxeapplication.ui.custom;


import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

public class PointsOverlayView extends View {

    PointF[] points;
    private Paint paint;
    private boolean blinkState;
    private boolean init;

    public PointsOverlayView(Context context) {
        super(context);
        init();
    }

    public PointsOverlayView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PointsOverlayView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        blinkState = true;
        init = true;
    }

    public void setPoints(PointF[] points) {
        this.points = points;
        init = false;
        invalidate();
    }

    public void blink(){
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.e("blink", Boolean.toString(blinkState));
                int timeToBlink = 1000;
                try{Thread.sleep(timeToBlink);}catch (Exception e) {}
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (blinkState) {
                            blinkState = false;
                        } else {
                            blinkState = true;
                        }
                        invalidate();
                    }
                });
            }
        }).start();
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (init) {
            paint.setColor(Color.WHITE);
            paint.setStyle(Paint.Style.STROKE);
            float strokewidth = 10f;
            paint.setStrokeWidth(strokewidth);

            int width = canvas.getWidth();
            int height = canvas.getHeight();
            int length = (int) (width * 0.60);
            float left = width / 2f - length / 2f;
            float top = height / 2f - length / 2f;
            float right = width / 2f + length / 2f;
            float bot = height / 2f + length / 2f;
            int lineLength = (int) (length * .25);
            // top left corner
            canvas.drawLine(left, top - strokewidth / 2, left, top + lineLength, paint); // vert
            canvas.drawLine(left - strokewidth / 2, top, left + lineLength, top, paint); // hor
            // top right corner
            canvas.drawLine(right, top - strokewidth / 2, right, top + lineLength, paint); // vert
            canvas.drawLine(right - lineLength + strokewidth / 2, top, right + strokewidth / 2, top, paint); // hor
            // bot left corner
            canvas.drawLine(left, bot - lineLength + strokewidth / 2, left, bot + strokewidth / 2, paint); //  vert
            canvas.drawLine(left - strokewidth / 2, bot, left + lineLength - strokewidth / 2, bot, paint); // hor
            // bot right corner
            canvas.drawLine(right, bot - lineLength + strokewidth / 2, right, bot + strokewidth / 2, paint); // vert
            canvas.drawLine(right - lineLength + strokewidth / 2, bot, right + strokewidth / 2, bot, paint); //  hor

            paint.setStyle(Paint.Style.FILL);

            if (blinkState) {
                paint.setColor(Color.argb(200, 0, 255, 255));
            } else {
                paint.setColor(Color.argb(100, 0, 255, 255));
            }

            int moduleLength = length / 21;
            canvas.drawCircle(left + (3 * moduleLength) + strokewidth / 2, top + (3 * moduleLength), 5, paint);
            canvas.drawCircle(right - (3 * moduleLength), top + (3 * moduleLength), 5, paint);
            canvas.drawCircle(left + (3 * moduleLength), bot - (3 * moduleLength), 5, paint);
            canvas.drawCircle(right - (5 * moduleLength), bot - (5 * moduleLength), 5, paint);
            blink();
        } else {
            paint.setStyle(Paint.Style.FILL);
            paint.setColor(Color.argb(200, 0, 255, 255));
            for (PointF pointF : points) {
                canvas.drawCircle(pointF.x, pointF.y, 5, paint);
            }
        }
    }
}