package tappluxe.b28.action.aws.com.tappluxeapplication.ui.forgot_password.fragments;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import tappluxe.b28.action.aws.com.tappluxeapplication.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ForgotPasswordEmailFragment extends Fragment {

    @BindView(R.id.et_forgot_password_email)
    public EditText etEmail;

    @BindView(R.id.til_forgot_password_email)
    public TextInputLayout tilEmail;

    private OnEmailFragmentListener mListener;

    public ForgotPasswordEmailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mListener = (OnEmailFragmentListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnEmailSubmittedListener");
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.forgot_password_email_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @OnClick(R.id.btn_forgot_password_email)
    public void submitEmail() {
        String email = etEmail.getText().toString();
        mListener.onEmailSubmitted(email);

    }

    @OnTextChanged(R.id.et_forgot_password_email)
    public void emailTextChange() {
        tilEmail.setErrorEnabled(false);
    }

    public interface OnEmailFragmentListener {
        void onEmailSubmitted(String email);
    }

    public void setEmailError(String error) {
        tilEmail.setErrorEnabled(true);
        tilEmail.setError(error);
    }

}
