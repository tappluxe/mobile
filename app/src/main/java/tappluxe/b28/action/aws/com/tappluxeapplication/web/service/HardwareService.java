package tappluxe.b28.action.aws.com.tappluxeapplication.web.service;

import android.widget.Toast;

import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.interfaces.HardwareServiceInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.interfaces.WebServiceInterface;

/**
 * Created by jay.bilocura on 3/21/2017.
 */

public class HardwareService {
    protected static final String UNIT_URL = "http://192.168.1.1:80";
    protected final OkHttpClient.Builder httpClientBuilder;
    protected final HttpLoggingInterceptor logging;
    protected HardwareServiceInterface hardwareServiceInterface;


    public HardwareService() {
        httpClientBuilder = new OkHttpClient.Builder();
        logging = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClientBuilder.connectTimeout(5, TimeUnit.SECONDS);
        httpClientBuilder.readTimeout(30, TimeUnit.SECONDS);
        httpClientBuilder.writeTimeout(30, TimeUnit.SECONDS);
        httpClientBuilder.addInterceptor(logging);
        Retrofit.Builder builder = new Retrofit.Builder().baseUrl(UNIT_URL).addConverterFactory(GsonConverterFactory.create());
        Retrofit retrofit = builder.client(httpClientBuilder.build()).build();
        hardwareServiceInterface = retrofit.create(HardwareServiceInterface.class);
    }

    public void setUpSSID(String ssid, String password, Callback<ResponseBody> callback){
        String text = "uname="+ssid+"\npwd=" + password;
        RequestBody body = RequestBody.create(MediaType.parse("text/plain"), text);
        Call<ResponseBody> call = hardwareServiceInterface.setUpSSID(body);
        call.enqueue(callback);
    }

}
