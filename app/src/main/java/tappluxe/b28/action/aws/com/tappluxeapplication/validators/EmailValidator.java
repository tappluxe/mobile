package tappluxe.b28.action.aws.com.tappluxeapplication.validators;

/**
 * Created by benjo.uriarte on 2/10/2017.
 */

public class EmailValidator {

    public EmailValidator() {

    }

    public boolean isValid(final CharSequence email) {
        if (android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            return true;
        } else {
            return false;
        }
    }
}
