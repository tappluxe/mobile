package tappluxe.b28.action.aws.com.tappluxeapplication.ui.registration.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import tappluxe.b28.action.aws.com.tappluxeapplication.R;

public class UnitNameFragment extends Fragment {
    private static final String UNIT_NAME = "unit_name";
    private String unitName;

    @BindView(R.id.til_unit_name) public TextInputLayout tilUnitName;
    @BindView(R.id.et_unit_name) public EditText etUnitName;

    private OnUnitNameFragmentInteractionListener mListener;

    public UnitNameFragment() {
    }

    public static UnitNameFragment newInstance(String unitName) {
        UnitNameFragment fragment = new UnitNameFragment();
        Bundle args = new Bundle();
        args.putString(UNIT_NAME, unitName);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            unitName = getArguments().getString(UNIT_NAME);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.unit_name_fragment, container, false);
        ButterKnife.bind(this, view);
        if (unitName != null && !unitName.isEmpty()) {
            etUnitName.setText(unitName);
        }
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnUnitNameFragmentInteractionListener) {
            mListener = (OnUnitNameFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnUnitNameFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        String unitName = etUnitName.getText().toString();
        mListener.onSaveUnitName(unitName);
        super.onDetach();
        mListener = null;
    }


    @OnTextChanged(R.id.et_unit_name)
    public void unitNameTextChanged() {
        tilUnitName.setError(null);
        tilUnitName.setErrorEnabled(false);
    }

    @OnClick(R.id.btn_unit_name_next)
    public void unitNameNext() {
        String unitName = etUnitName.getText().toString();
        if (isValidUnitName(unitName)) {
            mListener.onUnitNameDone(unitName);
        }
}

    public boolean isValidUnitName(String unitName){
        if(unitName.isEmpty()) {
            tilUnitName.setErrorEnabled(true);
            tilUnitName.setError(getText(R.string.empty_unit_name_message));
            return false;
        }
        return true;
    }

    public interface OnUnitNameFragmentInteractionListener {
        void onUnitNameDone(String unitName);
        void onSaveUnitName(String unitName);
    }

}
