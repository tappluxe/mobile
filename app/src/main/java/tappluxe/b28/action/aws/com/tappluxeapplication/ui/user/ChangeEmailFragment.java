package tappluxe.b28.action.aws.com.tappluxeapplication.ui.user;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import tappluxe.b28.action.aws.com.tappluxeapplication.R;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.SimpleCallerUserInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.validators.EmailValidator;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.UserDTO;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.service.UserService;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnChangeEmailFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ChangeEmailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChangeEmailFragment extends Fragment implements SimpleCallerUserInterface{

    private static final String USER = "user";

    private UserDTO user;
    private UserDTO updatedUser;
    private UserService userService;
    private OnChangeEmailFragmentInteractionListener mListener;
    private AlertDialog alertDialog;
    private View dialogView;

    @BindView(R.id.et_change_email) public EditText changeEmail;
    @BindView(R.id.til_change_email) public TextInputLayout tilChangeEmail;
    @BindView(R.id.btn_change_email) public Button btnChangeEmail;

    public ChangeEmailFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ChangeEmailFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ChangeEmailFragment newInstance(UserDTO user) {
        ChangeEmailFragment fragment = new ChangeEmailFragment();
        Bundle args = new Bundle();
        args.putSerializable(USER, user);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            user = (UserDTO) getArguments().getSerializable(USER);
        }
        userService = new UserService(this.getContext());
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.change_email_fragment, container, false);
        ButterKnife.bind(this, view);
        changeEmail.setText(user.getEmail());

        AlertDialog.Builder builder = new AlertDialog.Builder(this.getContext());
        builder.setTitle(R.string.password_confirm);
        builder.setMessage(R.string.password_confirm_msg);
        LayoutInflater layoutInflater = LayoutInflater.from(this.getContext());
        dialogView = layoutInflater.inflate(R.layout.password_confirm_dialog_layout, null);
        builder.setView(dialogView);
        builder.setCancelable(true);
        builder.setPositiveButton(R.string.ok, null);
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog = builder.create();
        return view;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnChangeEmailFragmentInteractionListener) {
            mListener = (OnChangeEmailFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnChangeEmailFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @OnTextChanged(R.id.et_change_email)
    public void emailChange() {
        String email = changeEmail.getText().toString();
        if (email.equals(user.getEmail())) {
            btnChangeEmail.setEnabled(false);
            btnChangeEmail.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.disabled_button, null));
        } else {
            btnChangeEmail.setEnabled(true);
            btnChangeEmail.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.round_button, null));
        }
        tilChangeEmail.setError("");
        tilChangeEmail.setErrorEnabled(true);
    }

    @OnClick(R.id.btn_change_email)
    public void emailUpdate() {
        String email = changeEmail.getText().toString();
        if(isValidEmail(email)) {
            userService.checkEmail(email, this);
        }
    }

    public boolean isValidEmail(CharSequence email) {
        EmailValidator ev = new EmailValidator();

        if(email.toString().isEmpty()) {
            tilChangeEmail.setErrorEnabled(true);
            tilChangeEmail.setError(getText(R.string.empty_email_message));
            return false;
        }
        else if(!ev.isValid(changeEmail.getText()))
        {
            tilChangeEmail.setErrorEnabled(true);
            tilChangeEmail.setError(getText(R.string.invalid_email_format_message));
            return false;
        }
        tilChangeEmail.setErrorEnabled(false);
        tilChangeEmail.setError("");
        return true;
    }

    @Override
    public void requestStatus(boolean success, String message) {
        if(success && message != null) {
            if (message.equals("Email is valid.")) {
                alertDialog.show();
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final EditText pass = (EditText) dialogView.findViewById(R.id.et_verify_pass);
                        final TextInputLayout tilPass = (TextInputLayout) dialogView.findViewById(R.id.til_verify_pass);
                        pass.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                            }

                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                                tilPass.setErrorEnabled(false);
                                tilPass.setError("");
                            }

                            @Override
                            public void afterTextChanged(Editable s) {

                            }
                        });
                        if (pass.getText().toString().isEmpty()) {
                            tilPass.setErrorEnabled(true);
                            tilPass.setError("Password must not be empty.");
                        } else {
                            String email = changeEmail.getText().toString();
                            updatedUser = new UserDTO();
                            updatedUser.setConfirmPassword(pass.getText().toString());
                            updatedUser.setEmail(email);
                            userService.updateUser(updatedUser, ChangeEmailFragment.this);
                        }
                    }
                });
            } else if (message.equals("User successfully updated.")) {
                user.setEmail(updatedUser.getEmail());
                userService.loginPost(this, updatedUser.getEmail(), updatedUser.getConfirmPassword());
            }
        } else if (success && message == null){
            alertDialog.dismiss();
            btnChangeEmail.setEnabled(false);
            btnChangeEmail.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.disabled_button, null));
            Toast.makeText(getContext(), "Email successfully updated", Toast.LENGTH_LONG).show();
            mListener.onChangeEmailDone(user);
        } else {
            try {
                JsonObject obj = new JsonParser().parse(message).getAsJsonObject();
                String msg = obj.get("confirmPassword").getAsString();
                if (msg != null) {
                    TextInputLayout tilPass = (TextInputLayout) alertDialog.findViewById(R.id.til_verify_pass);
                    tilPass.setErrorEnabled(true);
                    tilPass.setError(msg);
                } else {
                    tilChangeEmail.setErrorEnabled(true);
                    tilChangeEmail.setError(message);
                }
            } catch(JsonSyntaxException e){
                tilChangeEmail.setErrorEnabled(true);
                tilChangeEmail.setError(message);
            }


        }
    }


    public interface OnChangeEmailFragmentInteractionListener {
        // TODO: Update argument type and name
        void onChangeEmailDone(UserDTO user);
    }
}
