package tappluxe.b28.action.aws.com.tappluxeapplication.ui.sockets;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import retrofit2.Call;
import tappluxe.b28.action.aws.com.tappluxeapplication.MainActivity;
import tappluxe.b28.action.aws.com.tappluxeapplication.R;
import tappluxe.b28.action.aws.com.tappluxeapplication.ui.device.ListDeviceActivity;
import tappluxe.b28.action.aws.com.tappluxeapplication.ui.main.ScheduleFragment;
import tappluxe.b28.action.aws.com.tappluxeapplication.ui.main.TabHolder;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.adapter.DeviceSpinnerAdapter;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.adapter.ScheduleSpinnerItemAdapter;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.FormInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.ListDeviceInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.ListScheduleInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.MultipleSourceSimpleCallerUserInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.DeviceDTO;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.ScheduleDTO;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.UnitDTO;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.UnitSocketDTO;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.service.DeviceService;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.service.ScheduleService;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.service.SocketService;

public class UpdateSocket extends AppCompatActivity implements ListDeviceInterface, ListScheduleInterface, MultipleSourceSimpleCallerUserInterface, FormInterface {

	private DeviceService deviceService;
	private ScheduleService scheduleService;
	private SocketService socketService;
	private UnitDTO unit;
	private Long deviceID;
	private UnitSocketDTO socket;
	private ScheduleDTO schedule;
	private Call callDevice;
	private Call callSchedule;
	private Call callSocketSchedule;
	private Call callSocketDevice;

	private boolean deviceIsPopulating = true;
	private boolean scheduleIsPopulating = true;
	private static final String DEVICESOURCE = "DEVICESOURCE";
	private static final String SCHEDULESOURCE = "SCHEDULESOURCE";

	@BindView(R.id.manage_devices)
	public TextView manageDevices;
	@BindView(R.id.schedule_time_text_view)
	public TextView scheduleTime;
	@BindView(R.id.schedule_days_text_view)
	public TextView scheduleDays;
	@BindView(R.id.device_list_spinner)
	public Spinner deviceList;
	@BindView(R.id.schedule_list_spinner)
	public Spinner scheduleList;
	@BindView(R.id.text_error_message_device)
	public TextView errorDevice;
	@BindView(R.id.text_error_message_schedule)
	public TextView errorSchedule;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.socket_update_activity);
		ButterKnife.bind(this);

		scheduleService = new ScheduleService(this);
		deviceService = new DeviceService(this);
		socketService = new SocketService(this);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);

		Intent intent = getIntent();
		unit = (UnitDTO) intent.getSerializableExtra("unit");
		socket = (UnitSocketDTO) intent.getSerializableExtra("socket");
		schedule = socket.getSchedule();
		deviceID = socket.getDeviceId();

		setTitle(unit.getName(), socket.getSocketNumber());
		scheduleList.setAdapter(new ScheduleSpinnerItemAdapter(this, new ArrayList<ScheduleDTO>()));
		deviceList.setAdapter(new DeviceSpinnerAdapter(new ArrayList<DeviceDTO>(), this));
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				this.finish();
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onStart() {
		super.onStart();
		refresh();
	}

	protected void refresh() {
		setScheduleDisplay(schedule);
		onCancelCall();
		callSchedule = scheduleService.getScheduleList(this);
		callDevice = deviceService.getDeviceList(this);
	}

	protected void onCancelCall() {
		if (callDevice != null && !callDevice.isCanceled())
			callDevice.cancel();
		if (callSchedule != null && !callSchedule.isCanceled())
			callSchedule.cancel();
		if (callSocketDevice != null && !callSocketDevice.isCanceled())
			callSocketDevice.cancel();
		if (callSocketSchedule != null && !callSocketSchedule.isCanceled())
			callSocketSchedule.cancel();
	}

	@Override
	protected void onStop() {
		super.onStop();
		onCancelCall();
	}

	public void setTitle(String unitName, int socketNumber) {
		setTitle(unitName + "'s " + getString(R.string.socket) + " " + socketNumber);
	}

	@OnClick(R.id.manage_devices)
	public void manageDevice() {
		Intent intent = new Intent(this, ListDeviceActivity.class);
		startActivity(intent);
	}

	@OnClick(R.id.manage_schedule)
	public void manageSchedule() {
		Intent intent = new Intent(this, MainActivity.class);
		intent.putExtra(TabHolder.GO_TO_FRAGMENT, ScheduleFragment.GO_TO_SCHEDULE_FRAGMENT);
		intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
	}

	private void setScheduleDisplay(ScheduleDTO scheduleDTO) {
		if (scheduleDTO == null || scheduleDTO.getId() == null) {
			scheduleTime.setText(getString(R.string.recieve_time_format));
			scheduleDays.setText("Days");
			scheduleTime.setVisibility(View.GONE);
			scheduleDays.setVisibility(View.GONE);
			return;
		}

		String[] numdays = scheduleDTO.getRepeat().split("\\|");
		ArrayList<String> days = new ArrayList<>();
		for (int i = 0; i < numdays.length; i++) {
			switch (Integer.parseInt(numdays[i])) {
				case 0:
					days.add("Sun");
					break;
				case 1:
					days.add("Mon");
					break;
				case 2:
					days.add("Tue");
					break;
				case 3:
					days.add("Wed");
					break;
				case 4:
					days.add("Thurs");
					break;
				case 5:
					days.add("Fri");
					break;
				case 6:
					days.add("Sat");
					break;
			}
		}
		scheduleDays.setText(TextUtils.join(", ", days));
		String newTime = null;
		try {
			Date time;
			DateFormat format = new SimpleDateFormat(getString(R.string.recieve_time_format));
			time = format.parse(scheduleDTO.getTime());
			Calendar calendar = Calendar.getInstance();
			if (time == null)
				Log.e(getClass().getName(), "time null");
			if (calendar == null)
				Log.e(getClass().getName(), " calendar null");
			calendar.set(Calendar.HOUR_OF_DAY, time.getHours());
			calendar.set(Calendar.MINUTE, time.getMinutes());
			newTime = (calendar.get(Calendar.HOUR) == 0 ? 12 : calendar.get(Calendar.HOUR)) + ":" + ((calendar.get(Calendar.MINUTE) < 10) ? "0" + calendar.get(Calendar.MINUTE) : calendar.get(Calendar.MINUTE)) + " " + ((calendar.get(Calendar.AM_PM) == Calendar.PM) ? "PM" : "AM");

		} catch (ParseException e) {
			Toast.makeText(this, "Invalid Time Format recieved from Server", Toast.LENGTH_LONG).show();
		}

		scheduleTime.setText(newTime);
		scheduleTime.setVisibility(View.VISIBLE);
		scheduleDays.setVisibility(View.VISIBLE);
	}

	@Override
	public void setListSchedule(List<ScheduleDTO> schedules) {
		scheduleIsPopulating = true;
		//ScheduleSpinnerItemAdapter scheduleSpinnerItemAdapter = new ScheduleSpinnerItemAdapter(this, schedules);
		ArrayAdapter<ScheduleDTO> scheduleDTOArrayAdapter = new ArrayAdapter<ScheduleDTO>(this, android.R.layout.simple_spinner_item, schedules);
		schedules.add(0, new ScheduleDTO());
		int i = 0;
		if (schedule != null) {
			boolean notsame = true;
			for (; i < schedules.size() && notsame; i++) {
				if (schedule.getId().equals(schedules.get(i).getId())) {
					notsame = false;
					i--;
				}
			}
			if (notsame)
				i = 0;
		}
		scheduleDTOArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		scheduleList.setAdapter(scheduleDTOArrayAdapter);
		scheduleList.setSelection(i);
	}

	@Override
	public void setListDevice(List<DeviceDTO> devices) {
		deviceIsPopulating = true;
		ArrayAdapter<DeviceDTO> deviceDTOArrayAdapter = new ArrayAdapter<DeviceDTO>(this, android.R.layout.simple_spinner_item, devices);
		devices.add(0, new DeviceDTO());
		int i = 0;
		if (deviceID != null) {
			boolean notsame = true;
			for (; i < devices.size() && notsame; i++) {
				if (deviceID.equals(devices.get(i).getId())) {
					notsame = false;
					i--;
				}
			}
			if (notsame)
				i = 0;
		}
		deviceDTOArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		deviceList.setAdapter(deviceDTOArrayAdapter);
		deviceList.setSelection(i);
	}


	@OnItemSelected(R.id.schedule_list_spinner)
	public void onScheduleSpinnerItemSelected() {
		errorSchedule.setVisibility(View.GONE);
		if(!scheduleIsPopulating){
			ScheduleDTO schedule = (ScheduleDTO) scheduleList.getSelectedItem();
			setScheduleDisplay(schedule);

			Long scheduleId = schedule.getId();
			if (scheduleId == null) {
				callSocketSchedule = socketService.assignSchedule(unit.getId(), socket.getId(), -1l, SCHEDULESOURCE, this);
//			callSocketDevice = socketService.removeSchedule(unit.getId(), socket.getId(), SCHEDULESOURCE, this);
			} else {
				callSocketSchedule = socketService.assignSchedule(unit.getId(), socket.getId(), scheduleId, SCHEDULESOURCE, this);
			}
		}
		else
			scheduleIsPopulating = false;
	}

	@OnItemSelected(R.id.device_list_spinner)
	public void onItemSelectedDevice(){
		errorDevice.setVisibility(View.GONE);
		if(!deviceIsPopulating){
			Long deviceId = ((DeviceDTO) deviceList.getSelectedItem()).getId();
			if (deviceId == null) {
				callSocketDevice = deviceService.attachDevice(socket.getDeviceId(), -1l, DEVICESOURCE, UpdateSocket.this);
			} else {
				callSocketDevice = deviceService.attachDevice(deviceId, socket.getId(), DEVICESOURCE, UpdateSocket.this);
			}
		}
		else
			deviceIsPopulating = false;
	}

	@Override
	public void getError(String message) {
		errorDevice.setVisibility(View.VISIBLE);
		try {
			JsonElement element = new JsonParser().parse(message);
			if (element == null) {
				Toast.makeText(this, message, Toast.LENGTH_LONG).show();
				errorDevice.setText(message);
			} else {
				JsonObject obj = element.getAsJsonObject();
				if (obj == null) {
					Toast.makeText(this, message, Toast.LENGTH_LONG).show();
					errorDevice.setText(message);
				} else {
					JsonElement messageEl = obj.get("message");
					if (messageEl != null && !messageEl.getAsString().isEmpty()) {
						Toast.makeText(this, messageEl.getAsString(), Toast.LENGTH_LONG).show();
						errorDevice.setText(message);
					} else {
						Toast.makeText(this, message, Toast.LENGTH_LONG).show();
						errorDevice.setText(message);
					}
				}
			}
		} catch (JsonSyntaxException | IllegalStateException err) {
			Log.e(getClass().getName(), err.getMessage());
			Toast.makeText(this, message, Toast.LENGTH_LONG).show();
		}
	}


	@Override
	public void clearForm() {
	}

	@Override
	public void enableForm(boolean enabled) {
		deviceList.setEnabled(enabled);
		scheduleList.setEnabled(enabled);
	}

	@Override
	public void requestStatus(boolean success, String message, String source) {
		if (success) {
			Log.d(getClass().getName(), "test toast: " + message);
			Toast.makeText(this, message, Toast.LENGTH_LONG).show();
			switch (source){
				case DEVICESOURCE: {
					deviceID = ((DeviceDTO)deviceList.getSelectedItem()).getId();
				}break;
				case SCHEDULESOURCE: {
					schedule = ((ScheduleDTO)scheduleList.getSelectedItem());
				} break;
			}
		} else {
			String showmessage = message;
			try {
				JsonElement element = new JsonParser().parse(message);
				if (element != null) {
					JsonObject obj = element.getAsJsonObject();
					if (obj != null) {
						JsonElement generalMessage = obj.get("message");
						JsonElement userUnitSocketId = obj.get("userUnitSocketId");
						if (generalMessage != null && !generalMessage.getAsString().isEmpty()) {
							showmessage = generalMessage.getAsString();
						}
						else if (userUnitSocketId != null && !userUnitSocketId.getAsString().isEmpty()) {
							showmessage = userUnitSocketId.getAsString();
						}
					}
				}
			} catch (JsonSyntaxException | IllegalStateException err) {
				err.printStackTrace();
			}
			switch (source){
				case DEVICESOURCE: {
					errorDevice.setVisibility(View.VISIBLE);
					errorDevice.setText(showmessage);
				}break;
				case SCHEDULESOURCE: {
					errorSchedule.setVisibility(View.VISIBLE);
					errorSchedule.setText(showmessage);
				} break;
				default:{
					Toast.makeText(this, showmessage, Toast.LENGTH_LONG).show();break;
				}
			}
		}
	}
}
