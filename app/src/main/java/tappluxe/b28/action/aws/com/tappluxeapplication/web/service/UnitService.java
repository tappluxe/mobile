package tappluxe.b28.action.aws.com.tappluxeapplication.web.service;

import android.content.Context;
import android.util.Log;
import android.widget.Button;
import android.widget.ProgressBar;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.GetUnitStatusInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.GetUnitInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.ListUnitInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.SimpleCallerUserInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.session.Session;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.UnitDTO;

/**
 * Created by ariel.fabilena on 2/20/2017.
 */

public class UnitService extends BaseService {
	private static final String TAG = UnitService.class.getName();

	public UnitService(Context activity) {
		super(activity);
	}

	public Call getUnitList(final ListUnitInterface activity) {
		Call<List<UnitDTO>> call = webServiceInterface.getUnitList(session.getValue(Session.ACCESS_TOKEN));

		call.enqueue(new Callback<List<UnitDTO>>() {

			@Override
			public void onResponse(Call<List<UnitDTO>> call, Response<List<UnitDTO>> response) {
				if (response.isSuccessful()) {
					String str = response.body().toString();

					Log.d(TAG, "UnitService.getUnitList - successully fetched List of Units");

					activity.setListUnit(response.body());
				} else {
					//get error message
					try {
						String str = response.errorBody().string();
						Log.e(TAG, "error code: " + response.code()+ " onResponse: " + str);
						activity.getError(str);

						Log.e(TAG, "onResponse: " + str);

					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

			@Override
			public void onFailure(Call<List<UnitDTO>> call, Throwable t) {
				Log.e(TAG, t.getClass().getName());
				if(t.getMessage() != null)
					Log.e(TAG, t.getMessage());
				t.printStackTrace();
				activity.getError("Cannot connect to the server.");
			}
		});

		return call;
	}

	public Call getUnit(Long unitId, final GetUnitInterface activity) {
		Call<UnitDTO> call = webServiceInterface.getUnit(unitId, session.getValue(Session.ACCESS_TOKEN));

		call.enqueue(new Callback<UnitDTO>() {

			@Override
			public void onResponse(Call<UnitDTO> call, Response<UnitDTO> response) {
				if (response.isSuccessful()) {
					String str = response.body().toString();

					Log.d(TAG, "UnitService.getUnitList - successully fetched List of Units");

					activity.setUnit("Success",response.body());
				} else {
					//get error message
					try {
						String str = response.errorBody().string();
						Log.e(TAG, "error code: " + response.code()+ " onResponse: " + str);
						activity.setError(str);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

			@Override
			public void onFailure(Call<UnitDTO> call, Throwable t) {
				Log.e(TAG, t.getClass().getName());
				if(t.getMessage() != null)
					Log.e(TAG, t.getMessage());
				t.printStackTrace();
				activity.setError("Cannot connect to the server.");
			}
		});

		return call;
	}

	public void addUnitPost(String productKey, String unitName, final SimpleCallerUserInterface activity) {
		Call<UnitDTO> call = webServiceInterface.postAddUnit(new UnitDTO(productKey, unitName), session.getValue(Session.ACCESS_TOKEN));

		call.enqueue(new Callback<UnitDTO>() {
			@Override
			public void onResponse(Call<UnitDTO> call, Response<UnitDTO> response) {
				if (response.isSuccessful()) {
					Log.d(TAG, "UnitService.addUnitPost - successfully added a Unit");
					activity.requestStatus(true, "Successully Added a Unit");
				} else {
					//get error message
					try {
						String str = response.errorBody().string();
						Log.e(TAG, "error code: " + response.code()+ " onResponse: " + str);
						activity.requestStatus(false, str);
					} catch (IOException e) {
						activity.requestStatus(false, e.getMessage());

						e.printStackTrace();
					}
				}
			}

			@Override
			public void onFailure(Call<UnitDTO> call, Throwable t) {
				Log.e(TAG, t.getClass().getName());
				if(t.getMessage() != null)
					Log.e(TAG, t.getMessage());
				t.printStackTrace();
				activity.requestStatus(false, "Cannot connect to the server.");
			}
		});
	}

	public void updateUnit(Long userUnitId, String unitName, final SimpleCallerUserInterface activity) {
		Call<UnitDTO> call = webServiceInterface.putUpdateUnit(new UnitDTO(unitName), userUnitId, session.getValue(Session.ACCESS_TOKEN));

		call.enqueue(new Callback<UnitDTO>() {
			@Override
			public void onResponse(Call<UnitDTO> call, Response<UnitDTO> response) {
				if (response.isSuccessful()) {
					Log.d(TAG, "UnitService.updateUnit - successfully updated a Unit");
					activity.requestStatus(true, "Successfully Updated Unit");
				} else {
					//get error message
					try {
						String str = response.errorBody().string();
						Log.e(TAG, "error code: " + response.code()+ " onResponse: " + str);
						activity.requestStatus(false, str);
					} catch (IOException e) {
						activity.requestStatus(false, e.getMessage());
						e.printStackTrace();
					}
				}
			}

			@Override
			public void onFailure(Call<UnitDTO> call, Throwable t) {
				Log.e(TAG, t.getClass().getName());
				if(t.getMessage() != null)
					Log.e(TAG, t.getMessage());
				t.printStackTrace();
				activity.requestStatus(false, "Cannot connect to the server.");
			}
		});
	}

	public void updateSSID(Long userUnitId, String ssid, final SimpleCallerUserInterface activity) {
		UnitDTO unitDTO = new UnitDTO();
		unitDTO.setSsid(ssid);
		Call<UnitDTO> call = webServiceInterface.putUpdateUnit(unitDTO, userUnitId, session.getValue(Session.ACCESS_TOKEN));
        call.enqueue(new Callback<UnitDTO>() {
            @Override
            public void onResponse(Call<UnitDTO> call, Response<UnitDTO> response) {
                if (response.isSuccessful()) {
                    Log.d(TAG, "UnitService.updateUnit - successfully updated a Unit");
                    activity.requestStatus(true, "Successfully Updated Unit");
                } else {
                    //get error message
                    try {
                        String str = response.errorBody().string();
                        Log.e(TAG, "error code: " + response.code()+ " onResponse: " + str);
                        activity.requestStatus(false, str);
                    } catch (IOException e) {
                        activity.requestStatus(false, e.getMessage());
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<UnitDTO> call, Throwable t) {
                Log.e(TAG, t.getClass().getName());
                if(t.getMessage() != null)
                    Log.e(TAG, t.getMessage());
                t.printStackTrace();
                activity.requestStatus(false, "Cannot connect to the server.");
            }
        });
	}

	public void deleteUnit(Long userUnitId, final SimpleCallerUserInterface activity) {
		Call<UnitDTO> call = webServiceInterface.deleteUnit(userUnitId,
				session.getValue(Session.ACCESS_TOKEN));

		call.enqueue(new Callback<UnitDTO>() {
			@Override
			public void onResponse(Call<UnitDTO> call, Response<UnitDTO> response) {
				if (response.isSuccessful()) {
					Log.d(TAG, "UnitService.deleteUnit - successfully deleted a Unit");
					activity.requestStatus(true, "Successfully Deleted Unit");
				} else {
					//get error message
					try {
						String str = response.errorBody().string();
						Log.e(TAG, "error code: " + response.code()+ " onResponse: " + str);
						activity.requestStatus(false, str);
					} catch (IOException e) {
						activity.requestStatus(false, e.getMessage());

						e.printStackTrace();
					}
				}
			}

			@Override
			public void onFailure(Call<UnitDTO> call, Throwable t) {
				Log.e(TAG, t.getClass().getName());
				if(t.getMessage() != null)
					Log.e(TAG, t.getMessage());
				t.printStackTrace();
				activity.requestStatus(false, "Cannot connect to the server.");
			}
		});
	}

	public void checkProductKey(String productKey, final SimpleCallerUserInterface activity) {
		Call<UnitDTO> call = webServiceInterface.checkProductKey(productKey);

		call.enqueue(new Callback<UnitDTO>() {
			@Override
			public void onResponse(Call<UnitDTO> call, Response<UnitDTO> response) {
				Log.d(TAG, response.message());
				if (response.isSuccessful()) {
					Log.d(TAG, "UnitService.checkProductKey - " +
							"successfully checked product key of a Unit");
					activity.requestStatus(true, "Product key is valid.");
				} else {
					//get error message
					try {
						String str = response.errorBody().string();
						JsonObject obj = new JsonParser().parse(str).getAsJsonObject();
						Log.e(TAG, "error code: " + response.code()+ " onResponse: " + str);
						activity.requestStatus(false, obj.get("message").getAsString());
					} catch (IOException e) {
						activity.requestStatus(false, e.getMessage());

						e.printStackTrace();
					}
				}
			}

			@Override
			public void onFailure(Call<UnitDTO> call, Throwable t) {
				Log.e(TAG, t.getClass().getName());
				if(t.getMessage() != null)
					Log.e(TAG, t.getMessage());
				t.printStackTrace();
				activity.requestStatus(false, "Cannot connect to the server.");
			}
		});
	}

	public void getSocketStatus(Long userUnitId, final UnitDTO unit, final GetUnitStatusInterface activity, final ProgressBar progressBar, final Button btnLed) {
		Call<ResponseBody> call = webServiceInterface.socketStatus(userUnitId,
				session.getValue(Session.ACCESS_TOKEN));

		call.enqueue(new Callback<ResponseBody>() {
			@Override
			public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
				if (response.isSuccessful()) {
					Log.e(TAG, Integer.toString(response.code()));
					activity.requestUnitStatus(true, Integer.toString(response.code()), unit, progressBar, btnLed);
				} else {
					//get error message
					try {
						String str = response.errorBody().string();
						Log.e(TAG, "error code: " + response.code()+ " onResponse: " + str);
						if(response.code() == 429) {
							JsonObject obj = new JsonParser().parse(str).getAsJsonObject();
							activity.requestUnitStatus(false, obj.get("message").getAsString(), unit, progressBar, btnLed);
						}else if(response.code() == 501 || response.code() == 503){
							activity.requestUnitStatus(false, Integer.toString(response.code()), unit, progressBar, btnLed);
						}
//						JsonObject obj = new JsonParser().parse(str).getAsJsonObject();
//						activity.requestStatus(false, obj.get("message").getAsString());
					} catch (IOException e) {
						activity.requestUnitStatus(false, e.getMessage(), unit, progressBar, btnLed);

						e.printStackTrace();
					}
				}
			}


			@Override
			public void onFailure(Call<ResponseBody> call, Throwable t) {
				Log.e(TAG, t.getClass().getName());
				if(t.getMessage() != null)
					Log.e(TAG, t.getMessage());
				t.printStackTrace();
				activity.requestUnitStatus(false, "Cannot connect to the server.", unit, progressBar, btnLed);
			}
		});
	}

	public void refreshSocketStatus(Long userUnitId, final UnitDTO unit, final GetUnitStatusInterface activity, final ProgressBar progressBar, final Button btnLed) {
		Call<ResponseBody> call = webServiceInterface.refreshSocketStatus(userUnitId,
				session.getValue(Session.ACCESS_TOKEN));

		call.enqueue(new Callback<ResponseBody>() {
			@Override
			public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
				if (response.isSuccessful()) {
					Log.e(TAG, Integer.toString(response.code()));
					activity.requestUnitStatus(true, Integer.toString(response.code()), unit, progressBar, btnLed);
				} else {
					//get error message
					try {
						String str = response.errorBody().string();
						Log.e(TAG, "error code: " + response.code()+ " onResponse: " + str);
						if(response.code() == 429) {
							JsonObject obj = new JsonParser().parse(str).getAsJsonObject();
							activity.requestUnitStatus(false, obj.get("message").getAsString(), unit, progressBar, btnLed);
						}else if(response.code() == 501 || response.code() == 503){
							activity.requestUnitStatus(false, Integer.toString(response.code()), unit, progressBar, btnLed);
						}
//						JsonObject obj = new JsonParser().parse(str).getAsJsonObject();
//						activity.requestStatus(false, obj.get("message").getAsString());
					} catch (IOException e) {
						activity.requestUnitStatus(false, e.getMessage(), unit, progressBar, btnLed);

						e.printStackTrace();
					}
				}
			}

			@Override
			public void onFailure(Call<ResponseBody> call, Throwable t) {
				Log.e(TAG, t.getClass().getName());
				if(t.getMessage() != null)
					Log.e(TAG, t.getMessage());
				t.printStackTrace();
				activity.requestUnitStatus(false, "Cannot connect to the server.", unit, progressBar, btnLed);
			}
		});
	}
}
