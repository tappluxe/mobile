package tappluxe.b28.action.aws.com.tappluxeapplication.utility.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import tappluxe.b28.action.aws.com.tappluxeapplication.R;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.DeviceDTO;

/**
 * Created by ariel.fabilena on 3/14/2017.
 */

public class DeviceSpinnerAdapter extends ArrayAdapter<DeviceDTO> {

	public DeviceSpinnerAdapter (List<DeviceDTO> objects, Activity parentActivity) {
		super(parentActivity.getApplicationContext(), R.layout.spinner_item, objects);
	}

	@NonNull
	@Override
	public View getView(int position, @Nullable View convertView, @NonNull final ViewGroup parent) {
		final DeviceDTO device = getItem(position);
		if(convertView == null){
			convertView = LayoutInflater.from(getContext()).inflate(R.layout.spinner_item, parent, false);
		}
		TextView name = (TextView) convertView.findViewById(R.id.spinner_name);
		name.setText(device.getName());
		return convertView;
	}
}
