package tappluxe.b28.action.aws.com.tappluxeapplication.web.dto;

import java.io.Serializable;
import java.util.List;

/**
 * Created by benjo.uriarte on 3/10/2017.
 */

public class UnitSocketDTO implements Serializable {

	private Long id;
	private Integer socketNumber;
	private Long userUnitId;
	private Long deviceId;
	private ScheduleDTO schedule;
	private List<DeviceStatusDTO> statuses;

	public UnitSocketDTO(Long id, Integer socketNumber, Long userUnitId, Long deviceId, ScheduleDTO schedule, List<DeviceStatusDTO> statuses) {
		this.id = id;
		this.socketNumber = socketNumber;
		this.userUnitId = userUnitId;
		this.deviceId = deviceId;
		this.schedule = schedule;
		this.statuses = statuses;
	}

	public Long getId() {
		return id;
	}

	public Integer getSocketNumber() {
		return socketNumber;
	}

	public void setSocketNumber(Integer socketNumber) {
		this.socketNumber = socketNumber;
	}

	public Long getUserUnitId() {
		return userUnitId;
	}

	public void setUserUnitId(Long userUnitId) {
		this.userUnitId = userUnitId;
	}

	public Long getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(Long deviceId) {
		this.deviceId = deviceId;
	}

	public List<DeviceStatusDTO> getStatuses() {
		return statuses;
	}

	public void setStatuses(List<DeviceStatusDTO> statuses) {
		this.statuses = statuses;
	}

	public ScheduleDTO getSchedule() {
		return schedule;
	}

	public void setSchedule(ScheduleDTO schedule) {
		this.schedule = schedule;
	}
}
