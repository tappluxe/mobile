package tappluxe.b28.action.aws.com.tappluxeapplication.ui.unit.setup;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiConfiguration.KeyMgmt;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.widget.Button;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import tappluxe.b28.action.aws.com.tappluxeapplication.BaseActivity;
import tappluxe.b28.action.aws.com.tappluxeapplication.R;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.UnitDTO;

/**
 * Created by jay.bilocura on 3/21/2017.
 */

public class UnitConnectionActivity extends BaseActivity {

    private String networkSSID;
    private String networkPass;
    private UnitDTO unit;
    public Thread t;
    public boolean reconfigure;
    private static final int CONNECTION_FAILED = -1;
    private static final int CONNECTION_STARTED = 0;
    private static final int CONNECTION_FINISHED = 1;
    private ProgressDialog progressDialog;
    private WifiManager wifiManager;
    private String homeSsid;

    @BindView(R.id.btn_unit_wifi_connect) public Button btnConnect;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.unit_connection_layout);
        setTitle("Unit Connection Setup");
        ButterKnife.bind(this);
        Intent intent = getIntent();
        unit = (UnitDTO) intent.getSerializableExtra("unit");
        reconfigure = intent.getBooleanExtra("reconfigure", false);
        networkSSID = "tplx" + unit.getManufacturedUnitDetails().getProductId();
        networkPass = "12345678";
        progressDialog = new ProgressDialog(UnitConnectionActivity.this);
        wifiManager = (WifiManager) this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        homeSsid = wifiInfo.getSSID();
    }

    private Handler handler = new Handler(){
        @Override public void handleMessage(Message msg) {
            switch (msg.what) {
                case CONNECTION_STARTED:
                    progressDialog.setTitle("Connecting");
                    progressDialog.setMessage("Please wait...");
                    progressDialog.setCancelable(true);
                    progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            t.interrupt();
                            reconnect();
                        }
                    });

                    progressDialog.show();
                    break;
                case CONNECTION_FINISHED:
                    if(progressDialog.isShowing()){
                        progressDialog.dismiss();
                    }
                    Intent intent = new Intent(UnitConnectionActivity.this, InputSSIDActivity.class);
                    intent.putExtra("unit", unit);
                    intent.putExtra("reconfigure", reconfigure);
                    intent.putExtra("homeSsid", homeSsid);
                    if (reconfigure) {
                        intent.addFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
                    }
                    startActivity(intent);
                    finish();
                    break;
                case CONNECTION_FAILED:
                    if(progressDialog.isShowing()){
                        progressDialog.dismiss();
                    }
                    AlertDialog.Builder dialog = new AlertDialog.Builder(UnitConnectionActivity.this);
                    dialog.setTitle("Connection Failed");
                    dialog.setMessage("Your device cannot discover your Tappluxe unit's network. Please make sure you are near your unit.");
                    dialog.setCancelable(true);
                    dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    dialog.setPositiveButton("Try Again", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            connectWifi();
                        }
                    });
                    dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            reconnect();
                        }
                    });
                    dialog.create().show();
                    break;
            }
        }
    };

    @OnClick(R.id.btn_unit_wifi_connect)
    public void connectWifi() {
        Message m = new Message();
        m.what = CONNECTION_STARTED;
        handler.sendMessage(m);
        WifiConfiguration conf = new WifiConfiguration();
        conf.SSID = "\"" + networkSSID + "\"";
        conf.preSharedKey = "\""+ networkPass +"\"";
        conf.allowedKeyManagement.set(KeyMgmt.WPA_PSK);
        wifiManager.addNetwork(conf);

        boolean connected = false;
        List<WifiConfiguration> list = wifiManager.getConfiguredNetworks();
        for( WifiConfiguration i : list ) {
            System.err.println(i.SSID);
            if (i.SSID != null && i.SSID.equals("\"" + networkSSID + "\"")) {
                wifiManager.disconnect();
                wifiManager.enableNetwork(i.networkId, true);
                connected = wifiManager.reconnect();
                break;
            }
        }
        if (connected) {
            t = new Thread() {
                @Override
                public void run(){
                    int tries = 0;
                    ConnectivityManager Cm = (ConnectivityManager) getApplicationContext().getSystemService(CONNECTIVITY_SERVICE);
                    NetworkInfo Ni = Cm.getActiveNetworkInfo();
                    while (Ni == null) {
                        Ni = Cm.getActiveNetworkInfo();
                        tries++;
                        try {
                            Thread.sleep(2000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        if (tries > 5) {
                            break;
                        }
                    }
                    Message m = new Message();
                    if (Ni != null && Ni.isConnected()) {
                        m.what = CONNECTION_FINISHED;
                    } else {
                        m.what = CONNECTION_FAILED;
                    }
                    handler.sendMessage(m);
                }
            };
            t.start();

        } else {
            m = new Message();
            m.what = CONNECTION_FAILED;
            handler.sendMessage(m);
        }
    }

    public void reconnect(){
        List<WifiConfiguration> list = wifiManager.getConfiguredNetworks();
        for( WifiConfiguration i : list ) {
            if (i.SSID != null && i.SSID.equals(homeSsid)) {
                wifiManager.disconnect();
                wifiManager.enableNetwork(i.networkId, true);
                wifiManager.reconnect();
                break;
            }
        }
    }


}
