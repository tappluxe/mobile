package tappluxe.b28.action.aws.com.tappluxeapplication.ui.unit;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import tappluxe.b28.action.aws.com.tappluxeapplication.R;
import tappluxe.b28.action.aws.com.tappluxeapplication.ui.unit.setup.UnitConnectionActivity;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.adapter.SocketListItemAdapter;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.GetUnitInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.ListViewParentInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.session.Session;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.UnitDTO;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.service.UnitService;

public class ViewUnit extends AppCompatActivity implements GetUnitInterface, ListViewParentInterface {
	@BindView(R.id.lv_view_all_devices)
	ListView lvViewAllDevices;

	private SocketListItemAdapter socketListItemAdapter;
	private UnitService unitService;
	private Call callUnit;
	private UnitDTO unit;
	private Session session;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.unit_single_view_activity);
		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowHomeEnabled(true);

		ButterKnife.bind(this);
		session = new Session(this);

		unit = (UnitDTO) getIntent().getSerializableExtra("unit");

		if (unit != null) {
			setTitle(unit.getName());
			unitService = new UnitService(this);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.unit_options_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
		switch(item.getItemId()){
			case android.R.id.home:
				this.finish();
				return true;
			case R.id.menu_unit_refresh:
				refresh();
				return true;
			case R.id.menu_unit_update_name:
                intent = new Intent(this, UpdateUnit.class);
				intent.putExtra("unit", unit);
				startActivity(intent);
				return true;
            case R.id.menu_unit_network_config:
                intent = new Intent(this, NetworkConfigurationActivity.class);
                intent.putExtra("unit", unit);
                startActivity(intent);
                return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onStart() {
		super.onStart();
        String ssid = unit.getSsid();
        if (ssid == null || ssid.isEmpty()) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setTitle(R.string.unit_ssid_setup_title);
            dialog.setMessage(R.string.unit_ssid_setup_msg);
            dialog.setCancelable(true);
            dialog.setPositiveButton(R.string.config, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(getApplicationContext(), UnitConnectionActivity.class);
                    intent.putExtra("unit", unit);
                    startActivity(intent);
                    finish();
                }
            });
			dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
				@Override
				public void onCancel(DialogInterface dialog) {
					finish();
				}
			});
            dialog.create().show();
        } else {
            refresh();
        }

	}

	@Override
	public void refresh() {
		if(unitService != null){
			callUnit = unitService.getUnit(unit.getId(), this);
		}
	}

	//finish();

//	@Override
//	public void turnOffDeviceResult(boolean status, String message, Long socketNumber) {
//		Toast.makeText(this, message, Toast.LENGTH_LONG).show();
//
//		Switch sw = (Switch) lvViewAllDevices.getChildAt(((int) (long) socketNumber) - 1).findViewById(R.id.switch_device_off);
//
//		if (status) {
//			sw.setChecked(false);
//		} else {
//			sw.setEnabled(true);
//		}
//	}

	@Override
	protected void onStop() {
		super.onStop();

		if (callUnit != null)
			callUnit.cancel();
	}

	@Override
	public void setUnit(String message, UnitDTO unit) {
		this.unit = unit;
		socketListItemAdapter = new SocketListItemAdapter(this, unit.getSockets(), this, unit, this);
		lvViewAllDevices.setAdapter(socketListItemAdapter);

	}

	@Override
	public void setError(String message) {
		try {
			JsonElement element = new JsonParser().parse(message);
			if (element == null) {
				Toast.makeText(this, message, Toast.LENGTH_LONG).show();
			} else {
				JsonObject obj = element.getAsJsonObject();
				if (obj == null) {
					Toast.makeText(this, message, Toast.LENGTH_LONG).show();
				} else {
					JsonElement generalMessage = obj.get("message");
					if(generalMessage != null && !generalMessage.getAsString().isEmpty()){
						Toast.makeText(this, generalMessage.getAsString(), Toast.LENGTH_LONG).show();
					}
					else
						Toast.makeText(this, message, Toast.LENGTH_LONG).show();
				}
			}
		} catch (JsonSyntaxException | IllegalStateException err) {
			Log.e(getClass().getName(), err.getMessage());
			Toast.makeText(this, message, Toast.LENGTH_LONG).show();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		if(session.getValue(Session.UNIT_NAME) != null){
			setTitle(session.getValue(Session.UNIT_NAME));
			session.removeKey(Session.UNIT_NAME);
		}
	}
}
