package tappluxe.b28.action.aws.com.tappluxeapplication.ui.registration;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.MenuItem;

import butterknife.ButterKnife;
import tappluxe.b28.action.aws.com.tappluxeapplication.BaseActivity;
import tappluxe.b28.action.aws.com.tappluxeapplication.R;
import tappluxe.b28.action.aws.com.tappluxeapplication.ui.registration.fragments.CredentialsFragment;
import tappluxe.b28.action.aws.com.tappluxeapplication.ui.registration.fragments.ProductKeyFragment;
import tappluxe.b28.action.aws.com.tappluxeapplication.ui.registration.fragments.ProfileFragment;
import tappluxe.b28.action.aws.com.tappluxeapplication.ui.registration.fragments.UnitNameFragment;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.SimpleCallerUserInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.UserDTO;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.service.UserService;

/**
 * Created by jay.bilocura on 3/18/2017.
 */

public class RegistrationActivity extends BaseActivity
		implements ProductKeyFragment.OnProductKeyFragmentInteractionListener,
		UnitNameFragment.OnUnitNameFragmentInteractionListener,
		CredentialsFragment.OnCredentialsFragmentInteractionListener,
		ProfileFragment.OnProfileFragmentInteractionListener,
		SimpleCallerUserInterface {

	// fragments
	private ProductKeyFragment productKeyFragment;
	private UnitNameFragment unitNameFragment;
	private CredentialsFragment credentialsFragment;
	private ProfileFragment profileFragment;

	private int containerId;
	private UserService userService;

	private String productKey;
	private String unitName;
	private String email;
	private String password;
	private String confirmPassword;
	private String firstName;
	private String lastName;
	private String countryIso;
	private String plainMobileNumber;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ButterKnife.bind(this);
		setContentView(R.layout.registration_layout);
		setTitle(getString(R.string.account_registration));
		containerId = R.id.registration_fragment_container;
		userService = new UserService(this);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		if (findViewById(containerId) != null) {
			if (savedInstanceState != null) {
				return;
			}
			productKeyFragment = new ProductKeyFragment();
			getSupportFragmentManager().beginTransaction()
					.add(containerId, productKeyFragment).commit();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				onBackPressed();
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
            boolean canback = getSupportFragmentManager().getBackStackEntryCount() > 0;
            getSupportActionBar().setDisplayHomeAsUpEnabled(canback);
        } else {
            super.onBackPressed();
        }
    }


	@Override
	public boolean onSupportNavigateUp() {
		getSupportFragmentManager().popBackStack();
		return true;
	}

	public void newFragment(Fragment fragment) {
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		transaction.replace(containerId, fragment);
		transaction.addToBackStack(null);
		transaction.commit();
	}

	@Override
	public void onProductKeyDone(String productKey) {
		this.productKey = productKey;
		unitNameFragment = UnitNameFragment.newInstance(unitName);
		newFragment(unitNameFragment);
	}

	@Override
	public void onUnitNameDone(String unitName) {
		this.unitName = unitName;
		credentialsFragment = CredentialsFragment.newInstance(email, password, confirmPassword);
		newFragment(credentialsFragment);
	}

	@Override
	public void onSaveUnitName(String unitName) {
		this.unitName = unitName;
	}

	@Override
	public void onCredentialsDone(String email, String password, String confirmPassword) {
		this.email = email;
		this.password = password;
		this.confirmPassword = confirmPassword;
		profileFragment = ProfileFragment.newInstance(firstName, lastName, countryIso, plainMobileNumber);
		newFragment(profileFragment);
	}

	@Override
	public void onSaveCredentials(String email, String password, String confirmPassword) {
		this.email = email;
		this.password = password;
		this.confirmPassword = confirmPassword;
	}

	@Override
	public void onProfileFragmentDone(String firstName, String lastName, String countryIso, String plainMobileNumber, String mobileNumber) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.countryIso = countryIso;
		this.plainMobileNumber = plainMobileNumber;
		userService.signUpPost(
				productKey, unitName, email, password, confirmPassword, firstName, lastName,
				mobileNumber, this);
	}

	@Override
	public void onSaveProfile(String firstName, String lastName, String countryIso, String plainMobileNumber) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.countryIso = countryIso;
		this.plainMobileNumber = plainMobileNumber;
	}

	@Override
	public void requestStatus(boolean success, String message) {
		if (success) {
            UserDTO user = new UserDTO();
            user.setEmail(email);
            Intent intent = new Intent(this, CompleteRegistrationActivity.class);
			startActivity(intent);
			finish();
		}
	}
}
