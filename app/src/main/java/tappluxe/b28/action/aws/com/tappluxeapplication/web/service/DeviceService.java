package tappluxe.b28.action.aws.com.tappluxeapplication.web.service;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.GetDeviceInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.ListDeviceInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.MultipleSourceSimpleCallerUserInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.SimpleCallerUserInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.session.Session;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.DeviceDTO;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.interfaces.WebServiceInterface;

/**
 * Created by ariel.fabilena on 2/20/2017.
 */

public class DeviceService extends BaseService{
	private static final String TAG = DeviceService.class.getName();

	public DeviceService(Context activity) {
		super(activity);
	}

//	public Call getDeviceListSpecific(Long unitID, final ListDeviceInterface activity) {
//		Retrofit.Builder builder =
//				new Retrofit.Builder().baseUrl(SERVER_URL).addConverterFactory(GsonConverterFactory.create());
//		Retrofit retrofit = builder.client(httpClientBuilder.build()).build();
//		WebServiceInterface addDeviceInterface = retrofit.create(WebServiceInterface.class);
//		Call<List<DeviceDTO>> call = addDeviceInterface.getDevicesFromUnit(unitID,
//				session.getValue(Session.ACCESS_TOKEN));
//
//		call.enqueue(new Callback<List<DeviceDTO>>() {
//			@Override
//			public void onResponse(Call<List<DeviceDTO>> call, Response<List<DeviceDTO>> response) {
//				if (response.isSuccessful()) {
//					String str = response.body().toString();
//					Log.d(TAG, "DeviceService.getDeviceListSpecific - " +
//							"successfully fetched List of Devices");
//					activity.setListDevice(response.body());
//				} else {
//					//get error message
//					try {
//						String str = response.errorBody().string();
//
//						Log.d(TAG, "DeviceService.getDeviceListSpecific onResponse: " + str);
//
//						JsonObject obj = new JsonParser().parse(str).getAsJsonObject();
//						JsonPrimitive status = obj.getAsJsonPrimitive("status");
//
//						if (status != null && status.getAsInt() == 500) {
//							activity.getError(obj.getAsJsonPrimitive("error").getAsString());
//						} else {
//							activity.getError(str);
//						}
//					} catch (IOException e) {
//						e.printStackTrace();
//					}
//				}
//			}
//
//			@Override
//			public void onFailure(Call<List<DeviceDTO>> call, Throwable t) {
//				activity.getError("Cannot connect to the server.");
//			}
//		});
//
//		return call;
//	}

	public Call getDeviceList(final ListDeviceInterface activity) {
		Call<List<DeviceDTO>> call = webServiceInterface.getDevices(session.getValue(Session.ACCESS_TOKEN));

		call.enqueue(new Callback<List<DeviceDTO>>() {
			@Override
			public void onResponse(Call<List<DeviceDTO>> call, Response<List<DeviceDTO>> response) {
				if (response.isSuccessful()) {
					String str = response.body().toString();

					Log.d(TAG, "DeviceService.getDeviceList - successfully fetched List of Devices");
					activity.setListDevice(response.body());
				} else {
					try {
						String str = response.errorBody().string();

						Log.d(TAG, "DeviceService.getDeviceList onResponse: " + str);
						JsonObject obj = new JsonParser().parse(str).getAsJsonObject();
						JsonPrimitive status = obj.getAsJsonPrimitive("status");

						if (status != null && status.getAsInt() == 500) {
							activity.getError(obj.getAsJsonPrimitive("error").getAsString());
						} else {
							activity.getError(str);
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

			@Override
			public void onFailure(Call<List<DeviceDTO>> call, Throwable t) {
				Log.e(TAG, t.getClass().getName());
				if(t.getMessage() != null)
					Log.e(TAG, t.getMessage());
				t.printStackTrace();
				activity.getError("Cannot connect to the server.");
			}
		});
		return call;
	}

	public Call addDevicePost(String deviceName, Long userUnitSocketId, final SimpleCallerUserInterface activity) {

		Call<DeviceDTO> call = webServiceInterface.postAddDevice(new DeviceDTO(deviceName, userUnitSocketId), session.getValue(Session.ACCESS_TOKEN));

		call.enqueue(new Callback<DeviceDTO>() {
			@Override
			public void onResponse(Call<DeviceDTO> call, Response<DeviceDTO> response) {
				if (response.isSuccessful()) {
					Log.e(TAG, "DeviceService.addDevicePost - Successfully added a Device");
					activity.requestStatus(true, "Successfully Added a Device");
				} else {
					try {
						String str = response.errorBody().string();
						Log.e(TAG, "error code: " + response.code()+ " onResponse: " + str);
						activity.requestStatus(false, str);
					} catch (IOException e) {
						activity.requestStatus(false, e.getMessage());
						e.printStackTrace();
					}
				}
			}

			@Override
			public void onFailure(Call<DeviceDTO> call, Throwable t) {
				Log.e(TAG, t.getClass().getName());
				if(t.getMessage() != null)
					Log.e(TAG, t.getMessage());
				t.printStackTrace();
				activity.requestStatus(false, "Cannot connect to the server.");
			}
		});
		return call;
	}

	public void updateDevice(Long id, String deviceName, final SimpleCallerUserInterface activity) {

		Call<DeviceDTO> call = webServiceInterface.putUpdateDevice(id, new DeviceDTO(deviceName),
				session.getValue(Session.ACCESS_TOKEN));

		call.enqueue(new Callback<DeviceDTO>() {
			@Override
			public void onResponse(Call<DeviceDTO> call, Response<DeviceDTO> response) {
				if (response.isSuccessful()) {
					Log.d(TAG, "DeviceService.updateDevice - " +
							"successfully updated a Device");
					activity.requestStatus(true, "Successfully Updated Device");
				} else {
					//get error message
					try {
						String str = response.errorBody().string();
						Log.e(TAG, "error code: " + response.code()+ " onResponse: " + str);
						activity.requestStatus(false, str);
					} catch (IOException e) {
						activity.requestStatus(false, e.getMessage());
						e.printStackTrace();
					}
				}
			}

			@Override
			public void onFailure(Call<DeviceDTO> call, Throwable t) {
				Log.e(TAG, t.getClass().getName());
				if(t.getMessage() != null)
					Log.e(TAG, t.getMessage());
				t.printStackTrace();
				activity.requestStatus(false, "Cannot connect to the server.");
			}
		});
	}

	public Call attachDevice(Long id, final Long socketId, final String source, final MultipleSourceSimpleCallerUserInterface activity) {

		Call<DeviceDTO> call = webServiceInterface.putUpdateDevice(id, new DeviceDTO(socketId), session.getValue(Session.ACCESS_TOKEN));

		call.enqueue(new Callback<DeviceDTO>() {
			@Override
			public void onResponse(Call<DeviceDTO> call, Response<DeviceDTO> response) {
				if (response.isSuccessful()) {
					if(socketId == -1){
						activity.requestStatus(true, "Successfully Detached Device", source);
					}
					else{
						activity.requestStatus(true, "Successfully Attached Device", source);
					}
				} else {
					//get error message
					try {
						String str = response.errorBody().string();
						Log.e(TAG, "error code: " + response.code()+ " onResponse: " + str);
						activity.requestStatus(false, str, source);
					} catch (IOException e) {
						activity.requestStatus(false, e.getMessage(), source);
						e.printStackTrace();
					}
				}
			}

			@Override
			public void onFailure(Call<DeviceDTO> call, Throwable t) {
				Log.e(TAG, t.getClass().getName());
				if(t.getMessage() != null)
					Log.e(TAG, t.getMessage());
				t.printStackTrace();
				activity.requestStatus(false, "Cannot connect to the server.", source);
			}
		});
		return call;
	}

	public void deleteDevice(Long id, final SimpleCallerUserInterface activity) {
		Call<DeviceDTO> call = webServiceInterface.deleteDevice(id, session.getValue(Session.ACCESS_TOKEN));

		call.enqueue(new Callback<DeviceDTO>() {
			@Override
			public void onResponse(Call<DeviceDTO> call, Response<DeviceDTO> response) {
				if (response.isSuccessful()) {
					Log.d(TAG, "DeviceService.deleteDevice - " +
							"successfully deleted a Device");
					activity.requestStatus(true, "Successfully Deleted Device");
				} else {
					//get error message
					try {
						String str = response.errorBody().string();
						Log.e(TAG, "error code: " + response.code()+ " onResponse: " + str);
						activity.requestStatus(false, str);
					} catch (IOException e) {
						activity.requestStatus(false, e.getMessage());
						e.printStackTrace();
					}
				}
			}

			@Override
			public void onFailure(Call<DeviceDTO> call, Throwable t) {
				Log.e(TAG, t.getClass().getName());
				if(t.getMessage() != null)
					Log.e(TAG, t.getMessage());
				t.printStackTrace();
				activity.requestStatus(false, "Cannot connect to the server.");
			}
		});
	}


	public Call getDevice(Long deviceId, final GetDeviceInterface activity, final View socketView) {
		Call<DeviceDTO> call = webServiceInterface.getSpecificDevice(deviceId, session.getValue(Session.ACCESS_TOKEN));

		call.enqueue(new Callback<DeviceDTO>() {
			@Override
			public void onResponse(Call<DeviceDTO> call, Response<DeviceDTO> response) {
				if (response.isSuccessful()) {
					Log.d(TAG, ""+response.body().getId());
					activity.

							setDevice("Successfully get Device", response.body(), socketView);
				} else {
					//get error message
					try {
						String str = response.errorBody().string();
						Log.e(TAG, "error code: " + response.code()+ " onResponse: " + str);
						activity.setError(str);
					} catch (IOException e) {
						activity.setError(e.getMessage());
						e.printStackTrace();
					}
				}
			}

			@Override
			public void onFailure(Call<DeviceDTO> call, Throwable t) {
				Log.e(TAG, t.getClass().getName());
				if(t.getMessage() != null)
					Log.e(TAG, t.getMessage());
				t.printStackTrace();
				activity.setError("Cannot connect to the server.");
			}
		});
		return call;
	}
}
