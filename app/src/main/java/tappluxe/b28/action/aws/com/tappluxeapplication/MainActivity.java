package tappluxe.b28.action.aws.com.tappluxeapplication;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.OnClick;
import tappluxe.b28.action.aws.com.tappluxeapplication.ui.login.Login;
import tappluxe.b28.action.aws.com.tappluxeapplication.ui.main.TabHolder;
import tappluxe.b28.action.aws.com.tappluxeapplication.ui.schedule.AddSchedule;
import tappluxe.b28.action.aws.com.tappluxeapplication.ui.unit.AddUnit;
import tappluxe.b28.action.aws.com.tappluxeapplication.ui.user.ChangeEmailFragment;
import tappluxe.b28.action.aws.com.tappluxeapplication.ui.user.ChangePasswordFragment;
import tappluxe.b28.action.aws.com.tappluxeapplication.ui.user.UpdateProfileFragment;
import tappluxe.b28.action.aws.com.tappluxeapplication.ui.verification.VerificationFragment;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.session.Session;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.UserDTO;

public class MainActivity extends BaseActivity implements
        UpdateProfileFragment.OnUpdateProfileFragmentInteractionListener,
        ChangeEmailFragment.OnChangeEmailFragmentInteractionListener,
        ChangePasswordFragment.OnChangePasswordFragmentInteractionListener,
        VerificationFragment.OnVerificationDoneFragmentInteractionListener{

    public UserDTO user;
    private Session session;

	private NavigationView navigationView;
	private DrawerLayout drawer;
	private View navHeader;
	private TextView txtName, txtEmail, txtPhone;
	private Toolbar toolbar;
	private FloatingActionButton fab;

	// index to identify current nav menu item
	public static int navItemIndex = 0;
    private MenuItem currentMenuItem;

	// tags used to attach the fragments
    private static final String TAG_VERIFICATION = "verification";
	private static final String TAG_HOME = "home";
	private static final String TAG_PROFILE = "profile";
	private static final String TAG_CHANGE_EMAIL = "change email";
	private static final String TAG_CHANGE_PASSWORD = "change password";
	public static String CURRENT_TAG = TAG_HOME;

	// flag to load home fragment when user presses back key
	private boolean shouldLoadHomeFragOnBackPress = true;
	private Handler mHandler;

    private TabHolder tabHolder;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
		toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		mHandler = new Handler();
        user = (UserDTO) getIntent().getSerializableExtra("user");
        user.resolvePhone();
		drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		navigationView = (NavigationView) findViewById(R.id.nav_view);
		fab = (FloatingActionButton) findViewById(R.id.fab_general_page);
		// Navigation view header
		navHeader = navigationView.getHeaderView(0);
		txtName = (TextView) navHeader.findViewById(R.id.name);
		txtEmail = (TextView) navHeader.findViewById(R.id.email);
		txtPhone = (TextView) navHeader.findViewById(R.id.mobile);

        if (savedInstanceState == null) {
            navItemIndex = R.id.nav_home;
            if (user.getVerified()) {
                CURRENT_TAG = TAG_HOME;
            } else {
                CURRENT_TAG = TAG_VERIFICATION;
            }
            currentMenuItem = navigationView.getMenu().getItem(0);
            Log.e("menu", currentMenuItem.getTitle().toString());
            loadHomeFragment();
        }

		// load nav menu header data
		loadNavHeader();

		// initializing navigation menu
		setUpNavigationView();

        session = new Session(this);

	}

    @Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
        if (CURRENT_TAG.equals(TAG_HOME)) {
            tabHolder.goToTab(intent.getStringExtra(TabHolder.GO_TO_FRAGMENT));
        }
	}

    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.general_options_menu, menu);
		return true;
	}
    @Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
            case R.id.menu_refresh_fragment_option:
                if (CURRENT_TAG.equals(TAG_HOME)) {
                    tabHolder.refresh();
                }
                return true;
            default:
                return false;
        }
    }

	@OnClick(R.id.fab_general_page)
    public void fabClick(){
        if (CURRENT_TAG.equals(TAG_HOME)) {
            if (TabHolder.pagerItem == TabHolder.UNIT_PAGE) {
                startActivity(new Intent(MainActivity.this, AddUnit.class));
            } else if (TabHolder.pagerItem == TabHolder.SCHEDULE_PAGE) {
                startActivity(new Intent(MainActivity.this, AddSchedule.class));
            }
        }
    }

    /*
        User information on nav header
     */
	private void loadNavHeader() {
		txtName.setText(user.getFullName());
		txtEmail.setText(user.getEmail());
        txtPhone.setText(user.getMobilePlain());
	}

	/***
	 * Returns respected fragment that user
	 * selected from navigation menu
	 */
	private void loadHomeFragment() {
		// selecting appropriate nav menu item
		selectNavMenu();
        Log.e("verify", CURRENT_TAG);
		// set toolbar title
		setToolbarTitle();

		// if user select the current navigation menu again, don't do anything
		// just close the navigation drawer
		if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
			drawer.closeDrawers();

			// show or hide the fab button
			toggleFab();
			return;
		}

		Runnable mPendingRunnable = new Runnable() {
			@Override
			public void run() {
				// update the main content by replacing fragments
				Fragment fragment = getHomeFragment();
				FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
				fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left,
						android.R.anim.slide_out_right);
				fragmentTransaction.replace(R.id.main_frame, fragment, CURRENT_TAG);
				fragmentTransaction.commitAllowingStateLoss();
			}
		};

		// If mPendingRunnable is not null, then add to the message queue
		if (mPendingRunnable != null) {
			mHandler.post(mPendingRunnable);
		}

		// show or hide the fab button
		toggleFab();

		//Closing drawer on item click
		drawer.closeDrawers();

		// refresh toolbar menu
		invalidateOptionsMenu();
	}

	private Fragment getHomeFragment() {
		switch (navItemIndex) {
            case R.id.nav_home:
                if (user.getVerified()) {
                    tabHolder = TabHolder.newInstance(getIntent().getStringExtra(TabHolder.GO_TO_FRAGMENT));
                    return tabHolder;
                } else {
                    return VerificationFragment.newInstance(user);
                }
            case R.id.nav_profile:
				return UpdateProfileFragment.newInstance(user);
            case R.id.nav_change_email:
                return ChangeEmailFragment.newInstance(user);
            case R.id.nav_change_pass:
                return ChangePasswordFragment.newInstance(user);
            default:
                return TabHolder.newInstance(getIntent().getStringExtra(TabHolder.GO_TO_FRAGMENT));
        }
	}

	private void setToolbarTitle() {
        if (navItemIndex == R.id.nav_home) {
//	        getSupportActionBar().setLogo(R.drawable.tappluxe_white);
            getSupportActionBar().setTitle("Tappluxe");
        } else if (navItemIndex != R.id.nav_logout){
            getSupportActionBar().setLogo(null);
            getSupportActionBar().setTitle(currentMenuItem.getTitle());
        }
	}

	private void selectNavMenu() {
		currentMenuItem.setChecked(true);
	}

	private void setUpNavigationView() {
		//Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
		navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

			// This method will trigger on item Click of navigation menu
			@Override
			public boolean onNavigationItemSelected(final MenuItem menuItem) {
                navItemIndex = menuItem.getItemId();
                switch (menuItem.getItemId()) {
                    case R.id.nav_home:
                        if (user.getVerified()) {
                            CURRENT_TAG = TAG_HOME;
                        } else {
                            CURRENT_TAG = TAG_VERIFICATION;
                        }
                        break;
                    case R.id.nav_profile:
                        CURRENT_TAG = TAG_PROFILE;
                        break;
                    case R.id.nav_change_email:
                        CURRENT_TAG = TAG_CHANGE_EMAIL;
                        break;
                    case R.id.nav_change_pass:
                        CURRENT_TAG = TAG_CHANGE_PASSWORD;
                        break;
                    case R.id.nav_logout:
                        AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this);
                        dialog.setTitle(R.string.logout);
                        dialog.setMessage(R.string.confirm_logout_message);
                        dialog.setCancelable(true);
                        dialog.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Session session = new Session(getApplication());
                                session.removeKey(Session.ACCESS_TOKEN);
                                session.removeKey(Session.LOG_STATE);
                                startActivity(new Intent(MainActivity.this, Login.class));
                                finish();
                            }
                        });
                        dialog.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {
                                currentMenuItem.setChecked(true);
                                menuItem.setChecked(false);
                            }
                        });
                        dialog.create().show();
                        break;
                }
                menuItem.setChecked(true);
                if (currentMenuItem != null) {
                    currentMenuItem.setChecked(false);
                }
                if (navItemIndex != R.id.nav_logout) {
                    currentMenuItem = menuItem;
                }
                loadHomeFragment();
                hideKeyboard();
				return true;
			}
		});


		ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.openDrawer, R.string.closeDrawer) {

			@Override
			public void onDrawerClosed(View drawerView) {
				// Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
				super.onDrawerClosed(drawerView);
			}

			@Override
			public void onDrawerOpened(View drawerView) {
				// Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
				super.onDrawerOpened(drawerView);
			}
		};

		//Setting the actionbarToggle to drawer layout
		drawer.setDrawerListener(actionBarDrawerToggle);

		//calling sync state is necessary or else your hamburger icon wont show up
		actionBarDrawerToggle.syncState();
	}

	@Override
	public void onBackPressed() {
		if (drawer.isDrawerOpen(GravityCompat.START)) {
			drawer.closeDrawers();
			return;
		}

		// This code loads home fragment when back key is pressed
		// when user is in other fragment than home
		if (shouldLoadHomeFragOnBackPress) {
			// checking if user is on other navigation menu
			// rather than home
			if (navItemIndex != R.id.nav_home) {
				navItemIndex = R.id.nav_home;
				CURRENT_TAG = TAG_HOME;
				loadHomeFragment();
				return;
			}
		}

		super.onBackPressed();
	}

	// show or hide the fab
	private void toggleFab() {
		if (CURRENT_TAG.equals(TAG_HOME)){
            fab.show();
        } else {
            fab.hide();
        }
	}

    @Override
    public void onUpdateProfile(UserDTO updatedUser) {
        this.user = updatedUser;
        loadNavHeader();
    }

    @Override
    public void onChangeEmailDone(UserDTO updatedUser) {
        this.user = updatedUser;
        session.setValue(session.USER_EMAIL, updatedUser.getEmail());
        loadNavHeader();
    }

    @Override
    public void onChangePasswordDone(UserDTO user) {
        session.setValue(session.USER_PASSWORD, user.getPassword());
    }

    @Override
    public void onVerificationDone() {
        this.user.setVerified(true);
        CURRENT_TAG = TAG_HOME;
        loadHomeFragment();
    }
}
