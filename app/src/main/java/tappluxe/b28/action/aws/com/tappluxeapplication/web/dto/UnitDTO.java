package tappluxe.b28.action.aws.com.tappluxeapplication.web.dto;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ariel.fabilena on 2/21/2017.
 */

public class UnitDTO implements Serializable {

	private String name;
	private String productKey;
    private Long id;
	private String registrationDate;
	private Long userId;
	private List<UnitSocketDTO> sockets;
	private String ssid;
	private ManufacturedUnitDTO manufacturedUnitDetails;

	public UnitDTO(String name, Long id, String registrationDate, Long userId,
                   List<UnitSocketDTO> sockets, String ssid, ManufacturedUnitDTO manufacturedUnitDetails) {
        this.manufacturedUnitDetails = manufacturedUnitDetails;
		this.name = name;
		this.id = id;
		this.registrationDate = registrationDate;
		this.userId = userId;
		this.sockets = sockets;
        this.ssid = ssid;
	}

	public UnitDTO(String productKey, String name) {
		this.productKey = productKey;
		this.name = name;
	}

	public UnitDTO(String name) {
		this.name = name;
	}

	public UnitDTO() {

    }

	public String getProductKey() {
		return productKey;
	}

	public void setProductKey(String productKey) {
		this.productKey = productKey;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setRegistrationDate(String registrationDate) {
		this.registrationDate = registrationDate;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public List<UnitSocketDTO> getSockets() {
		return sockets;
	}

	public void setSockets(List<UnitSocketDTO> sockets) {
		this.sockets = sockets;
	}

    public String getSsid() {
        return ssid;
    }

    public void setSsid(String ssid) {
        this.ssid = ssid;
    }

	public ManufacturedUnitDTO getManufacturedUnitDetails() {
		return manufacturedUnitDetails;
	}

	public void setManufacturedUnitDetails(ManufacturedUnitDTO manufacturedUnitDetails) {
		this.manufacturedUnitDetails = manufacturedUnitDetails;
	}

	@Override
	public String toString() {
		return name;
	}
}
