package tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces;

import java.util.List;

import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.ScheduleDTO;

/**
 * Created by benjo.uriarte on 3/7/2017.
 */

public interface ListScheduleInterface {
    void setListSchedule(List<ScheduleDTO> schedule);
    void getError(String message);
}
