package tappluxe.b28.action.aws.com.tappluxeapplication.utility.adapter.schedule_util;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * Created by franc.margallo on 3/13/2017.
 */

public class TestService extends Service {
	TestReceiver alarm = new TestReceiver();
	@Nullable
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId)
	{
		alarm.setAlarm(this);
		return START_STICKY;
	}

	@Override
	public void onStart(Intent intent, int startId)
	{
		alarm.setAlarm(this);
	}
}
