package tappluxe.b28.action.aws.com.tappluxeapplication.web.interfaces;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.DeviceDTO;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.DeviceStatusDTO;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.ResetPasswordDTO;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.ScheduleDTO;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.SocketChangeDetailsResponseDTO;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.UnitDTO;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.UnitSocketDTO;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.UserDTO;

/**
 * Created by ariel.fabilena on 2/21/2017.
 */

public interface WebServiceInterface {
	//User Interface
	//Sign Up
	@POST("/api/membership")
	Call<UserDTO> postSignUp(@Body UserDTO body);
	@GET("/api/membership/check/email")
	Call<UserDTO> checkEmail(@Query("email") String email);
	@GET("/api/membership/check/product_id")
	Call<UnitDTO> checkProductKey(@Query("productId") String uuid);

	// Forgot Password
	@GET("/api/membership/forgot_password")
	Call<ResponseBody> forgotPassword(@Query("email") String email);
	@POST("/api/membership/reset_password")
	Call<ResponseBody> resetPassword(@Body ResetPasswordDTO body);

	// User Interface
	@GET("/api/users")
	Call<UserDTO> userDetails(@Query("access_token") String token);
    @POST("/api/users")
    Call<UserDTO> updateUser(@Body UserDTO body, @Query("access_token") String token);

	//Login
	//grant_type=refresh_token
	@FormUrlEncoded
	@POST("/oauth/token?grant_type=password")
	Call<ResponseBody> postLogInJson(
			@Field("username") String username,
			@Field("password") String password
	);

	@FormUrlEncoded
	@POST("/oauth/token?grant_type=refresh_token")
	Call<ResponseBody> postRefreshToken(
			@Field("refresh_token") String refresh_token
	);


	//Unit Interface
	@POST("/api/user_units")
	Call<UnitDTO> postAddUnit(@Body UnitDTO body, @Query("access_token") String token);
	//Update
	@POST("/api/user_units/{userUnitId}")
	Call<UnitDTO> putUpdateUnit(@Body UnitDTO body, @Path("userUnitId") Long id, @Query("access_token") String token);
	//Delete
	@DELETE("/api/user_units/{userUnitId}")
	Call<UnitDTO> deleteUnit(@Path("userUnitId") Long id, @Query("access_token") String token);
	//View All
	@GET("/api/user_units/")
	Call<List<UnitDTO>> getUnitList(@Query("access_token") String token);
	//View Specific
	@GET("/api/user_units/{userUnitId}")
	Call<UnitDTO> getUnit(@Path("userUnitId") Long id, @Query("access_token") String token);
	//Turn Off
	@GET("/api/user_units/{userUnitId}/socket/{socketId}/change_state")
	Call<SocketChangeDetailsResponseDTO> turnOffSocket(@Path("userUnitId") Long userUnitId, @Path("socketId") Long socketId, @Query("state") Boolean state, @Query("access_token") String token);
	@GET("/api/user_units/{userUnitId}/socket/{socketId}/current_state")
	Call<DeviceStatusDTO> requestStateSocket(@Path("userUnitId") Long userUnitId, @Path("socketId") Long socketId, @Query("access_token") String token);
	@GET("/api/user_units/{userUnitId}/current_availability")
	Call<ResponseBody> socketStatus(@Path("userUnitId") Long userUnitId, @Query("access_token") String token);
	@GET("/api/user_units/{userUnitId}/refresh_availability")
	Call<ResponseBody> refreshSocketStatus(@Path("userUnitId") Long userUnitId, @Query("access_token") String token);


	//Device Interface
	//Create
	@POST("/api/devices")
	Call<DeviceDTO> postAddDevice(@Body DeviceDTO body, @Query("access_token") String token);
	//Update
	@POST("/api/devices/{deviceId}")
	Call<DeviceDTO> putUpdateDevice(@Path("deviceId") Long deviceId, @Body DeviceDTO body,
									@Query("access_token") String token);
	//Delete
	@DELETE("/api/devices/{deviceId}")
	Call<DeviceDTO> deleteDevice(@Path("deviceId") Long deviceId, @Query("access_token") String token);
	//View All (attached_to for filtered[boolean])
	@GET("/api/devices")
	Call<List<DeviceDTO>> getDevices(@Query("access_token") String token);
	//View Specific
	// @GET("/api/devices/unit/{unitId}")
//	Call<List<DeviceDTO>> getDevicesFromUnit(@Path("unitId") Long unitId, @Query("access_token") String token);
	//View Specific
	@GET("/api/devices/{deviceId}")
	Call<DeviceDTO> getSpecificDevice(@Path("deviceId") Long deviceId, @Query("access_token") String token);

	//Socket Interface
	//assignSchedule //changed assign_schedule to manage_schedule
	@GET("/api/user_units/{userUnitId}/socket/{socketId}/manage_schedule")
	Call<ResponseBody> assignSchedule(@Path("userUnitId") Long userUnitId, @Path("socketId") Long socketId, @Query("schedule_id") Long scheduleId, @Query("access_token") String token);
	//assignSchedule *removed
//	@GET("/api/user_units/{userUnitId}/socket/{socketId}/remove_schedule")
//	Call<ResponseBody> removeSchedule(@Path("userUnitId") Long userUnitId, @Path("socketId") Long socketId, @Query("access_token") String token);
	//insertDevice *removed
//	@GET("/api/user_units/{userUnitId}/socket/{socketId}/insert_device")
//	Call<ResponseBody> insertDevice(@Path("userUnitId") Long userUnitId, @Path("socketId") Long socketId, @Query("device_id") Long deviceId, @Query("access_token") String token);
	//insertDevice *removed
//	@GET("/api/user_units/{userUnitId}/socket/{socketId}/remove_device")
//	Call<ResponseBody> removeDevice(@Path("userUnitId") Long userUnitId, @Path("socketId") Long socketId, @Query("access_token") String token);


	// Device Status Interface
	// add status for the device
	@POST("/api/device/{deviceId}/status")
	Call<DeviceStatusDTO> createDeviceStatus(@Path("deviceId") Long deviceId, @Query("access_token") String token);
	// get all status for the device
	@GET("/api/device/{deviceId}/status/all")
	Call<List<DeviceStatusDTO>> getAllDeviceStatus(@Path("deviceId") Long deviceId, @Query("access_token") String token);
	// get current status for the device
	@GET("/api/device/{deviceId}/status/current")
	Call<DeviceStatusDTO> getCurrentDeviceStatus(@Path("deviceId") Long deviceId, @Query("access_token") String token);
	// delete status for the device
	@DELETE("/api/device/{deviceId}/status/{statusId}")
	Call<DeviceStatusDTO> deleteDeviceStatus(@Path("deviceId") Long deviceId, @Path("statusId") Long statusId, @Query("access_token") String token);


	//Turn off device
//	@GET("/api/device/{deviceId}/status/turn_off")
//	Call<DeviceStatusDTO> turnOff(@Path("deviceId") Long deviceId, @Query("access_token") String token);

	//Schedule
	//add
	@POST("/api/schedules")
	Call<ScheduleDTO> postAddSchedule(@Body ScheduleDTO body, @Query("access_token") String token);
	//edut
	@POST("/api/schedules/{scheduleId}")
	Call<ScheduleDTO> putUpdateSchedule(@Body ScheduleDTO body, @Path("scheduleId") Long id, @Query("access_token") String token);
	//delete
	@DELETE("/api/schedules/{scheduleId}")
	Call<ScheduleDTO> deleteSchedule(@Path("scheduleId") Long id, @Query("access_token") String token);
	//View All
	@GET("/api/schedules")
	Call<List<ScheduleDTO>> getSchedules(@Query("access_token") String token);
	//view
	@GET("/api/schedules/{scheduleId}")
	Call<ScheduleDTO> getSpecificSchedule(@Path("scheduleId") Long id, @Query("access_token") String token);
	//add to device
	@GET("/api/user_units/{userUnitId}/socket/{socketId}/assign_schedule")
	Call<ResponseBody> assignToSocket(@Path("userUnitId") Long userUnitId, @Path("socketId") Long socketId, @Query("schedule_id") Long scheduleId, @Query("access_token") String token);

	// Verification
	@POST("/api/verification/check")
	Call<UserDTO> checkCode(@Query("code") String code, @Query("email") String email);

	@GET("/api/verification/resend")
	Call<UserDTO> resendCode(@Query("email") String email);

	@GET("/api/verification/isVerified")
	Call<ResponseBody> isVerified(@Query("access_token") String token);
}
