package tappluxe.b28.action.aws.com.tappluxeapplication.ui.login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import tappluxe.b28.action.aws.com.tappluxeapplication.MainActivity;
import tappluxe.b28.action.aws.com.tappluxeapplication.R;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.SimpleCallerUserInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.UserDetailsInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.session.Session;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.UserDTO;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.service.UserService;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.service.VerificationService;

public class SplashScreen extends AppCompatActivity implements SimpleCallerUserInterface, UserDetailsInterface {
	private Session session;
	public static final String MESSAGE = "MESSAGE";
	private boolean fromlogin = false;
	private String email, password;

	private VerificationService verificationService;
	private UserService userService;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0) {
			// Activity was brought to front and not created,
			// Thus finishing this will get us to the last viewed activity
			finish();
			return;
		}

		setContentView(R.layout.splash_screen_activity);
		getSupportActionBar().hide();

		session = new Session(this);

		String access_token = session.getValue(Session.ACCESS_TOKEN);
		String refresh_token = session.getValue(Session.REFRESH_TOKEN);
		boolean logState = session.getBooleanValue(Session.LOG_STATE);

		Intent intent = getIntent();
		verificationService = new VerificationService(this);
		userService = new UserService(this);
		fromlogin = intent.getBooleanExtra(Login.LOGIN, false);
		if (fromlogin) {
			email = intent.getStringExtra(Login.EMAIL);
			password = intent.getStringExtra(Login.PASSWORD);
			userService.loginPost(this, email, password);
		} else {
			//KEEP LOGGING IN
			if (logState == true && refresh_token != null && access_token != null) {
				userService.refreshTokenPost(this, refresh_token);
			} else {
				intent = new Intent(this, Login.class);
				startActivity(intent);
				finish();
			}
		}

	}

	@Override
	public void requestStatus(boolean success, String message) {
		Intent intent;
		if (success) {
			userService.userDetails(SplashScreen.this);
		} else {
			intent = new Intent(this, Login.class);
			if (fromlogin) {
				intent.putExtra(Login.LOGIN, true);
				intent.putExtra(Login.EMAIL, email);
				intent.putExtra(Login.PASSWORD, password);
			}
			intent.putExtra(MESSAGE, message);
			//put extra
			startActivity(intent);
			finish();
		}
	}

	@Override
	public void userDetails(UserDTO user) {
		Intent intent;
		intent = new Intent(SplashScreen.this, MainActivity.class);
		intent.putExtra("user", user);
		startActivity(intent);
		finish();
	}
}
