package tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces;

/**
 * Created by jude.dassun on 3/23/2017.
 */

public interface MultipleSourceSimpleCallerUserInterface {
	void requestStatus(boolean success, String message, String source);
}
