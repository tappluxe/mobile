package tappluxe.b28.action.aws.com.tappluxeapplication.web.service;

import android.content.Context;
import android.util.Log;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.session.Session;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.UserDTO;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.interfaces.WebServiceInterface;

/**
 * Created by jay.bilocura on 3/10/2017.
 */

public class VerificationService extends BaseService {
    private static final String TAG = VerificationService.class.getName();

    public VerificationService(Context activity) {
        super(activity);
    }

    public void checkCode(String code, String email, Callback callback) {
        Call<UserDTO> call = webServiceInterface.checkCode(code, email);

        call.enqueue(callback);
    }

    public void resendCode(String email, Callback<UserDTO> callback) {
        Log.e("In Service", "resending code");
        Call<UserDTO> call = webServiceInterface.resendCode(email);
        call.enqueue(callback);
    }

    public void isVerified(Callback callback) {
        Call<ResponseBody> call = webServiceInterface.isVerified(session.getValue(Session.ACCESS_TOKEN));
        call.enqueue(callback);
    }


}
