package tappluxe.b28.action.aws.com.tappluxeapplication.ui.device;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import retrofit2.Call;
import tappluxe.b28.action.aws.com.tappluxeapplication.R;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.adapter.DeviceSpinnerAdapter;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.FormInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.ListDeviceInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.SimpleCallerUserInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.DeviceDTO;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.UnitDTO;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.service.DeviceService;

public class AddDevice extends AppCompatActivity implements FormInterface, ListDeviceInterface, SimpleCallerUserInterface {

	@BindView(R.id.et_add_device_name)
	EditText etDeviceName;
	@BindView(R.id.btn_add_device_accept)
	Button btnAdd;
	@BindView(R.id.til_et_add_device_name)
	TextInputLayout tilAddDeviceName;
	@BindView(R.id.spnnr_existing_devices)
	Spinner spnrExistingDevices;
	@BindView(R.id.rd_dd_existing_device)
	RadioButton rdAddExistingDevice;

	public DeviceService deviceService;
	private UnitDTO unit;
	private Long socketId;
	private Integer socketNumber;
	private Call callAdd;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.device_add_activity);
		ButterKnife.bind(this);
		deviceService = new DeviceService(this);
//		Intent intent = getIntent();
//		unit = (UnitDTO) intent.getSerializableExtra("unit");
//		socketId = intent.getExtras().getLong("socketId");
//		socketNumber = intent.getExtras().getInt("socketNumber");
//		setTitle(unit.getName() + ": Socket " + socketNumber);
		setTitle("Add Device");
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
//		if (unit != null) {
//			deviceService.getDeviceList(this);
//		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
			case android.R.id.home:
				this.finish();
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onStop() {
		super.onStop();
		if(callAdd != null && !callAdd.isCanceled())
			callAdd.cancel();
	}

	@OnTextChanged(R.id.et_add_device_name)
	public void deviceNameTextChanged() {
		tilAddDeviceName.setError(null);
	}

	@OnClick(R.id.btn_add_device_cancel)
	public void onBtnCancel() {
		finish();
	}

	@OnClick(R.id.btn_add_device_accept)
	public void onBtnAddDevice() {
		enableForm(false);
		if(callAdd != null && !callAdd.isCanceled())
			callAdd.cancel();
		String deviceName = etDeviceName.getText().toString().trim();
		if (isValidDeviceName(deviceName)) {
			callAdd = deviceService.addDevicePost(deviceName, null, this);
		} else {
			enableForm(true);
		}
	}

	@Override
	public void requestStatus(boolean success, String message) {
		Toast.makeText(this, message, Toast.LENGTH_LONG).show();
		enableForm(true);
		if (success) {
			finish();
		}
		else{
			try {
				JsonElement element = new JsonParser().parse(message);
				if (element == null) {
					Toast.makeText(this, message, Toast.LENGTH_LONG).show();
				} else {
					JsonObject obj = element.getAsJsonObject();
					if (obj == null) {
						Toast.makeText(this, message, Toast.LENGTH_LONG).show();
					} else {
						boolean hasnormalerror = false;
						JsonElement userUnitSocketId = obj.get("userUnitSocketId");
						JsonElement name = obj.get("name");
						JsonElement generalMessage = obj.get("message");
						if (userUnitSocketId != null && !userUnitSocketId.getAsString().isEmpty()) {
							Toast.makeText(this, userUnitSocketId.getAsString(), Toast.LENGTH_LONG).show();
							hasnormalerror = true;
						}
						if (name != null && !name.getAsString().isEmpty()) {
							tilAddDeviceName.setError(name.getAsString());
							hasnormalerror = true;
						}
						if(generalMessage != null && !generalMessage.getAsString().isEmpty()){
							Toast.makeText(this, generalMessage.getAsString(), Toast.LENGTH_LONG).show();
							hasnormalerror = true;
						}

						if(!hasnormalerror){
							Toast.makeText(this, message, Toast.LENGTH_LONG).show();
						}
					}
				}
			} catch (JsonSyntaxException | IllegalStateException err) {
				Toast.makeText(this, message, Toast.LENGTH_LONG).show();
			}
		}
	}

	@Override
	public void clearForm() {
	}

	@Override
	public void enableForm(boolean enabled) {
		etDeviceName.setEnabled(enabled);
		btnAdd.setEnabled(enabled);
	}

	public boolean isValidDeviceName(String deviceName) {
		if (deviceName.isEmpty()) {
			tilAddDeviceName.setError(getText(R.string.empty_device_name_message));
			return false;
		}
		return true;
	}

	@Override
	public void setListDevice(List<DeviceDTO> devices) {
		DeviceSpinnerAdapter deviceSpinnerAdapter = new DeviceSpinnerAdapter(devices, this);
		spnrExistingDevices.setAdapter(deviceSpinnerAdapter);
		if(devices.isEmpty()){
			rdAddExistingDevice.setEnabled(false);
			spnrExistingDevices.setEnabled(false);
			spnrExistingDevices.setVisibility(View.GONE);
		}
		spnrExistingDevices.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
			}
			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}
		});
	}

	@Override
	public void getError(String message) {
		Toast.makeText(this, message, Toast.LENGTH_LONG).show();
	}
}
