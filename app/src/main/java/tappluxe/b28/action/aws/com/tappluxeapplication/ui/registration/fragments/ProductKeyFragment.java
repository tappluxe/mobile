package tappluxe.b28.action.aws.com.tappluxeapplication.ui.registration.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import tappluxe.b28.action.aws.com.tappluxeapplication.R;
import tappluxe.b28.action.aws.com.tappluxeapplication.ui.login.Login;
import tappluxe.b28.action.aws.com.tappluxeapplication.ui.qrcode.QRCodeScanActivity;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.SimpleCallerUserInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.validators.ProductKeyValidator;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.service.UnitService;

public class ProductKeyFragment extends Fragment implements SimpleCallerUserInterface {

	private UnitService unitService;
	private int mPreviousLength;
	private boolean mBackSpace;

	@BindView(R.id.et_prod_key)
	public EditText etProdKey;
//	@BindView(R.id.tv_prod_key)
//	public TextView tvProdKey;
	@BindView(R.id.til_prod_key)
	public TextInputLayout tilProdKey;
    @BindView(R.id.btn_product_key_next)
    public Button next;


	private OnProductKeyFragmentInteractionListener mListener;

	public ProductKeyFragment() {
	}

	public static ProductKeyFragment newInstance() {
		ProductKeyFragment fragment = new ProductKeyFragment();
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.product_key_fragment, container, false);
		ButterKnife.bind(this, view);

		etProdKey.addTextChangedListener(new ProductKeyTextWatcher());
		unitService = new UnitService(getContext().getApplicationContext());
		return view;
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if (context instanceof OnProductKeyFragmentInteractionListener) {
			mListener = (OnProductKeyFragmentInteractionListener) context;
		} else {
			throw new RuntimeException(context.toString()
					+ " must implement OnProductKeyFragmentInteractionListener");
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mListener = null;
	}

	@Optional
	@OnClick(R.id.have_account)
	public void backToLogin() {
		startActivity(new Intent(getContext().getApplicationContext(), Login.class));
		getActivity().finish();
	}

	@OnClick(R.id.tv_prod_key)
	public void makeEditable() {
//		String key = tvProdKey.getText().toString();
//		etProdKey.append(key + "");
//		etProdKey.setVisibility(View.VISIBLE);
//		tvProdKey.setVisibility(View.GONE);
//		etProdKey.requestFocus();
		InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.showSoftInput(etProdKey, InputMethodManager.SHOW_IMPLICIT);
	}

	@OnClick(R.id.btn_scanQR)
	public void scanQR() {
		Intent intent = new Intent(getContext().getApplicationContext(), QRCodeScanActivity.class);
		startActivityForResult(intent, QRCodeScanActivity.QRCODE_READER_REQUEST_CODE);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == QRCodeScanActivity.QRCODE_READER_REQUEST_CODE) {
			if (resultCode == Activity.RESULT_OK) {
				if (data != null) {
					String result = data.getStringExtra(QRCodeScanActivity.QRCODE);
					etProdKey.setText(result);
                    Toast.makeText(getContext(), "QR Code successfully scanned.", Toast.LENGTH_LONG).show();
                    next.performClick();
				}
			}
		} else super.onActivityResult(requestCode, resultCode, data);
	}

	@OnClick(R.id.btn_product_key_next)
	public void productKeyNext() {
		String productKey = etProdKey.getText().toString();
		if (isValidProdKey(productKey)) {
			unitService.checkProductKey(productKey, this);
		}
	}


	public boolean isValidProdKey(String prodKey) {
		ProductKeyValidator pkv = new ProductKeyValidator();

		if (prodKey.isEmpty()) {
			tilProdKey.setErrorEnabled(true);
			tilProdKey.setError(getText(R.string.empty_prod_key_message));
			return false;
		} else if (!pkv.isValid(prodKey)) {
			tilProdKey.setErrorEnabled(true);
			tilProdKey.setError(getText(R.string.invalid_prod_key_format_message));
			return false;
		}

		return true;
	}

	public String getProductKey() {
		String prodKey = etProdKey.getText().toString();
		String val = prodKey.substring(0, 18) + "\n" + prodKey.substring(18, 36);
		return val;
	}

	@Override
	public void requestStatus(boolean success, String message) {
		if (success) {
			String prodKey = etProdKey.getText().toString();
			mListener.onProductKeyDone(prodKey);
		} else {
			tilProdKey.setErrorEnabled(true);
			tilProdKey.setError(message);
		}
	}

	private class ProductKeyTextWatcher implements TextWatcher {
		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			mPreviousLength = s.length();
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
			tilProdKey.setError(null);
			tilProdKey.setErrorEnabled(false);
		}

		@Override
		public void afterTextChanged(Editable s) {
			mBackSpace = mPreviousLength > s.length();
			String str = etProdKey.getText().toString().trim();
			int newProductKeyLen = str.length();

			if (mBackSpace) {
				if (str.length() != 0) {
					str = str.substring(0, str.length() - 1);
					switch (mPreviousLength) {
						case 9:
						case 14:
						case 19:
						case 24:
							etProdKey.setText("");
							etProdKey.append(str);
							break;
					}
				}
			} else {
				switch (newProductKeyLen) {
					case 8:
					case 13:
					case 18:
					case 23:
						etProdKey.append("-");
						break;
				}
				if (newProductKeyLen >= 36) {
					etProdKey.clearFocus();
//					tvProdKey.setVisibility(View.VISIBLE);
//					tvProdKey.setText(getProductKey());
//					etProdKey.setVisibility(View.GONE);
					InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(getActivity().getWindow().getDecorView().getWindowToken(), 0);
				}
			}
		}
	}


	public interface OnProductKeyFragmentInteractionListener {
		void onProductKeyDone(String productKey);
	}

}
