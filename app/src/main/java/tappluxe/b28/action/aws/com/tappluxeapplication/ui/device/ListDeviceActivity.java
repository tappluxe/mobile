package tappluxe.b28.action.aws.com.tappluxeapplication.ui.device;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import tappluxe.b28.action.aws.com.tappluxeapplication.R;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.adapter.DeviceListItemAdapter;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.ListDeviceInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.ListViewParentInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.SimpleCallerUserInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.DeviceDTO;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.service.DeviceService;

public class ListDeviceActivity extends AppCompatActivity implements ListViewParentInterface, ListDeviceInterface, SimpleCallerUserInterface{

	private DeviceService deviceService;
	private Call callList;

	@BindView(R.id.device_list)
	public ListView device_list;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.device_list_activity);
		deviceService = new DeviceService(this);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		ButterKnife.bind(this);
		setTitle("Devices");
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
			case android.R.id.home:
				this.finish();
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onStart() {
		super.onStart();
		refresh();
	}

	@Override
	protected void onStop() {
		super.onStop();
		if(callList != null && !callList.isCanceled()){
			callList.cancel();
		}
	}

	@Override
	public void refresh() {
		callList = deviceService.getDeviceList(this);
	}

	@Override
	public void setListDevice(List<DeviceDTO> devices) {
		DeviceListItemAdapter deviceListItemAdapter = new DeviceListItemAdapter(devices, this, this);
		device_list.setAdapter(deviceListItemAdapter);
	}

	@OnClick(R.id.fab_add_device)
	public void addDevice(){
		AlertDialog.Builder dialog = new android.app.AlertDialog.Builder(this);
		LayoutInflater inflater = this.getLayoutInflater();;
		View parentView = inflater.inflate(R.layout.device_edit_dialog,null);
		final EditText et_device_name = (EditText) parentView.findViewById(R.id.et_device_name);
		dialog.setTitle(R.string.device_add_title);
		dialog.setCancelable(true);
		dialog.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				deviceService.addDevicePost(et_device_name.getText().toString(), null, ListDeviceActivity.this);
			}
		});

		dialog.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});

		dialog.setView(parentView);
		dialog.create().show();
	}

	@Override
	public void getError(String message) {
		try {
			JsonElement element = new JsonParser().parse(message);
			if (element == null) {
				Toast.makeText(this, message, Toast.LENGTH_LONG).show();
			} else {
				JsonObject obj = element.getAsJsonObject();
				if (obj == null) {
					Toast.makeText(this, message, Toast.LENGTH_LONG).show();
				} else {
					JsonElement generalMessage = obj.get("message");
					if(generalMessage != null && !generalMessage.getAsString().isEmpty()){
						Toast.makeText(this, generalMessage.getAsString(), Toast.LENGTH_LONG).show();
					}
					else
						Toast.makeText(this, message, Toast.LENGTH_LONG).show();
				}
			}
		} catch (JsonSyntaxException | IllegalStateException err) {
			Log.e(getClass().getName(), err.getMessage());
			Toast.makeText(this, message, Toast.LENGTH_LONG).show();
		}
	}

	@Override
	public void requestStatus(boolean success, String message) {
		if(success){
			Toast.makeText(this, message, Toast.LENGTH_LONG).show();
			refresh();
		}
		else{
			try {
				JsonElement element = new JsonParser().parse(message);
				if (element == null) {
					Toast.makeText(this, message, Toast.LENGTH_LONG).show();
				} else {
					JsonObject obj = element.getAsJsonObject();
					if (obj == null) {
						Toast.makeText(this, message, Toast.LENGTH_LONG).show();
					} else {
						JsonElement name = obj.get("name");
						JsonElement generalMessage = obj.get("message");
						if(name != null && !name.getAsString().isEmpty()){
							Toast.makeText(this, name.getAsString(), Toast.LENGTH_LONG).show();
						}
						else if(generalMessage != null && !generalMessage.getAsString().isEmpty()){
							Toast.makeText(this, generalMessage.getAsString(), Toast.LENGTH_LONG).show();
						}
						else
							Toast.makeText(this, message, Toast.LENGTH_LONG).show();
					}
				}
			} catch (JsonSyntaxException | IllegalStateException err) {
				Log.e(getClass().getName(), err.getMessage());
				Toast.makeText(this, message, Toast.LENGTH_LONG).show();
			}
		}
	}
}
