package tappluxe.b28.action.aws.com.tappluxeapplication.ui.schedule;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import tappluxe.b28.action.aws.com.tappluxeapplication.BaseActivity;
import tappluxe.b28.action.aws.com.tappluxeapplication.R;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.SimpleCallerUserInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.ScheduleDTO;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.service.ScheduleService;

public class UpdateSchedule extends BaseActivity implements SimpleCallerUserInterface {
	@BindView(R.id.sched_sun)
	public Button sun;
	@BindView(R.id.sched_mon)
	public Button mon;
	@BindView(R.id.sched_tue)
	public Button tue;
	@BindView(R.id.sched_wed)
	public Button wed;
	@BindView(R.id.sched_thu)
	public Button thu;
	@BindView(R.id.sched_fri)
	public Button fri;
	@BindView(R.id.sched_sat)
	public Button sat;
	@BindView(R.id.sched_notif)
	public TimePicker sched;
	@BindView(R.id.schedule_name_field)
	public EditText schedule_name_field;
	@BindView(R.id.til_schedule_name)
	public TextInputLayout til_schedule_name;

	@BindView(R.id.spnr_time_before_turn_off_notification)
	public Spinner timeBeforeTurnOffNotification;

	public boolean selected_sched[] = {false, false, false, false, false, false, false};
	private ScheduleService scheduleService;
	private ScheduleDTO scheduleDTO;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.schedule_update);
		ButterKnife.bind(this);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);

		NumberPickerDividerColor.applyStyle(sched, this);

		Intent intent = getIntent();
		scheduleDTO = (ScheduleDTO) intent.getSerializableExtra("schedule");

		Log.d(getClass().getName(), "test onCreate: timeBeforeNotification = "
				+ scheduleDTO.getTimeBeforeNotification());
		Log.d(getClass().getName(), "test onCreate: isNotificationEnabled = "
				+ scheduleDTO.isNotificationEnabled());

		sched.setIs24HourView(false);
		scheduleService = new ScheduleService(this);

		getSupportActionBar().setTitle(getString(R.string.edit) + " " + scheduleDTO.getName());
		schedule_name_field.setText(scheduleDTO.getName());

		DateFormat format = new SimpleDateFormat(getString(R.string.recieve_time_format));
		Log.e(getClass().getName(), scheduleDTO.getTime());
		try {
			Date time;
			time = format.parse(scheduleDTO.getTime());
			Log.e(getClass().getName(), scheduleDTO.getRepeat());
			String[] days = scheduleDTO.getRepeat().split("\\|");
			Log.e(getClass().getName(), "" + days.length);
			for (String day : days) {
				selected_sched[Integer.parseInt(day)] = true;
			}
			displaySetting(selected_sched, time.getHours(), time.getMinutes(), scheduleDTO.getName());
		} catch (ParseException e) {
			Toast.makeText(this, "Invalid Time Format recieved from Server", Toast.LENGTH_LONG).show();
		}

		initNotificationSpinner();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				this.finish();
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@OnTextChanged(R.id.schedule_name_field)
	public void onNameTextChange() {
		til_schedule_name.setError(null);
	}

	public void initNotificationSpinner() {
		int minimum, interval, maximum;

		minimum = getResources().getInteger(R.integer.min_reminder_time);
		interval = getResources().getInteger(R.integer.interval_reminder_time);
		maximum = getResources().getInteger(R.integer.max_reminder_time);

		List<String> spinnerArray = new ArrayList<>();
		spinnerArray.add(0, "OFF");
		spinnerArray.addAll(1, setNotificationSpinnerTimes(minimum, interval, maximum));
		ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, spinnerArray);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		Spinner sItems = (Spinner) findViewById(R.id.spnr_time_before_turn_off_notification);
		sItems.setAdapter(adapter);

		if (scheduleDTO.getTimeBeforeNotification() == 0)
			sItems.setSelection(0);
		else {
			Log.e(getClass().getName(), "time " + scheduleDTO.getTimeBeforeNotification());
			Log.e(getClass().getName(), "index " + scheduleDTO.getTimeBeforeNotification() / 5);
			sItems.setSelection(scheduleDTO.getTimeBeforeNotification() / 5);
		}

	}

	public List setNotificationSpinnerTimes(int minimum, int interval, int maximum) {
		List<Integer> timeList = new ArrayList<>();

		for (int i = minimum; i <= maximum; i += interval) {
			timeList.add(i);
		}

		return timeList;
	}

	public void setSchedule(int day, Button day_but) {
		if (selected_sched[day]) {
			day_but.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.sched_button, null));
			selected_sched[day] = false;
		} else {
			day_but.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.sched_button_enabled, null));
			selected_sched[day] = true;
		}
	}

	@OnClick(R.id.schedule_save)
	public void updateScheduleChanges() {
		til_schedule_name.setError(null);

		String name = schedule_name_field.getText().toString().trim();
		if (name.isEmpty()) {
			til_schedule_name.setError(getString(R.string.empty_schedule_name_message));
		} else if (getSchedule().equalsIgnoreCase("")) {
			Toast.makeText(this, R.string.instruction_please_select_at_least_1_day, Toast.LENGTH_SHORT).show();
		} else {
			int timeBeforeNotification = timeBeforeTurnOffNotification.getSelectedItem().toString().equals("OFF") ? 0 : Integer.parseInt(timeBeforeTurnOffNotification.getSelectedItem().toString());
			scheduleService.updateSchedule(scheduleDTO.getId(), name, getTime(), getSchedule(), timeBeforeNotification, this);
			String msg = name + "--" + getTime() + "--" + getSchedule() + "--" + timeBeforeNotification;
			Log.d("UpdatedSuccessfully", msg);
		}
	}

	public String getTime() {
		SimpleDateFormat sdf = new SimpleDateFormat(getString(R.string.send_time_format));
		Calendar calendar = Calendar.getInstance();
		calendar.set(calendar.YEAR, Calendar.MONTH, Calendar.DAY_OF_MONTH, sched.getCurrentHour(), sched.getCurrentMinute(), 0);
		return sdf.format(calendar.getTime());
	}

	public String getScheduleName() {
		return schedule_name_field.getText().toString().trim();
	}

	public String getSchedule() {
		String schedule = "";
		int x;
		for (x = 0; x < selected_sched.length; x++) {
			if (selected_sched[x]) {
				schedule += (x + "|");
			}
		}
		int length = schedule.length() == 0 ? 0 : (schedule.length() - 1);

		return schedule.substring(0, length);
	}


	public void displaySetting(boolean[] days, int hour, int min, String name) {
		sched.setCurrentHour(hour);
		sched.setCurrentMinute(min);
		schedule_name_field.setText(name);

		/**
		 * set days
		 */
		for (int x = 0; x < days.length; x++) {
			switch (x) {
				case 0: {
					if (days[x]) {
						sun.setBackground(ResourcesCompat.getDrawable(getResources(),
								R.drawable.sched_button_enabled, null));
					}
				}
				;
				break;
				case 1: {
					if (days[x]) {
						mon.setBackground(ResourcesCompat.getDrawable(getResources(),
								R.drawable.sched_button_enabled, null));
					}
				}
				;
				break;
				case 2: {
					if (days[x]) {
						tue.setBackground(ResourcesCompat.getDrawable(getResources(),
								R.drawable.sched_button_enabled, null));
					}
				}
				;
				break;
				case 3: {
					if (days[x]) {
						wed.setBackground(ResourcesCompat.getDrawable(getResources(),
								R.drawable.sched_button_enabled, null));
					}
				}
				;
				break;
				case 4: {
					if (days[x]) {
						thu.setBackground(ResourcesCompat.getDrawable(getResources(),
								R.drawable.sched_button_enabled, null));
					}
				}
				;
				break;
				case 5: {
					if (days[x]) {
						fri.setBackground(ResourcesCompat.getDrawable(getResources(),
								R.drawable.sched_button_enabled, null));
					}
				}
				;
				break;
				case 6: {
					if (days[x]) {
						sat.setBackground(ResourcesCompat.getDrawable(getResources(),
								R.drawable.sched_button_enabled, null));
					}
				}
				;
				break;
			}
		}
	}

	/**/
	public void onRemoveSchedule(MenuItem view) {
		final SimpleCallerUserInterface simpleCallerUserInterface = this;
		AlertDialog.Builder dialog = new AlertDialog.Builder(this);
		dialog.setTitle(R.string.confirm_delete_schedule_title);
		dialog.setMessage(getString(R.string.confirm_delete_schedule_message)
				+ " \"" + scheduleDTO.getName() + "\"" + "?");
		dialog.setCancelable(true);
		dialog.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				scheduleService.deleteSchedule(scheduleDTO.getId(), simpleCallerUserInterface);
			}
		});
		dialog.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});

		dialog.create().show();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.schedule_remove_button, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public void requestStatus(boolean success, String message) {
		if (success) {
			Toast.makeText(this, message, Toast.LENGTH_LONG).show();
			finish();
		} else {
			try {
				JsonElement element = new JsonParser().parse(message);
				if (element == null) {
					Toast.makeText(this, message, Toast.LENGTH_LONG).show();
				} else {
					JsonObject obj = element.getAsJsonObject();
					if (obj == null) {
						Toast.makeText(this, message, Toast.LENGTH_LONG).show();
					} else {
						boolean hasnormalerror = false;
						JsonElement name = obj.get("name");
						JsonElement time = obj.get("time");
						JsonElement repeat = obj.get("repeat");
						JsonElement timeBeforeNotification = obj.get("timeBeforeNotification");
						JsonElement generalMessage = obj.get("message");
						if (name != null && !name.getAsString().isEmpty()) {
							til_schedule_name.setError(name.getAsString());
							hasnormalerror = true;
						}
						if (time != null && !time.getAsString().isEmpty()) {
							Toast.makeText(this, time.getAsString(), Toast.LENGTH_LONG).show();
							hasnormalerror = true;
						}
						if (repeat != null && !repeat.getAsString().isEmpty()) {
							Toast.makeText(this, repeat.getAsString(), Toast.LENGTH_LONG).show();
							hasnormalerror = true;
						}
						if (timeBeforeNotification != null && !timeBeforeNotification.getAsString().isEmpty()) {
							Toast.makeText(this, timeBeforeNotification.getAsString(), Toast.LENGTH_LONG).show();
							hasnormalerror = true;
						}
						if (generalMessage != null && !generalMessage.getAsString().isEmpty()) {
							Toast.makeText(this, generalMessage.getAsString(), Toast.LENGTH_LONG).show();
							hasnormalerror = true;
						}
						if (!hasnormalerror) {
							Toast.makeText(this, message, Toast.LENGTH_LONG).show();
						}
					}
				}
			} catch (JsonSyntaxException | IllegalStateException err) {
				Log.e(getClass().getName(), err.getMessage());
				Toast.makeText(this, message, Toast.LENGTH_LONG).show();
			}
		}
	}


	@OnClick(R.id.sched_sun)
	public void setScheduleSun() {
		setSchedule(0, sun);
	}

	@OnClick(R.id.sched_mon)
	public void setScheduleMon() {
		setSchedule(1, mon);
	}

	@OnClick(R.id.sched_tue)
	public void setScheduleTue() {
		setSchedule(2, tue);
	}

	@OnClick(R.id.sched_wed)
	public void setScheduleWed() {
		setSchedule(3, wed);
	}

	@OnClick(R.id.sched_thu)
	public void setScheduleThu() {
		setSchedule(4, thu);
	}

	@OnClick(R.id.sched_fri)
	public void setScheduleFri() {
		setSchedule(5, fri);
	}

	@OnClick(R.id.sched_sat)
	public void setScheduleSat() {
		setSchedule(6, sat);
	}

}
