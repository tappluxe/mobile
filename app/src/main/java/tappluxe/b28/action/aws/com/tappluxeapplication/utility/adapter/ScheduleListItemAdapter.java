package tappluxe.b28.action.aws.com.tappluxeapplication.utility.adapter;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import tappluxe.b28.action.aws.com.tappluxeapplication.R;
import tappluxe.b28.action.aws.com.tappluxeapplication.ui.custom.TappluxeSwitchCompat;
import tappluxe.b28.action.aws.com.tappluxeapplication.ui.schedule.UpdateSchedule;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.ListViewParentInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.ToggleScheduleStatusInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.ScheduleDTO;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.service.ScheduleService;

/**
 * Created by benjo.uriarte on 3/7/2017.
 */

public class ScheduleListItemAdapter extends ArrayAdapter<ScheduleDTO> implements ToggleScheduleStatusInterface {

	private Activity parentActivity;
	private ScheduleService scheduleService;
	private ListViewParentInterface listViewParentInterface;

	public ScheduleListItemAdapter(@NonNull List<ScheduleDTO> objects, Activity parentActivity, ListViewParentInterface listViewParentInterface) {
		super(parentActivity, 0, objects);
		this.parentActivity = parentActivity;
		scheduleService = new ScheduleService(parentActivity);
		this.listViewParentInterface = listViewParentInterface;
	}

	@NonNull
	@Override
	public View getView(int position, @Nullable View convertView, @NonNull final ViewGroup parent) {
		final ScheduleDTO scheduleDTO = getItem(position);

		if (convertView == null) {
			convertView = LayoutInflater.from(getContext()).inflate(R.layout.schedule_adapter_layout,
					parent, false);
		}

		final TextView scheduleName = (TextView) convertView.findViewById(R.id.schedule_name_text_view);
		TextView scheduleTime = (TextView) convertView.findViewById(R.id.schedule_time_text_view);
		TextView scheduleDays = (TextView) convertView.findViewById(R.id.schedule_days_text_view);
		LinearLayout schedule_detail_holder = (LinearLayout) convertView.findViewById(R.id.schedule_detail_holder);
		final TappluxeSwitchCompat scheduleStatus = (TappluxeSwitchCompat) convertView.findViewById(R.id.schedule_status_switch);

		scheduleStatus.setCheckedFromCode(scheduleDTO.isActive());
		DateFormat format = new SimpleDateFormat(parentActivity.getString(R.string.recieve_time_format));
		String newTime = null;
		try {
			Date time;
			time = format.parse(scheduleDTO.getTime());
			Calendar calendar = Calendar.getInstance();
			if (time == null)
				Log.e(getClass().getName(), "time null");
			if (calendar == null)
				Log.e(getClass().getName(), " calendar null");
			calendar.set(Calendar.HOUR_OF_DAY, time.getHours());
			calendar.set(Calendar.MINUTE, time.getMinutes());
			newTime = (calendar.get(Calendar.HOUR) == 0 ? 12 : calendar.get(Calendar.HOUR)) + ":" + ((calendar.get(Calendar.MINUTE) < 10) ? "0" + calendar.get(Calendar.MINUTE) : calendar.get(Calendar.MINUTE)) + " "
					+ ((calendar.get(Calendar.AM_PM) == Calendar.PM) ? "PM" : "AM");
		} catch (ParseException e) {
			Toast.makeText(parentActivity, "Invalid Time Format received from Server",
					Toast.LENGTH_LONG).show();
		}

		scheduleTime.setText(newTime);
		scheduleName.setText(scheduleDTO.getName());

		String[] numdays = scheduleDTO.getRepeat().split("\\|");
		ArrayList<String> days = new ArrayList<>();

		for (int i = 0; i < numdays.length; i++) {
			switch (Integer.parseInt(numdays[i])) {
				case 0:
					days.add("Sun");
					break;
				case 1:
					days.add("Mon");
					break;
				case 2:
					days.add("Tue");
					break;
				case 3:
					days.add("Wed");
					break;
				case 4:
					days.add("Thurs");
					break;
				case 5:
					days.add("Fri");
					break;
				case 6:
					days.add("Sat");
					break;
			}
		}
		scheduleDays.setText(TextUtils.join(", ", days));

		schedule_detail_holder.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(parentActivity, UpdateSchedule.class);
				intent.putExtra("schedule", scheduleDTO);
				parentActivity.startActivity(intent);
			}
		});
		scheduleStatus.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, final boolean isChecked) {
				if (!scheduleStatus.isFromCode()) {
					scheduleStatus.setEnabled(false);
					scheduleStatus.setCheckedFromCode(!isChecked);

					AlertDialog.Builder dialog =
							new AlertDialog.Builder(parentActivity);
					dialog.setCancelable(true);

					if (isChecked) {
						dialog.setTitle(R.string.confirm_activate_schedule_title);
						dialog.setMessage(parentActivity.getString(R.string.confirm_activate_schedule_message)
								+ " \"" + scheduleDTO.getName() + "\" " + "?");
					} else {
						dialog.setTitle(R.string.confirm_deactivate_schedule_title);
						dialog.setMessage(parentActivity.getString(R.string.confirm_deactivate_schedule_message)
								+ " \"" + scheduleDTO.getName() + "\" " + "?");
					}

					dialog.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							scheduleService.toggleSchedule(scheduleDTO.getId(), isChecked, scheduleStatus, ScheduleListItemAdapter.this);
						}
					});
					dialog.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.cancel();
						}
					});
					dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
						@Override
						public void onCancel(DialogInterface dialog) {
							scheduleStatus.setCheckedFromCode(!isChecked);
							scheduleStatus.setEnabled(true);
						}
					});
					dialog.create().show();
				}
			}
		});

		return convertView;
	}

	@Override
	public void toggleScheduleResult(boolean status, String message, View switchCompat) {
		TappluxeSwitchCompat tappluxeSwitchCompat = (TappluxeSwitchCompat) switchCompat;
		if (status) {
			tappluxeSwitchCompat.setCheckedFromCode(!tappluxeSwitchCompat.isChecked());
			//listViewParentInterface.refresh();
		} else {
			try {
				JsonElement element = new JsonParser().parse(message);
				if (element == null) {
					Toast.makeText(parentActivity, message, Toast.LENGTH_LONG).show();
				} else {
					JsonObject obj = element.getAsJsonObject();
					if (obj == null) {
						Toast.makeText(parentActivity, message, Toast.LENGTH_LONG).show();
					} else {
						JsonElement generalMessage = obj.get("message");
						if (generalMessage != null && !generalMessage.getAsString().isEmpty()) {
							Toast.makeText(parentActivity, generalMessage.getAsString(), Toast.LENGTH_LONG).show();
						} else
							Toast.makeText(parentActivity, message, Toast.LENGTH_LONG).show();
					}
				}
			} catch (JsonSyntaxException | IllegalStateException err) {
				Log.e(getClass().getName(), err.getMessage());
				Toast.makeText(parentActivity, message, Toast.LENGTH_LONG).show();
			}
		}
		tappluxeSwitchCompat.setEnabled(true);
	}
}
