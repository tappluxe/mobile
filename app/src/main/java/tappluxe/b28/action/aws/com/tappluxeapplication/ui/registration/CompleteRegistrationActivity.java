package tappluxe.b28.action.aws.com.tappluxeapplication.ui.registration;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import butterknife.ButterKnife;
import butterknife.OnClick;
import tappluxe.b28.action.aws.com.tappluxeapplication.R;
import tappluxe.b28.action.aws.com.tappluxeapplication.ui.login.Login;

public class CompleteRegistrationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_complete_registration);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.go_to_log_in)
    public void onDoneSignUp() {
        Intent intent = new Intent(this, Login.class);
        startActivity(intent);
        finish();
    }
}
