package tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces;

import java.util.List;

import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.DeviceStatusDTO;

/**
 * Created by jay.bilocura on 2/28/2017.
 */

public interface ListDeviceStatusInterface extends SimpleCallerUserInterface{
    void setListDeviceStatus(List<DeviceStatusDTO> deviceStatuses);
}
