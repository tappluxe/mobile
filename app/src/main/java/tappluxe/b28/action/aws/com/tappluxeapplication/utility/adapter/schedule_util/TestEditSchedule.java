package tappluxe.b28.action.aws.com.tappluxeapplication.utility.adapter.schedule_util;

import android.util.Log;

/**
 * Created by franc.margallo on 3/9/2017.
 */

public class TestEditSchedule {

    public TestEditSchedule() {

    }

    public int[] selectedDays(){
        int[] days = {0,1,0,0,1,0,0};
        return days;
    }

    public String schedName(){
        String name = "My Schedule";
        return name;
    }

    public int[] time(){
        int hour = 3;
        int minute = 30;
        int[] time = {hour, minute};
        return time;
    }

    public void updateSchedule(String name, String time, String repeat, int timeBeforeNotification) {
        Log.d("update schedule",""+name+"\n"+time+"\n"+repeat+"\n"+timeBeforeNotification+"");
    }


}
