package tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces;

import android.widget.TextView;

import tappluxe.b28.action.aws.com.tappluxeapplication.ui.custom.TappluxeSwitchCompat;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.UnitSocketDTO;

/**
 * Created by ariel.fabilena on 3/27/2017.
 */

public interface GetStatusSocketInterface {
	void requestSocketStatus(Boolean state, final UnitSocketDTO socket, TextView turnedOnElapsedTime, TappluxeSwitchCompat switchCompat);
	void socketError(String message);
}
