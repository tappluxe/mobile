package tappluxe.b28.action.aws.com.tappluxeapplication.utility.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import tappluxe.b28.action.aws.com.tappluxeapplication.R;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.SimpleCallerUserInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.UnitDTO;

/**
 * Created by jude.dassun on 2/28/2017.
 */

public class UnitSpinnerAdapter extends ArrayAdapter<UnitDTO>{

	public UnitSpinnerAdapter (List<UnitDTO> objects, Activity parentActivity) {
		super(parentActivity.getApplicationContext(), R.layout.unit_spinner_adapter_layout, objects);
	}

	@NonNull
	@Override
	public View getView(int position, @Nullable View convertView, @NonNull final ViewGroup parent) {
		final UnitDTO unit = getItem(position);
		if(convertView == null){
			convertView = LayoutInflater.from(getContext()).inflate(R.layout.unit_spinner_adapter_layout, parent, false);
		}
		TextView name = (TextView) convertView.findViewById(R.id.unit_spinner_name);
		name.setText(unit.getName());
		return convertView;
	}
}
