package tappluxe.b28.action.aws.com.tappluxeapplication.ui.forgot_password.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import tappluxe.b28.action.aws.com.tappluxeapplication.R;
import tappluxe.b28.action.aws.com.tappluxeapplication.validators.PasswordValidator;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ResetPasswordFragment.OnResetPasswordFragmentListener} interface
 * to handle interaction events.
 * Use the {@link ResetPasswordFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ResetPasswordFragment extends Fragment {
    private static final String USER_EMAIL = "user_email";
    private String email;
    private OnResetPasswordFragmentListener mListener;

    @BindView(R.id.et_reset_new_pass) public EditText etNewPass;
    @BindView(R.id.et_reset_confirm_pass) public EditText etConfirmPass;
    @BindView(R.id.til_reset_new_pass) public TextInputLayout tilNewPass;
    @BindView(R.id.til_reset_confirm_pass) public TextInputLayout tilConfirmPass;

    public ResetPasswordFragment() {
        // Required empty public constructor
    }

    public static ResetPasswordFragment newInstance(String email) {
        ResetPasswordFragment fragment = new ResetPasswordFragment();
        Bundle args = new Bundle();
        args.putString(USER_EMAIL, email);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            email = getArguments().getString(USER_EMAIL);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.reset_password_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnResetPasswordFragmentListener) {
            mListener = (OnResetPasswordFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnResetPasswordFragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @OnClick(R.id.btn_reset_password)
    public void resetPassword(){
        String newPass = etNewPass.getText().toString();
        String confirmPass = etConfirmPass.getText().toString();
        if (isValidPassword(newPass, confirmPass)) {
            mListener.onResetPassword(email, newPass, confirmPass);
        }
    }

    @OnTextChanged(R.id.et_reset_new_pass)
    public void newPassChanged() {
        tilNewPass.setErrorEnabled(false);
    }

    @OnTextChanged(R.id.et_reset_confirm_pass)
    public void confirmPassChanged() {
        tilConfirmPass.setErrorEnabled(false);
    }


    public boolean isValidPassword(String password, String confirmPassword) {

        PasswordValidator pv = new PasswordValidator();

        if(password.isEmpty()) {
            tilNewPass.setErrorEnabled(true);
            tilNewPass.setError(getText(R.string.empty_password_message));
            return false;
        }
        else if(password.length()
                < getResources().getInteger(R.integer.min_password)) {
            tilNewPass.setErrorEnabled(true);
            tilNewPass.setError(getText(R.string.invalid_password_less_part1of2_message).toString()
                    + " "
                    + getResources().getInteger(R.integer.min_password)
                    + " "
                    + getText(R.string.invalid_password_less_part2of2_message));
            return false;
        }
        else if(!pv.isValid(password)) {
            tilNewPass.setErrorEnabled(true);
            tilNewPass.setError(getText(R.string.invalid_password_do_not_meet_requirements_message));
            return false;
        }
        else if(!password.equals(confirmPassword)) {
            tilConfirmPass.setErrorEnabled(true);
            tilConfirmPass.setError(getText(R.string.invalid_password_do_not_match_message));
            return false;
        }

        return true;
    }

    public interface OnResetPasswordFragmentListener {
        void onResetPassword(String email, String newPass, String confirmPass);
    }
}
