package tappluxe.b28.action.aws.com.tappluxeapplication.ui.user;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import tappluxe.b28.action.aws.com.tappluxeapplication.R;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.SimpleCallerUserInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.validators.PasswordValidator;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.UserDTO;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.service.UserService;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnChangePasswordFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ChangePasswordFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChangePasswordFragment extends Fragment implements SimpleCallerUserInterface{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String USER = "USER";

    private UserDTO user;
    private UserService userService;

    private OnChangePasswordFragmentInteractionListener mListener;

    @BindView(R.id.til_current_pass) public TextInputLayout tilCurrentPassword;
    @BindView(R.id.til_change_new_pass) public TextInputLayout tilNewPassword;
    @BindView(R.id.til_change_confirm_pass) public TextInputLayout tilConfirmPassword;
    @BindView(R.id.et_current_pass) public EditText etCurrentPassword;
    @BindView(R.id.et_change_new_pass) public EditText etNewPassword;
    @BindView(R.id.et_change_confirm_pass) public EditText etConfirmPassword;

    public ChangePasswordFragment() {
        // Required empty public constructor
    }

    public static ChangePasswordFragment newInstance(UserDTO user) {
        ChangePasswordFragment fragment = new ChangePasswordFragment();
        Bundle args = new Bundle();
        args.putSerializable(USER, user);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            user = (UserDTO) getArguments().getSerializable(USER);
        }
        userService = new UserService(getContext());
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.change_password_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnChangePasswordFragmentInteractionListener) {
            mListener = (OnChangePasswordFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnChangePasswordFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @OnClick(R.id.btn_change_password)
    public void changePassword() {
        String currentPass = etCurrentPassword.getText().toString();
        String newPass = etNewPassword.getText().toString();
        String confirmPass = etConfirmPassword.getText().toString();
        if (isValidPassword(currentPass, newPass, confirmPass)) {
            UserDTO updatedUser = new UserDTO();
            updatedUser.setOldPassword(currentPass);
            updatedUser.setNewPassword(newPass);
            updatedUser.setConfirmPassword(confirmPass);
            userService.updateUser(updatedUser, this);
        }
    }

    @OnTextChanged(R.id.et_current_pass)
    public void oldPassChanged() {
        tilCurrentPassword.setErrorEnabled(false);
        tilCurrentPassword.setError("");
    }

    @OnTextChanged(R.id.et_change_new_pass)
    public void newPassChanged() {
        tilNewPassword.setErrorEnabled(false);
        tilNewPassword.setError("");
    }

    @OnTextChanged(R.id.et_change_confirm_pass)
    public void confirmPassChanged() {
        tilConfirmPassword.setErrorEnabled(false);
        tilConfirmPassword.setError("");
    }

    public boolean isValidPassword(String currentPassword, String newPassword, String confirmPassword) {

        PasswordValidator pv = new PasswordValidator();
        if (currentPassword.isEmpty()) {
            tilCurrentPassword.setErrorEnabled(true);
            tilCurrentPassword.setError(getText(R.string.empty_password_message));
        }
        if(newPassword.isEmpty()) {
            tilNewPassword.setErrorEnabled(true);
            tilNewPassword.setError(getText(R.string.empty_password_message));
            return false;
        } else if(newPassword.length()
                < getResources().getInteger(R.integer.min_password)) {
            tilNewPassword.setErrorEnabled(true);
            tilNewPassword.setError(getText(R.string.invalid_password_less_part1of2_message).toString()
                    + " "
                    + getResources().getInteger(R.integer.min_password)
                    + " "
                    + getText(R.string.invalid_password_less_part2of2_message));
            return false;
        } else if(!pv.isValid(newPassword)) {
            tilNewPassword.setErrorEnabled(true);
            tilNewPassword.setError(getText(R.string.invalid_password_do_not_meet_requirements_message));
            return false;
        } else if(!newPassword.equals(confirmPassword)) {
            tilConfirmPassword.setErrorEnabled(true);
            tilConfirmPassword.setError(getText(R.string.invalid_password_do_not_match_message));
            return false;
        }

        return true;
    }

    @Override
    public void requestStatus(boolean success, String message) {
        if (success) {
            Toast.makeText(getContext(), "Password successfully changed.", Toast.LENGTH_LONG).show();
            String newPass = etNewPassword.getText().toString();
            user.setPassword(newPass);
            etCurrentPassword.setText("");
            etNewPassword.setText("");
            etConfirmPassword.setText("");
            tilConfirmPassword.setErrorEnabled(false);
            tilCurrentPassword.setErrorEnabled(false);
            tilNewPassword.setErrorEnabled(false);
            tilConfirmPassword.setError("");
            tilCurrentPassword.setError("");
            tilNewPassword.setError("");
            mListener.onChangePasswordDone(user);
        } else {
            JsonObject obj = new JsonParser().parse(message).getAsJsonObject();
            String msg = obj.get("oldPassword").getAsString();
            tilCurrentPassword.setErrorEnabled(true);
            tilCurrentPassword.setError(msg);
        }
    }

    public interface OnChangePasswordFragmentInteractionListener {
        void onChangePasswordDone(UserDTO user);
    }
}
