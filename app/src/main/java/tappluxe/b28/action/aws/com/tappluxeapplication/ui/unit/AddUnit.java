package tappluxe.b28.action.aws.com.tappluxeapplication.ui.unit;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import tappluxe.b28.action.aws.com.tappluxeapplication.BaseActivity;
import tappluxe.b28.action.aws.com.tappluxeapplication.MainActivity;
import tappluxe.b28.action.aws.com.tappluxeapplication.R;
import tappluxe.b28.action.aws.com.tappluxeapplication.ui.main.TabHolder;
import tappluxe.b28.action.aws.com.tappluxeapplication.ui.main.UnitFragment;
import tappluxe.b28.action.aws.com.tappluxeapplication.ui.qrcode.QRCodeScanActivity;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.FormInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.SimpleCallerUserInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.validators.ProductKeyValidator;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.service.UnitService;

public class AddUnit extends BaseActivity implements FormInterface, SimpleCallerUserInterface {
	@BindView(R.id.et_add_unit_name)
	TextInputEditText etUnitName;

	@BindView(R.id.et_add_product_key)
	TextInputEditText etProductKey;

	@BindView(R.id.btn_add_accept)
	Button btnAccept;

	@BindView(R.id.til_et_add_product_key)
	TextInputLayout tilAddProductKey;

	@BindView(R.id.til_et_add_unit_name)
	TextInputLayout tilAddUnitName;

	private UnitService unitService;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.unit_add_activity);
		setTitle(getString(R.string.add_tappluxe_unit));
		ButterKnife.bind(this);
		unitService = new UnitService(this);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);

		etProductKey.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				tilAddProductKey.setError(null);
			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		});

		etUnitName.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				tilAddUnitName.setError(null);
			}

			@Override
			public void afterTextChanged(Editable s) {}
		});
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
			case android.R.id.home:
				this.finish();
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@OnClick(R.id.btn_add_accept)
	public void onBtnAccept() {
		String productKey = etProductKey.getText().toString().trim();
		String name = etUnitName.getText().toString().trim();
		if (isValidProductKey(productKey) && isValidUnitName(name)) {
			unitService.addUnitPost(productKey, name, this);
			enableForm(false);
		}
	}

	@Override
	public void clearForm() {
		etProductKey.setText("");
		etUnitName.setText("");
	}

	@Override
	public void enableForm(boolean enabled) {
		btnAccept.setEnabled(enabled);
	}

	@Override
	public void requestStatus(boolean success, String message) {
		if (success) {
			Toast.makeText(this, message, Toast.LENGTH_LONG).show();
			Intent intent = new Intent(this, MainActivity.class);
			intent.putExtra(TabHolder.GO_TO_FRAGMENT, UnitFragment.GO_TO_UNIT_FRAGMENT);
			intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			finish();
		}
		else {
			try {
				JsonElement element = new JsonParser().parse(message);
				if (element == null) {
					Toast.makeText(this, message, Toast.LENGTH_LONG).show();
				} else {
					JsonObject obj = element.getAsJsonObject();
					if (obj == null) {
						Toast.makeText(this, message, Toast.LENGTH_LONG).show();
					} else {
						boolean hasnormalerror = false;
						JsonElement productId = obj.get("productId");
						JsonElement name = obj.get("name");
						JsonElement generalMessage = obj.get("message");
						if (productId != null && !productId.getAsString().isEmpty()) {
							tilAddProductKey.setError(productId.getAsString());
							hasnormalerror = true;
						}
						if (name != null && !name.getAsString().isEmpty()) {
							tilAddUnitName.setError(name.getAsString());
							hasnormalerror = true;
						}
						if(generalMessage != null && !generalMessage.getAsString().isEmpty()){
							Toast.makeText(this, generalMessage.getAsString(), Toast.LENGTH_LONG).show();
							hasnormalerror = true;
						}

						if(!hasnormalerror){
							Toast.makeText(this, message, Toast.LENGTH_LONG).show();
						}
					}
				}
			} catch (JsonSyntaxException | IllegalStateException err) {
				Toast.makeText(this, message, Toast.LENGTH_LONG).show();
			}
		}
		enableForm(true);
	}

	@OnClick(R.id.btn_scanQR)
	public void scanQR(View view) {
		Intent intent = new Intent(getApplicationContext(), QRCodeScanActivity.class);
		startActivityForResult(intent, QRCodeScanActivity.QRCODE_READER_REQUEST_CODE);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == QRCodeScanActivity.QRCODE_READER_REQUEST_CODE) {
			if (resultCode == RESULT_OK) {
				if (data != null) {
                    String result = data.getStringExtra(QRCodeScanActivity.QRCODE);
					etProductKey.setText(result);
				}
			}
		} else {
			super.onActivityResult(requestCode, resultCode, data);
		}
	}

	public boolean isValidProductKey(String productKey) {
		if (productKey.isEmpty()) {
			tilAddProductKey.setError(getText(R.string.empty_prod_key_message));
			return false;
		} else {
			ProductKeyValidator productKeyValidator = new ProductKeyValidator();
			if (!productKeyValidator.isValid(productKey)) {
				tilAddProductKey.setError(getText(R.string.invalid_prod_key_format_message));
				return false;
			}
		}
		return true;
	}

	public boolean isValidUnitName(String unitName) {
		if (unitName.isEmpty()) {
			tilAddUnitName.setError(getText(R.string.empty_unit_name_message));
			return false;
		}
		return true;
	}
}
