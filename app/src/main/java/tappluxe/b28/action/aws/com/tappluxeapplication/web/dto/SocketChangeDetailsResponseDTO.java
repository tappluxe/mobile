package tappluxe.b28.action.aws.com.tappluxeapplication.web.dto;

import java.io.Serializable;

/**
 * Created by ariel.fabilena on 3/16/2017.
 */

public class SocketChangeDetailsResponseDTO implements Serializable {
	private Long id;
	private Long socketId;
	private String updateDate;
	private Boolean state;

	public SocketChangeDetailsResponseDTO(Long id, Long socketId, String updateDate, Boolean state) {
		this.id = id;
		this.updateDate = updateDate;
		this.state = state;
		this.socketId = socketId;
	}

	public SocketChangeDetailsResponseDTO() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public Boolean getState() {
		return state;
	}

	public void setState(Boolean state) {
		this.state = state;
	}

	public Long getSocketId() {
		return socketId;
	}

	public void setSocketId(Long socketId) {
		this.socketId = socketId;
	}

}
