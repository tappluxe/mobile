package tappluxe.b28.action.aws.com.tappluxeapplication.entities;

/**
 * Created by jay.bilocura on 3/9/2017.
 */

public class Country {
    private String name;
    private String isoCode;
    private int dialCode;

    public Country(String name, String isoCode, int dialCode) {
        setName(name);
        setIsoCode(isoCode);
        setDialCode(dialCode);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIsoCode() {
        return isoCode;
    }

    public void setIsoCode(String isoCode) {
        this.isoCode = isoCode.toUpperCase();
    }

    public int getDialCode() {
        return dialCode;
    }

    public void setDialCode(int dialCode) {
        this.dialCode = dialCode;
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof Country) && (((Country) o).getIsoCode().toUpperCase().equals(this.getIsoCode().toUpperCase()));
    }


}
