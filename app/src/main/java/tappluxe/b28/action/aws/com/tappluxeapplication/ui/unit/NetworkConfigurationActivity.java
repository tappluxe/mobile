package tappluxe.b28.action.aws.com.tappluxeapplication.ui.unit;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import tappluxe.b28.action.aws.com.tappluxeapplication.R;
import tappluxe.b28.action.aws.com.tappluxeapplication.ui.unit.setup.InputSSIDActivity;
import tappluxe.b28.action.aws.com.tappluxeapplication.ui.unit.setup.UnitConnectionActivity;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.UnitDTO;

/**
 * Created by jay.bilocura on 3/22/2017.
 */

public class NetworkConfigurationActivity extends AppCompatActivity {

    private UnitDTO unit;

    @BindView(R.id.tv_curr_ssid) public TextView tvCurrSsid;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.network_configuration_activity);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        unit = (UnitDTO) intent.getSerializableExtra("unit");
        setTitle(unit.getName());
        tvCurrSsid.setText(unit.getSsid());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick(R.id.btn_reconfigure_network)
    public void reconfigure() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(R.string.network_config_reconfigure);
        dialog.setMessage(R.string.network_reconfigure_inst);
        dialog.setCancelable(true);
        dialog.setPositiveButton(R.string.network_config_proceed, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(getApplicationContext(), UnitConnectionActivity.class);
                intent.putExtra("unit", unit);
                intent.putExtra("reconfigure", true);
                startActivityForResult(intent, InputSSIDActivity.RECONFIGURE_REQUEST_CODE);
            }
        });
        dialog.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        dialog.create().show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == InputSSIDActivity.RECONFIGURE_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    StringBuilder builder = new StringBuilder();
                    builder.append(data.getCharSequenceExtra(InputSSIDActivity.RECONFIGURE));
                    tvCurrSsid.setText(builder.toString());
                }
            }
        } else super.onActivityResult(requestCode, resultCode, data);
    }

}
