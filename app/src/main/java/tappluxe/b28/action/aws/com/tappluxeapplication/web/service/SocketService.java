package tappluxe.b28.action.aws.com.tappluxeapplication.web.service;

import android.content.Context;
import android.util.Log;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.MultipleSourceSimpleCallerUserInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.session.Session;

/**
 * Created by jude.dassun on 3/23/2017.
 */

public class SocketService extends BaseService {
	private static final String TAG = BaseService.class.getName();

	public SocketService(Context activity) {
		super(activity);
	}

	public Call assignSchedule(Long userUnitId, Long socketId, final Long scheduleId, final String source, final MultipleSourceSimpleCallerUserInterface activity){
		Call<ResponseBody> call = webServiceInterface.assignSchedule(userUnitId, socketId, scheduleId, session.getValue(Session.ACCESS_TOKEN));

		call.enqueue(new Callback<ResponseBody>() {
			@Override
			public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
				if (response.isSuccessful()) {
					if(scheduleId == -1){
						activity.requestStatus(true, "Successfully Unassigned a Schedule", source);
					}
					else{
						activity.requestStatus(true, "Successfully Assigned a Schedule", source);
					}
				} else {
					try {
						String str = response.errorBody().string();
						Log.e(TAG, "error code: " + response.code()+ " onResponse: " + str);
						activity.requestStatus(false, str, source);
					} catch (IOException e) {
						activity.requestStatus(false, e.getMessage(), source);
						e.printStackTrace();
					}
				}
			}

			@Override
			public void onFailure(Call<ResponseBody> call, Throwable t) {
				Log.e(TAG, t.getClass().getName());
				if(t.getMessage() != null)
					Log.e(TAG, t.getMessage());
				t.printStackTrace();
				activity.requestStatus(false, "Cannot connect to the server.", source);
			}
		});
		return call;
	}

//	public Call removeSchedule(Long userUnitId, Long socketId, final String source, final MultipleSourceSimpleCallerUserInterface activity){
//		Call<ResponseBody> call = webServiceInterface.removeSchedule(userUnitId, socketId, session.getValue(Session.ACCESS_TOKEN));
//
//		call.enqueue(new Callback<ResponseBody>() {
//			@Override
//			public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//				if (response.isSuccessful()) {
//					Log.e(TAG, "DeviceService.addDevicePost - Successfully Detach a Schedule");
//					activity.requestStatus(true, "Successfully Detach a Schedule", source);
//				} else {
//					try {
//						String str = response.errorBody().string();
//						Log.e(TAG, "error code: " + response.code()+ " onResponse: " + str);
//						activity.requestStatus(false, str, source);
//					} catch (IOException e) {
//						activity.requestStatus(false, e.getMessage(), source);
//						e.printStackTrace();
//					}
//				}
//			}
//
//			@Override
//			public void onFailure(Call<ResponseBody> call, Throwable t) {
//				Log.e(TAG, t.getClass().getName());
//				if(t.getMessage() != null)
//					Log.e(TAG, t.getMessage());
//				t.printStackTrace();
//				activity.requestStatus(false, "Cannot connect to the server.", source);
//			}
//		});
//		return call;
//	}
//
//	public Call insertDevice(Long userUnitId, Long socketId, Long deviceId, final String source, final MultipleSourceSimpleCallerUserInterface activity){
//		Call<ResponseBody> call = webServiceInterface.insertDevice(userUnitId, socketId, deviceId, session.getValue(Session.ACCESS_TOKEN));
//
//		call.enqueue(new Callback<ResponseBody>() {
//			@Override
//			public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//				if (response.isSuccessful()) {
//					Log.e(TAG, "DeviceService.addDevicePost - Successfully Attach a Device");
//					activity.requestStatus(true, "Successfully Attach a Device", source);
//				} else {
//					try {
//						String str = response.errorBody().string();
//						Log.e(TAG, "error code: " + response.code()+ " onResponse: " + str);
//						activity.requestStatus(false, str, source);
//					} catch (IOException e) {
//						activity.requestStatus(false, e.getMessage(), source);
//						e.printStackTrace();
//					}
//				}
//			}
//
//			@Override
//			public void onFailure(Call<ResponseBody> call, Throwable t) {
//				Log.e(TAG, t.getClass().getName());
//				if(t.getMessage() != null)
//					Log.e(TAG, t.getMessage());
//				t.printStackTrace();
//				activity.requestStatus(false, "Cannot connect to the server.", source);
//			}
//		});
//		return call;
//	}
//
//	public Call removeDevice(Long userUnitId, Long socketId, final String source, final MultipleSourceSimpleCallerUserInterface activity){
//		Call<ResponseBody> call = webServiceInterface.removeDevice(userUnitId, socketId, session.getValue(Session.ACCESS_TOKEN));
//
//		call.enqueue(new Callback<ResponseBody>() {
//			@Override
//			public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//				if (response.isSuccessful()) {
//					Log.e(TAG, "DeviceService.addDevicePost - Successfully Detach a Device");
//					activity.requestStatus(true, "Successfully Detach a Device", source);
//				} else {
//					try {
//						String str = response.errorBody().string();
//						Log.e(TAG, "error code: " + response.code()+ " onResponse: " + str);
//						activity.requestStatus(false, str, source);
//					} catch (IOException e) {
//						activity.requestStatus(false, e.getMessage(), source);
//						e.printStackTrace();
//					}
//				}
//			}
//
//			@Override
//			public void onFailure(Call<ResponseBody> call, Throwable t) {
//				Log.e(TAG, t.getClass().getName());
//				if(t.getMessage() != null)
//					Log.e(TAG, t.getMessage());
//				t.printStackTrace();
//				activity.requestStatus(false, "Cannot connect to the server.", source);
//			}
//		});
//		return call;
//	}
}
