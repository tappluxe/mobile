package tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces;

import android.widget.Button;
import android.widget.ProgressBar;

import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.UnitDTO;

/**
 * Created by ariel.fabilena on 3/27/2017.
 */

public interface GetUnitStatusInterface {
	void requestUnitStatus(boolean success, String message, UnitDTO unit, ProgressBar progressBar, Button btnLed);
}
