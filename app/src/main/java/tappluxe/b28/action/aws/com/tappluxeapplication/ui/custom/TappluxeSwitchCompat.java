package tappluxe.b28.action.aws.com.tappluxeapplication.ui.custom;

import android.content.Context;
import android.support.v7.widget.SwitchCompat;
import android.util.AttributeSet;

/**
 * Created by jude.dassun on 3/24/2017.
 */

public class TappluxeSwitchCompat extends SwitchCompat {

	private boolean fromCode = false;

	public TappluxeSwitchCompat(Context context) {
		super(context);
	}

	public TappluxeSwitchCompat(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public TappluxeSwitchCompat(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	public void setCheckedFromCode(boolean checked){
		fromCode = true;
		setChecked(checked);
	}
	public boolean isFromCode() {
		return fromCode;
	}

	@Override
	public void setChecked(boolean checked) {
		super.setChecked(checked);
		if(fromCode){
			fromCode = false;
		}
	}
}
