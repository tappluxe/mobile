package tappluxe.b28.action.aws.com.tappluxeapplication.ui.registration.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import tappluxe.b28.action.aws.com.tappluxeapplication.R;
import tappluxe.b28.action.aws.com.tappluxeapplication.ui.custom.MobileNumberInput;

public class ProfileFragment extends Fragment {
    private static final String FIRST_NAME = "first_name";
    private static final String LAST_NAME = "last_name";
    private static final String COUNTRY_ISO = "country";
    private static final String PLAIN_MOBILE_NUMBER = "mobile_number";

    private String firstName;
    private String lastName;
    private String countryIso;
    private String plainMobileNumber;

    @BindView(R.id.til_first_name) public TextInputLayout tilFirstName;
    @BindView(R.id.til_last_name) public TextInputLayout tilLastName;
    @BindView(R.id.et_first_name) public EditText etFirstName;
    @BindView(R.id.et_last_name) public EditText etLastName;
    @BindView(R.id.mobile_input) public MobileNumberInput mobileNumberInput;

    private OnProfileFragmentInteractionListener mListener;

    public ProfileFragment() {
    }

    public static ProfileFragment newInstance(String firstName, String lastName, String countryIso, String mobileNumber) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString(FIRST_NAME, firstName);
        args.putString(LAST_NAME, lastName);
        args.putString(COUNTRY_ISO, countryIso);
        args.putString(PLAIN_MOBILE_NUMBER, mobileNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            firstName = getArguments().getString(FIRST_NAME);
            lastName = getArguments().getString(LAST_NAME);
            countryIso = getArguments().getString(COUNTRY_ISO);
            plainMobileNumber = getArguments().getString(PLAIN_MOBILE_NUMBER);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.profile_fragment, container, false);
        ButterKnife.bind(this, view);
        if (firstName != null && !firstName.isEmpty()) {
            etFirstName.setText(firstName);
        }
        if (lastName != null && !lastName.isEmpty()) {
            etLastName.setText(lastName);
        }
        if (countryIso != null && !countryIso.isEmpty()) {
            mobileNumberInput.setAsDefault(countryIso);
        }
        if (plainMobileNumber != null && !plainMobileNumber.isEmpty()) {
            mobileNumberInput.setText(plainMobileNumber);
        }

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnProfileFragmentInteractionListener) {
            mListener = (OnProfileFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnProfileFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        String firstName = etFirstName.getText().toString();
        String lastName = etLastName.getText().toString();
        String plainMobileNumber = mobileNumberInput.getText();
        String countryIso = mobileNumberInput.getIso();
        mListener.onSaveProfile(firstName, lastName, countryIso, plainMobileNumber);
        super.onDetach();
        mListener = null;
    }

    @OnTextChanged(R.id.et_first_name)
    public void firstNameTextChange() {
        tilFirstName.setError(null);
        tilFirstName.setErrorEnabled(false);
    }

    @OnTextChanged(R.id.et_last_name)
    public void lastNameTextChange() {
        tilLastName.setError(null);
        tilLastName.setErrorEnabled(false);
    }

    @OnClick(R.id.btn_registration_done)
    public void registrationDone() {
        String firstName = etFirstName.getText().toString();
        String lastName = etLastName.getText().toString();
        String plainMobileNumber = mobileNumberInput.getText();
        String countryIso = mobileNumberInput.getIso();
        if(isValidName(firstName, lastName) && mobileNumberInput.isValidMobileNumber()){
            String mobileNumber = mobileNumberInput.getNumber();
            mListener.onProfileFragmentDone(firstName, lastName, countryIso, plainMobileNumber, mobileNumber);
        }

    }


    public boolean isValidName(String firstName, String lastName){
        if(firstName.isEmpty()){
            tilFirstName.setErrorEnabled(true);
            tilFirstName.setError(getText(R.string.empty_first_name_message));
            return false;
        } else if(lastName.isEmpty()){
            tilLastName.setErrorEnabled(true);
            tilLastName.setError(getText(R.string.empty_last_name_message));
            return false;
        }
        return true;
    }

    public interface OnProfileFragmentInteractionListener {
        void onProfileFragmentDone(String firstName, String lastName, String countryIso, String plainMobileNumber, String mobileNumber);
        void onSaveProfile(String firstName, String lastName, String countryIso, String mobileNumber);
    }
}
