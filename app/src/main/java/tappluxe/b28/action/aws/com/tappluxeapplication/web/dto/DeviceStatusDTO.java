package tappluxe.b28.action.aws.com.tappluxeapplication.web.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by jay.bilocura on 2/28/2017.
 */

public class DeviceStatusDTO implements Serializable {

	private Long id;
	private String updateDate;
	private Long socketId;
	private Boolean state;

	public DeviceStatusDTO(Long id, String updateDate, Long socketId, Boolean state) {
		this.id = id;
		this.updateDate = updateDate;
		this.socketId = socketId;
		this.state = state;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public Long getSocketId() {
		return socketId;
	}

	public void setSocketId(Long socketId) {
		this.socketId = socketId;
	}

	public Boolean isState() {
		return state;
	}

	public void setState(Boolean state) {
		this.state = state;
	}
}
