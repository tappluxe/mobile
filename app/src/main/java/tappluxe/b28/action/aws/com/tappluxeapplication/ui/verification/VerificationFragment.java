package tappluxe.b28.action.aws.com.tappluxeapplication.ui.verification;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tappluxe.b28.action.aws.com.tappluxeapplication.R;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.UserDTO;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.service.VerificationService;

/**
 * Created by jay.bilocura on 3/10/2017.
 */

public class VerificationFragment extends Fragment {

    private static final String USER = "user";

    @BindView(R.id.til_verification_code) public TextInputLayout tilVerificationCode;
    @BindView(R.id.et_verification_code) public EditText etVerificationCode;
    @BindView(R.id.tv_resend_verification_code) public TextView tvResendCode;
    @BindView(R.id.btn_submit_verification_code) public Button btnSubmitCode;

    private UserDTO user;
    private OnVerificationDoneFragmentInteractionListener mListener;
    private VerificationService verificationService;

    public VerificationFragment() {

    }

    public static VerificationFragment newInstance(UserDTO user) {
        VerificationFragment fragment = new VerificationFragment();
        Bundle args = new Bundle();
        args.putSerializable(USER, user);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            user = (UserDTO) getArguments().getSerializable(USER);
        }
        setHasOptionsMenu(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.verification_fragment, container, false);
        ButterKnife.bind(this, view);
        verificationService = new VerificationService(this.getContext());
        return view;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnVerificationDoneFragmentInteractionListener) {
            mListener = (OnVerificationDoneFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnVerificationDoneFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @OnClick(R.id.btn_submit_verification_code)
    public void checkCode() {
        String code = etVerificationCode.getText().toString();
        verificationService.checkCode(code, user.getEmail(), new Callback<UserDTO>() {
            @Override
            public void onResponse(Call<UserDTO> call, Response<UserDTO> response) {
                if (response.isSuccessful()) {
                    complete();
                } else {
                    tilVerificationCode.setErrorEnabled(true);
                    tilVerificationCode.setError("Invalid verification code. Code may have expired. Please have a new code resent.");
                }
            }

            @Override
            public void onFailure(Call<UserDTO> call, Throwable t) {

            }
        });
    }

    @OnTextChanged(R.id.et_verification_code)
    public void changeCode() {
        tilVerificationCode.setErrorEnabled(false);
        tilVerificationCode.setError("");
    }

    @OnClick(R.id.tv_resend_verification_code)
    public void resendCode() {
        enableForm(false);
        verificationService.resendCode(user.getEmail(), new Callback<UserDTO>() {
            @Override
            public void onResponse(Call<UserDTO> call, Response<UserDTO> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(getContext(), "Verification code has been resent.", Toast.LENGTH_LONG).show();
                    enableForm(true);
                } else {
                    Toast.makeText(getContext(), response.message(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<UserDTO> call, Throwable t) {
                Log.d("Verify", t.getMessage());
            }
        });
    }

    public void enableForm(boolean enable) {
        btnSubmitCode.setEnabled(enable);
        etVerificationCode.setEnabled(enable);
    }

    public void complete() {
        user.setVerified(true);
        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setTitle(R.string.verification_dialog_title);
        dialog.setMessage(R.string.verification_dialog_msg);
        dialog.setCancelable(false);
        dialog.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mListener.onVerificationDone();
            }
        });
        dialog.create().show();
    }

    public interface OnVerificationDoneFragmentInteractionListener {
        void onVerificationDone();
    }
}
