package tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces;

/**
 * Created by benjo.uriarte on 3/15/2017.
 */

public interface ListViewParentInterface {
	void refresh();
}
