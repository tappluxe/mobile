package tappluxe.b28.action.aws.com.tappluxeapplication.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by benjo.uriarte on 2/13/2017.
 */

public class MobileNumberValidator {

    private Pattern pattern;
    private Matcher matcher;

    private static final String MOBILENUMBER_PATTERN = "^[0-9]{11,}$";

    public MobileNumberValidator() {

        pattern = Pattern.compile(MOBILENUMBER_PATTERN);
    }

    public boolean isValid(final String mobileNumber) {

        matcher = pattern.matcher(mobileNumber);
        return matcher.matches();
    }
}
