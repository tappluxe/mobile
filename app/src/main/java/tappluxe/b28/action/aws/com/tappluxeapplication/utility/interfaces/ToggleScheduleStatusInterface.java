package tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces;

import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.Switch;

/**
 * Created by benjo.uriarte on 3/13/2017.
 */

public interface ToggleScheduleStatusInterface {
	void toggleScheduleResult(boolean status, String message, View switchCompat);
}
