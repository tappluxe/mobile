package tappluxe.b28.action.aws.com.tappluxeapplication.utility.session;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by ariel.fabilena on 3/24/2017.
 */

public class DatabaseHandler extends SQLiteOpenHelper {

	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = "unitStatus.db";
	private static final String TABLE_STATUS = "tableStatus";

	private static final String KEY_UNIT_ID = "unit_id";
	private static final String KEY_STATUS = "status";

	public DatabaseHandler(Context context){
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		String CREATE_STATUS_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_STATUS + "("
				+ KEY_UNIT_ID + " TEXT PRIMARY KEY,"
				+ KEY_STATUS + " TEXT" + ")";
		db.execSQL(CREATE_STATUS_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_STATUS);
		onCreate(db);
	}

	public void addStatus(UnitStatusLedSQL unitStatusLedSQL){
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(KEY_UNIT_ID, unitStatusLedSQL.getUnit_id());
		values.put(KEY_STATUS, unitStatusLedSQL.getStatus());
		db.insert(TABLE_STATUS, null, values);
		db.close();
	}

	public UnitStatusLedSQL getStatus(String unitId){
		SQLiteDatabase db = this.getReadableDatabase();
		String id = "";
		String status = "";
		String query = "SELECT unit_id, status FROM tableStatus WHERE unit_id="+unitId;
		Cursor cursor = db.rawQuery(query, null);

		if (cursor != null) {
			cursor.moveToFirst();
			id = cursor.getString(0);
			status = cursor.getString(1);
		}
		return new UnitStatusLedSQL(id, status);
	}

	public int updateStatus(UnitStatusLedSQL unitStatusLedSQL){
			SQLiteDatabase db = this.getWritableDatabase();

			ContentValues values = new ContentValues();
			values.put(KEY_UNIT_ID, unitStatusLedSQL.getUnit_id());
			values.put(KEY_STATUS, unitStatusLedSQL.getStatus());

			// updating row
			return db.update(TABLE_STATUS, values, KEY_UNIT_ID + " = ?",
					new String[] { String.valueOf(unitStatusLedSQL.getUnit_id())});
	}
	public void deleteStatus(UnitStatusLedSQL unitStatusLedSQL){
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_STATUS, KEY_UNIT_ID + " = ?",
				new String[] { String.valueOf(unitStatusLedSQL.getUnit_id()) });
		db.close();
	}

	public boolean keyExists(String unitId){
		SQLiteDatabase db = this.getWritableDatabase();
		String query = "SELECT * FROM tableStatus WHERE unit_id="+unitId;
		Cursor cursor = db.rawQuery(query, null);
		boolean exists = (cursor.getCount() > 0);
		cursor.close();
		return exists;
	}
}
