package tappluxe.b28.action.aws.com.tappluxeapplication.utility.session;

import android.content.Context;
import android.content.SharedPreferences;

import tappluxe.b28.action.aws.com.tappluxeapplication.R;

/**
 * Created by ariel.fabilena on 3/6/2017.
 */

public class Session {
	public static final String ACCESS_TOKEN = "ACCESS_TOKEN";
	public static final String REFRESH_TOKEN = "REFRESH_TOKEN";
	public static final String USER_EMAIL= "email";
	public static final String USER_PASSWORD = "password";
	public static final String LOG_STATE = "KeepMeLoggedIn";
	public static final String REMEMBERME = "REMEMBER_ME";
	public static final String UNIT_NAME = "UNIT_NAME";
	public static final String FIRST_LOAD = "FIRST LOAD";

	private SharedPreferences sharedPref;

	public Session(Context c) {
		sharedPref = c.getSharedPreferences(c.getString(R.string.file_name), Context.MODE_PRIVATE);
	}

	public void setValue(String key, String value){
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putString(key, value);
		editor.commit();
	}

	public String getValue(String key){
		return sharedPref.getString(key, null);
	}

	public void setBooleanValue(String key, boolean value){
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putBoolean(key, value);
		editor.commit();
	}

	public boolean getBooleanValue(String key){
		return sharedPref.getBoolean(key, false);
	}

	public void removeKey(String key){
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.remove(key);
		editor.commit();
	}
}
