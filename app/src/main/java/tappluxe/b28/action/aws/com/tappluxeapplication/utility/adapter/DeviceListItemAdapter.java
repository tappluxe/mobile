package tappluxe.b28.action.aws.com.tappluxeapplication.utility.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import java.util.List;

import tappluxe.b28.action.aws.com.tappluxeapplication.R;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.ListUnitInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.ListViewParentInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.SimpleCallerUserInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.DeviceDTO;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.UnitDTO;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.service.DeviceService;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.service.UnitService;

/**
 * Created by jude.dassun on 3/23/2017.
 */

public class DeviceListItemAdapter extends ArrayAdapter<DeviceDTO> implements SimpleCallerUserInterface, ListUnitInterface{

	private Activity parentActivity;
	private DeviceService deviceService;
	private UnitService unitService;
	private ListViewParentInterface parentList;
	List<UnitDTO> listUnits;

	TextView device_unit;
	TextView device_socket;
	TextView device_name;

	public DeviceListItemAdapter(@NonNull List objects, Activity parentActivity, ListViewParentInterface parentList) {
		super(parentActivity, 0, objects);
		this.parentActivity = parentActivity;
		deviceService = new DeviceService(parentActivity);
		unitService = new UnitService(parentActivity);
		this.parentList = parentList;
	}

	@NonNull
	@Override
	public View getView(int position, @Nullable View convertView, @NonNull final ViewGroup parent) {
		final DeviceDTO device = getItem(position);
		unitService.getUnitList(this);
		if (convertView == null) {
			convertView = LayoutInflater.from(getContext()).inflate(R.layout.device_list_item_adapter_layout, parent, false);
		}
		device_name = (TextView) convertView.findViewById(R.id.device_name);
		device_unit = (TextView) convertView.findViewById(R.id.device_unit);
		device_socket = (TextView) convertView.findViewById(R.id.device_socket);
		LinearLayout device_layout = (LinearLayout) convertView.findViewById(R.id.layout_device_view_info);
		final ImageButton device_menu = (ImageButton) convertView.findViewById(R.id.device_menu_btn);
		device_name.setText(device.getName());
		device_socket.setVisibility(View.VISIBLE);

		if (device.getUserUnitSocketId() == null || device.getUserUnitSocketId() == -1l) {
			device_unit.setText("Available");
			device_socket.setVisibility(View.GONE);
		} else{
			device_unit.setText("Attached to a socket");
			device_socket.setVisibility(View.GONE);
		}

		device_layout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				AlertDialog.Builder dialog = new android.app.AlertDialog.Builder(parentActivity);
				LayoutInflater inflater = parentActivity.getLayoutInflater();;
				View parentView = inflater.inflate(R.layout.device_edit_dialog,null);
				final EditText et_device_name = (EditText) parentView.findViewById(R.id.et_device_name);
				et_device_name.setText(device.getName());
				dialog.setTitle(R.string.device_edit_title);
				dialog.setCancelable(true);
				dialog.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						deviceService.updateDevice(device.getId(), et_device_name.getText().toString(), DeviceListItemAdapter.this);
					}
				});

				dialog.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});

				dialog.setView(parentView);
				dialog.create().show();
			}
		});
		device_menu.setOnClickListener(new View.OnClickListener(){
			@Override
			public void onClick(View v) {
				PopupMenu popup = new PopupMenu(getContext(), device_menu);
				popup.getMenuInflater().inflate(R.menu.device_options_menu, popup.getMenu());

				popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener(){
					@Override
					public boolean onMenuItemClick(MenuItem item) {
						switch(item.getItemId()){
							case R.id.menu_edit_device_option:
								AlertDialog.Builder dialog = new android.app.AlertDialog.Builder(parentActivity);
								LayoutInflater inflater = parentActivity.getLayoutInflater();
								View parentView = inflater.inflate(R.layout.device_edit_dialog,null);
								final EditText et_device_name = (EditText) parentView.findViewById(R.id.et_device_name);
								et_device_name.setText(device.getName());
								dialog.setTitle(R.string.device_edit_title);
								dialog.setCancelable(true);
								dialog.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										deviceService.updateDevice(device.getId(), et_device_name.getText().toString(), DeviceListItemAdapter.this);
									}
								});

								dialog.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										dialog.cancel();
									}
								});

								dialog.setView(parentView);
								dialog.create().show();
								return true;
							case R.id.menu_delete_device_option:
								dialog = new android.app.AlertDialog.Builder(parentActivity);
								dialog.setTitle(R.string.delete_device_title);
								dialog.setMessage(R.string.delete_device_message);
								dialog.setCancelable(true);
								dialog.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										deviceService.deleteDevice(device.getId(), DeviceListItemAdapter.this);
									}
								});

								dialog.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										dialog.cancel();
									}
								});
								dialog.create().show();
								return true;
						}
						return false;
					}
				});
				popup.show();
			}
		});
		return convertView;
	}

	@Override
	public void requestStatus(boolean success, String message) {
		if(success){
			Toast.makeText(parentActivity, message, Toast.LENGTH_LONG).show();
			parentList.refresh();
		}
		else{
			try {
				JsonElement element = new JsonParser().parse(message);
				if (element == null) {
					Toast.makeText(parentActivity, message, Toast.LENGTH_LONG).show();
				} else {
					JsonObject obj = element.getAsJsonObject();
					if (obj == null) {
						Toast.makeText(parentActivity, message, Toast.LENGTH_LONG).show();
					} else {
						JsonElement name = obj.get("name");
						JsonElement generalMessage = obj.get("message");
						if(name != null && !name.getAsString().isEmpty()){
							Toast.makeText(parentActivity, name.getAsString(), Toast.LENGTH_LONG).show();
						}
						else if (generalMessage != null && !generalMessage.getAsString().isEmpty()) {
							Toast.makeText(parentActivity, generalMessage.getAsString(), Toast.LENGTH_LONG).show();
						} else
							Toast.makeText(parentActivity, message, Toast.LENGTH_LONG).show();
					}
				}
			} catch (JsonSyntaxException | IllegalStateException err) {
				Log.e(getClass().getName(), err.getMessage());
				Toast.makeText(parentActivity, message, Toast.LENGTH_LONG).show();
			}
		}
	}

	@Override
	public void setListUnit(List<UnitDTO> unit) {
		listUnits = unit;
//		if (device.getUserUnitSocketId() == null || device.getUserUnitSocketId() == -1l) {
//			device_unit.setText("Available");
//			device_socket.setVisibility(View.GONE);
//		} else {
//			outloop:
//			for (int i = 0; i < listUnits.size(); i++) {
//				for (int j = 0; i < listUnits.get(i).getSockets().size(); j++) {
//					if (listUnits.get(i).getSockets().get(j).getId() == device.getUserUnitSocketId()) {
//						device_unit.setText(listUnits.get(i).getName());
//						device_socket.setText("Socket " + listUnits.get(i).getSockets().get(j).getSocketNumber());
//						break outloop;
//					}
//				}
//			}
//		}
	}

	@Override
	public void getError(String message) {

	}
}
