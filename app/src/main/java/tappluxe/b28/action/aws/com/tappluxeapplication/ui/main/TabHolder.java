package tappluxe.b28.action.aws.com.tappluxeapplication.ui.main;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import tappluxe.b28.action.aws.com.tappluxeapplication.R;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.session.Session;

public class TabHolder extends Fragment {

	private ViewPager viewPager;
	private UnitFragment unitFragment;
	private ScheduleFragment scheduleFragment;
    public final static int UNIT_PAGE = 0;
    public final static int SCHEDULE_PAGE = 1;
    public static int pagerItem;
	Session session;

	public final static String GO_TO_FRAGMENT = "GO_TO_FRAGMENT";
    private String gotoFragment;

    public TabHolder() {
        // Required empty public constructor
    }

    public static TabHolder newInstance(String toFragment) {
        TabHolder fragment = new TabHolder();
        Bundle args = new Bundle();
        args.putString(GO_TO_FRAGMENT, toFragment);
        fragment.setArguments(args);
        return fragment;
    }

	@Override
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            gotoFragment = getArguments().getString(GO_TO_FRAGMENT);
        }
	}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_tab_holder, container, false);
        pagerItem = UNIT_PAGE;
		ButterKnife.bind(this, view);

		viewPager = (ViewPager) view.findViewById(R.id.viewpager);
		setupViewPager(viewPager);

		TabLayout tabLayout = (TabLayout) view.findViewById(R.id.tabs);
		tabLayout.setupWithViewPager(viewPager);

		LinearLayout linearLayout = (LinearLayout)tabLayout.getChildAt(0);
		linearLayout.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
		GradientDrawable drawable = new GradientDrawable();
		drawable.setColor(Color.WHITE);
		drawable.setSize(1, 1);
		linearLayout.setDividerPadding(10);
		linearLayout.setDividerDrawable(drawable);

		session = new Session(getActivity());
		goToTab(gotoFragment);
        return view;
	}

	public void goToTab(String fragment_dest){
		if (fragment_dest != null){
			switch (fragment_dest){
				case UnitFragment.GO_TO_UNIT_FRAGMENT: {
					viewPager.setCurrentItem(0);
					unitFragment.refresh();
				}break;
				case ScheduleFragment.GO_TO_SCHEDULE_FRAGMENT:{
					viewPager.setCurrentItem(1);
				}break;
			}
		}
	}

	public void refresh() {
        if (viewPager.getCurrentItem() == 0) {
            if (session.getBooleanValue(Session.FIRST_LOAD))
                session.setBooleanValue(Session.FIRST_LOAD, false);
            unitFragment.refresh();
        } else if (viewPager.getCurrentItem() == 1) {
            scheduleFragment.refresh();
        }
    }

	//Assigns the Fragments to the ViewPager(ViewPager acts as a view per fragment; loads 3 fragments at the same time.)
	private void setupViewPager(ViewPager viewPager) {
		ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(unitFragment = new UnitFragment(), getString(R.string.units));
        adapter.addFragment(scheduleFragment = new ScheduleFragment(), getString(R.string.schedules));
		viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                pagerItem = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

	}

	//Initializes the Adapter for the ViewPager
	private class ViewPagerAdapter extends FragmentPagerAdapter {
		private final List<Fragment> mFragmentList = new ArrayList<>();
		private final List<String> mFragmentTitleList = new ArrayList<>();

		private ViewPagerAdapter(FragmentManager manager) {
			super(manager);
		}

		@Override
		public Fragment getItem(int position) {
			return mFragmentList.get(position);
		}

		@Override
		public int getCount() {
			return mFragmentList.size();
		}

		private void addFragment(Fragment fragment, String title) {
			mFragmentList.add(fragment);
			mFragmentTitleList.add(title);
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return mFragmentTitleList.get(position);
		}
	}
}
