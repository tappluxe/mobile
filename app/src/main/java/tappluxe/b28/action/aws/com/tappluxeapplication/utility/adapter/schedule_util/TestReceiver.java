package tappluxe.b28.action.aws.com.tappluxeapplication.utility.adapter.schedule_util;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.util.Log;
import android.widget.Toast;

import java.util.Calendar;

/**
 * Created by franc.margallo on 3/13/2017.
 */

public class TestReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent)
	{
	}

	public void setAlarm(Context context)
	{
		AlarmManager am =( AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
		Intent i = new Intent(context, this.getClass());
		PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, 0);
		//am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 1000 * 60 * 10, pi); // Millisec * Second * Minute

		// Set the alarm to start at approximately 2:00 p.m.
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(System.currentTimeMillis());
		calendar.set(Calendar.HOUR_OF_DAY, 19);
		calendar.set(Calendar.MINUTE, 37);
		am.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pi);
	}

	public void cancelAlarm(Context context)
	{
		Intent intent = new Intent(context, this.getClass());
		PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);
		AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		alarmManager.cancel(sender);
	}
}
