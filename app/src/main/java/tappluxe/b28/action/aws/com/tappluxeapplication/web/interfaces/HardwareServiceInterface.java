package tappluxe.b28.action.aws.com.tappluxeapplication.web.interfaces;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Created by jay.bilocura on 3/21/2017.
 */

public interface HardwareServiceInterface {

    @Headers({"Accept:text/plain"})
    @POST("/")
    Call<ResponseBody> setUpSSID(@Body RequestBody body);
}
