package tappluxe.b28.action.aws.com.tappluxeapplication.ui.custom;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.provider.ContactsContract;
import android.support.design.widget.TextInputLayout;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import org.w3c.dom.Text;

import java.util.Locale;
import tappluxe.b28.action.aws.com.tappluxeapplication.R;
import tappluxe.b28.action.aws.com.tappluxeapplication.entities.Country;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.adapter.CountrySpinnerAdapter;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.adapter.country_util.CountryList;
import tappluxe.b28.action.aws.com.tappluxeapplication.validators.MobileNumberValidator;

/**
 * Created by jay.bilocura on 3/9/2017.
 */

public class MobileNumberInput extends RelativeLayout {

    public Spinner countrySpinner;
    public EditText etMobileNumber;
    public TextInputLayout tilMobileNumber;
    public TextView mobileFormatHint;

    private CountrySpinnerAdapter countrySpinnerAdapter;
    private CountryList countries;
    private Country currentCountry;

    private PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
    private PhoneNumberWatcher phoneNumberWatcher;

    public MobileNumberInput(Context context) {
        super(context);
        init(null);
    }

    public MobileNumberInput(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public void init(AttributeSet attrs) {
        inflate(getContext(), R.layout.mobile_input_layout, this);
        countrySpinner = (Spinner) findViewById(R.id.et_country);
        etMobileNumber = (EditText) findViewById(R.id.et_mobile_number);
        tilMobileNumber = (TextInputLayout) findViewById(R.id.til_mobile_number);
        mobileFormatHint = (TextView) findViewById(R.id.mobile_format_hint);

        phoneNumberWatcher = new PhoneNumberWatcher(Locale.getDefault().getCountry());
        countrySpinnerAdapter = new CountrySpinnerAdapter(getContext());
        countrySpinner.setAdapter(countrySpinnerAdapter);

        countries = new CountryList();
        countrySpinnerAdapter.addAll(countries.getCountries(getContext()));
        countrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                currentCountry = countrySpinnerAdapter.getItem(position);
                etMobileNumber.removeTextChangedListener(phoneNumberWatcher);
                phoneNumberWatcher = new PhoneNumberWatcher(currentCountry.getIsoCode());
                etMobileNumber.addTextChangedListener(phoneNumberWatcher);
                etMobileNumber.setText(etMobileNumber.getText().toString());
                etMobileNumber.setSelection(etMobileNumber.getText().toString().length());
                setHint();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        etMobileNumber.addTextChangedListener(phoneNumberWatcher);


        setEmptyCountryDefault();
    }

    public void setAsDefault(String iso) {
        int defaultIdx = countries.indexOfIso(iso);
        currentCountry = countries.getCountry(defaultIdx);
        countrySpinner.setSelection(defaultIdx);
    }

    public void setAsDefault(int countryCode) {
        currentCountry = countries.getCountryWithCode(countryCode);
        countrySpinner.setSelection(countryCode);
    }

    public void setEmptyCountryDefault() {
        String iso = Locale.getDefault().getCountry();
        setAsDefault(iso);
    }

    public void setText(String text) {
        etMobileNumber.setText(text);
    }


    public void setHint() {
        if (etMobileNumber != null && currentCountry != null && currentCountry.getIsoCode() != null) {
            Phonenumber.PhoneNumber phoneNumber = phoneUtil.getExampleNumberForType(currentCountry.getIsoCode(), PhoneNumberUtil.PhoneNumberType.MOBILE);
            if (phoneNumber != null) {
                mobileFormatHint.setText("Format: " + phoneUtil.format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.NATIONAL));
            }
        }
    }

    public String getNumber() {
        Phonenumber.PhoneNumber phoneNumber = getPhoneNumber();
        if (phoneNumber == null) {
            return null;
        }
        return phoneUtil.format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.E164);
    }

    public String getText() {
        return etMobileNumber.getText().toString();
    }

    private Phonenumber.PhoneNumber getPhoneNumber() {
        try {
            String iso = null;
            if (currentCountry != null) {
                iso = currentCountry.getIsoCode();
            }
            return phoneUtil.parse(etMobileNumber.getText().toString(), iso);
        } catch (NumberParseException ignored) {
            return null;
        }
    }

    public String getIso() {
        return currentCountry.getIsoCode();
    }

    public Integer getCountryCode() {
        return currentCountry.getDialCode();
    }

    private boolean isValid() {
        Phonenumber.PhoneNumber phoneNumber = getPhoneNumber();
        return phoneNumber != null && phoneUtil.isValidNumber(phoneNumber);
    }

    public boolean isValidMobileNumber() {
        String mobileNumber = getNumber();

        if (mobileNumber == null) {
            tilMobileNumber.setErrorEnabled(true);
            tilMobileNumber.setError(getContext().getText(R.string.empty_mobile_number_message));
            return false;
        } else if (!isValid()) {
            tilMobileNumber.setError(getContext().getText(R.string.invalid_mobile_number_message));
            return false;
        }
        return true;
    }

    private class PhoneNumberWatcher extends PhoneNumberFormattingTextWatcher {

        public PhoneNumberWatcher() {
            super();
        }

        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
        public PhoneNumberWatcher(String countryCode) {
            super(countryCode);
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            super.onTextChanged(s, start, before, count);
            tilMobileNumber.setError(null);
            tilMobileNumber.setErrorEnabled(false);
            try {
                String iso = null;
                if (currentCountry != null) {
                    iso = currentCountry.getIsoCode();
                }
                Phonenumber.PhoneNumber phoneNumber = phoneUtil.parse(s.toString(), iso);
                iso = phoneUtil.getRegionCodeForNumber(phoneNumber);
                if (iso != null) {
                    int countryIdx = countries.indexOfIso(iso);
                    countrySpinner.setSelection(countryIdx);
                }
            } catch (NumberParseException ignored) {
            }
        }
    }

}
