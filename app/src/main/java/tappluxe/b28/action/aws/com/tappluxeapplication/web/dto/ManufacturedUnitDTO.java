package tappluxe.b28.action.aws.com.tappluxeapplication.web.dto;


import java.io.Serializable;

/**
 * Created by jay.bilocura on 3/22/2017.
 */

public class ManufacturedUnitDTO implements Serializable {

    private Long productId;
    private String productKey;

    public ManufacturedUnitDTO(Long productId, String productKey) {}

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductKey() {
        return productKey;
    }

    public void setProductKey(String productKey) {
        this.productKey = productKey;
    }
}
