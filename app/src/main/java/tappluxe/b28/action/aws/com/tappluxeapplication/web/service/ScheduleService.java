package tappluxe.b28.action.aws.com.tappluxeapplication.web.service;

import android.content.Context;
import android.nfc.Tag;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.View;
import android.widget.Switch;

import java.io.IOException;
import java.util.List;

import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.GetScheduleInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.ListScheduleInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.SimpleCallerUserInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.ToggleScheduleStatusInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.session.Session;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.ScheduleDTO;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.interfaces.WebServiceInterface;

/**
 * Created by jude.dassun on 3/7/2017.
 */

public class ScheduleService extends BaseService {

	private static final String TAG = ScheduleService.class.getName();

	public ScheduleService(Context activity) {
		super(activity);
	}

	public Call addSchedule(String name, String time, String repeat,
							Integer timeBeforeNotification,
							final SimpleCallerUserInterface activity) {
		Call<ScheduleDTO> call = webServiceInterface.postAddSchedule(new ScheduleDTO(name, time,
						repeat, timeBeforeNotification),
				session.getValue(Session.ACCESS_TOKEN));

		call.enqueue(new Callback<ScheduleDTO>() {
			@Override
			public void onResponse(Call<ScheduleDTO> call, Response<ScheduleDTO> response) {
				if (response.isSuccessful()) {
					Log.d(TAG, "Successfully added a Schedule");
					activity.requestStatus(true, "Successfully added a Schedule");
				} else {
					try {
						String str = response.errorBody().string();
						Log.e(TAG, "error code: " + response.code() + " onResponse: " + str);
						activity.requestStatus(false, str);
					} catch (IOException e) {
						activity.requestStatus(false, e.getMessage());
						e.printStackTrace();
					}
				}
			}

			@Override
			public void onFailure(Call<ScheduleDTO> call, Throwable t) {
				Log.e(TAG, t.getClass().getName());
				if(t.getMessage() != null)
					Log.e(TAG, t.getMessage());
				t.printStackTrace();
				activity.requestStatus(false, "Cannot connect to the server.");
			}
		});
		return call;
	}

	public Call toggleSchedule(Long id, boolean isActive, final View switchCompat,
							   final ToggleScheduleStatusInterface activity) {
		Call<ScheduleDTO> call = webServiceInterface.putUpdateSchedule(new ScheduleDTO(id, isActive),
				id, session.getValue(Session.ACCESS_TOKEN));

		call.enqueue(new Callback<ScheduleDTO>() {
			@Override
			public void onResponse(Call<ScheduleDTO> call, Response<ScheduleDTO> response) {
				if (response.isSuccessful()) {
					Log.d(TAG, "Successfully updated Schedule");
					activity.toggleScheduleResult(true, "Successfully updated Schedule", switchCompat);
				} else {
					//get error message
					try {
						String str = response.errorBody().string();
						Log.e(TAG, "error code: " + response.code() + " onResponse: " + str);
						activity.toggleScheduleResult(false, str, switchCompat);
					} catch (IOException e) {
						activity.toggleScheduleResult(false, e.getMessage(), switchCompat);
						e.printStackTrace();
					}
				}
			}

			@Override
			public void onFailure(Call<ScheduleDTO> call, Throwable t) {
				Log.e(TAG, t.getClass().getName());
				if(t.getMessage() != null)
					Log.e(TAG, t.getMessage());
				t.printStackTrace();
				activity.toggleScheduleResult(false, "Cannot connect to the server.", switchCompat);
			}
		});
		return call;
	}

	public void updateSchedule(Long id, String name, String time, String repeat,
							   Integer timeBeforeNotification, final SimpleCallerUserInterface activity) {
		Call<ScheduleDTO> call = webServiceInterface.putUpdateSchedule(new ScheduleDTO(name, time,
						repeat, timeBeforeNotification), id,
				session.getValue(Session.ACCESS_TOKEN));

		Log.e("date send", " "+time);

		call.enqueue(new Callback<ScheduleDTO>() {
			@Override
			public void onResponse(Call<ScheduleDTO> call, Response<ScheduleDTO> response) {
				if (response.isSuccessful()) {
					Log.d(TAG, "Successfully updated Schedule");
					activity.requestStatus(true, "Successfully updated Schedule");
				} else {
					//get error message
					try {
						String str = response.errorBody().string();
						Log.e(TAG, "error code: " + response.code() + " onResponse: " + str);
						activity.requestStatus(false, str);
					} catch (IOException e) {
						activity.requestStatus(false, e.getMessage());
						e.printStackTrace();
					}
				}
			}

			@Override
			public void onFailure(Call<ScheduleDTO> call, Throwable t) {
				Log.e(TAG, t.getClass().getName());
				if(t.getMessage() != null)
					Log.e(TAG, t.getMessage());
				t.printStackTrace();
				activity.requestStatus(false, "Cannot connect to the server.");
			}
		});
	}


	public void deleteSchedule(Long id, final SimpleCallerUserInterface activity) {
		Call<ScheduleDTO> call = webServiceInterface.deleteSchedule(id, session.getValue(Session.ACCESS_TOKEN));

		call.enqueue(new Callback<ScheduleDTO>() {
			@Override
			public void onResponse(Call<ScheduleDTO> call, Response<ScheduleDTO> response) {
				if (response.isSuccessful()) {
					Log.d(TAG, "Successfully deleted a Schedule");
					activity.requestStatus(true, "Successfully deleted a Schedule");
				} else {
					//get error message
					try {
						String str = response.errorBody().string();
						Log.e(TAG, "error code: " + response.code() + " onResponse: " + str);
						activity.requestStatus(false, str);
					} catch (IOException e) {
						activity.requestStatus(false, e.getMessage());
						e.printStackTrace();
					}
				}
			}

			@Override
			public void onFailure(Call<ScheduleDTO> call, Throwable t) {
				Log.e(TAG, t.getClass().getName());
				if(t.getMessage() != null)
					Log.e(TAG, t.getMessage());
				t.printStackTrace();
				activity.requestStatus(false, "Cannot connect to the server.");
			}
		});
	}

	public Call getSpecificSchedule(Long scheduleID, final GetScheduleInterface activity) {
		Call<ScheduleDTO> call = webServiceInterface.getSpecificSchedule(scheduleID, session.getValue(Session.ACCESS_TOKEN));

		call.enqueue(new Callback<ScheduleDTO>() {
			@Override
			public void onResponse(Call<ScheduleDTO> call, Response<ScheduleDTO> response) {
				if (response.isSuccessful()) {
					Log.d(TAG, "Successfully got a specific Schedule");
					activity.setSchedule("", response.body());
				} else {
					//get error message
					try {
						String str = response.errorBody().string();
						Log.e(TAG, "error code: " + response.code() + " onResponse: " + str);
						activity.setError(str);
					} catch (IOException e) {
						activity.setError(e.getMessage());
						e.printStackTrace();
					}
				}
			}

			@Override
			public void onFailure(Call<ScheduleDTO> call, Throwable t) {
				Log.e(TAG, t.getClass().getName());
				if(t.getMessage() != null)
					Log.e(TAG, t.getMessage());
				t.printStackTrace();
				activity.setError("Cannot connect to the server.");
			}
		});
		return call;
	}

	public Call getScheduleList(final ListScheduleInterface activity) {
		Call<List<ScheduleDTO>> call = webServiceInterface.getSchedules(session.getValue(Session.ACCESS_TOKEN));

		call.enqueue(new Callback<List<ScheduleDTO>>() {
			@Override
			public void onResponse(Call<List<ScheduleDTO>> call, Response<List<ScheduleDTO>> response) {
				if (response.isSuccessful()) {
					Log.e(TAG, "Success List Schedule");
					activity.setListSchedule(response.body());
				} else {
					try {
						String str = response.errorBody().string();
						Log.e(TAG, "error code: " + response.code() + " onResponse: " + str);
						activity.getError(str);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

			@Override
			public void onFailure(Call<List<ScheduleDTO>> call, Throwable t) {
				Log.e(TAG, t.getClass().getName());
				if(t.getMessage() != null)
					Log.e(TAG, t.getMessage());
				t.printStackTrace();
				activity.getError("Cannot connect to the server.");
			}
		});
		return call;
	}

	public void assignToDevice(Long userUnitId, Long socketId, Long scheduleId, final SimpleCallerUserInterface activity){
		Call<ResponseBody> call = webServiceInterface.assignToSocket(userUnitId, socketId, scheduleId, session.getValue(Session.ACCESS_TOKEN));
		call.enqueue(new Callback<ResponseBody>() {
			@Override
			public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
				if (response.isSuccessful()) {
					Log.d(TAG, "Successfully inserted a Schedule");
					activity.requestStatus(true, "Successfully inserted a Schedule");
				} else {
					//get error message
					try {
						String str = response.errorBody().string();
						Log.e(TAG, "error code: " + response.code() + " onResponse: " + str);
						activity.requestStatus(false, str);
					} catch (IOException e) {
						activity.requestStatus(false, e.getMessage());
						e.printStackTrace();
					}
				}
			}

			@Override
			public void onFailure(Call<ResponseBody> call, Throwable t) {
				if(t.getMessage() != null)
					Log.e(TAG, t.getMessage());
				Log.e(TAG, t.getClass().getName());
				t.printStackTrace();
				if(activity != null){
					activity.requestStatus(false, "Cannot connect to the server.");
				}
			}
		});
	}
}
