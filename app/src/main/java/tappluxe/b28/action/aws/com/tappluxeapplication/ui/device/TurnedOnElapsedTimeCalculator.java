package tappluxe.b28.action.aws.com.tappluxeapplication.ui.device;

import java.util.Date;

/**
 * Created by benjo.uriarte on 3/22/2017.
 */

public class TurnedOnElapsedTimeCalculator {
	Date currentDate;
	Date turnedOnDate;

	public TurnedOnElapsedTimeCalculator(Date turnedOnDate) {
		setCurrentDate(new Date());
		this.currentDate = getCurrentDate();
		this.turnedOnDate = turnedOnDate;

		System.out.println("test TurnedOnElapsedTimeCalculator turn on: " + turnedOnDate);
		System.out.println("test TurnedOnElapsedTimeCalculator current on: " + currentDate);
	}

	public TurnedOnElapsedTimeCalculator(Date turnedOnDate, Date currentDate) {
		this.currentDate = currentDate;
		this.turnedOnDate = turnedOnDate;
	}

	public String getTurnedOnElapsedTime() {
		String turnedOnElapsedTime = new String();

		Date startDate = getTurnedOnDate();
		Date endDate = getCurrentDate();

		long difference = endDate.getTime() - startDate.getTime();

		if (difference >= 0) {
			turnedOnElapsedTime = "Turned on for about ";

			long secondsInMilli = 1000;
			long minutesInMilli = secondsInMilli * 60;
			long hoursInMilli = minutesInMilli * 60;
			long daysInMilli = hoursInMilli * 24;

			long elapsedDays = difference / daysInMilli;
			difference = difference % daysInMilli;

			long elapsedHours = difference / hoursInMilli;
			difference = difference % hoursInMilli;

			long elapsedMinutes = difference / minutesInMilli;
			difference = difference % minutesInMilli;

			if (elapsedDays == 0 && elapsedHours == 0 && elapsedMinutes == 0) {
				turnedOnElapsedTime = "Turned on for less than 1 minute";
				return turnedOnElapsedTime;
			}

			if (elapsedDays > 0) {
				turnedOnElapsedTime = turnedOnElapsedTime + String.valueOf(elapsedDays);

				if (elapsedDays > 1) {
					turnedOnElapsedTime = turnedOnElapsedTime + " days";
				} else {
					turnedOnElapsedTime = turnedOnElapsedTime + " day";
				}
			}

			if (elapsedHours > 0) {
				if (elapsedDays > 0) {
					turnedOnElapsedTime = turnedOnElapsedTime + ", ";
				}

				turnedOnElapsedTime = turnedOnElapsedTime + String.valueOf(elapsedHours);

				if (elapsedHours > 1) {
					turnedOnElapsedTime = turnedOnElapsedTime + " hours";
				} else {
					turnedOnElapsedTime = turnedOnElapsedTime + " hour";
				}
			}

			if (elapsedMinutes > 0) {
				if (elapsedDays > 0 || elapsedHours > 0) {
					turnedOnElapsedTime = turnedOnElapsedTime + ", ";
				}

				turnedOnElapsedTime = turnedOnElapsedTime + String.valueOf(elapsedMinutes);

				if (elapsedMinutes > 1) {
					turnedOnElapsedTime = turnedOnElapsedTime + " minutes";
				} else {
					turnedOnElapsedTime = turnedOnElapsedTime + " minute";
				}
			}

			return turnedOnElapsedTime;
		} else {
			return "Invalid input. Turned on date is still in the future.";
		}
	}

	public Date getCurrentDate() {
		return currentDate;
	}

	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}

	public Date getTurnedOnDate() {
		return turnedOnDate;
	}

	public void setTurnedOnDate(Date turnedOnDate) {
		this.turnedOnDate = turnedOnDate;
	}
}
