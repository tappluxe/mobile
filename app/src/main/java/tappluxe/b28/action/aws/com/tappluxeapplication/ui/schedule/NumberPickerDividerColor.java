package tappluxe.b28.action.aws.com.tappluxeapplication.ui.schedule;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.widget.NumberPicker;
import android.widget.TimePicker;

import java.lang.reflect.Field;

import tappluxe.b28.action.aws.com.tappluxeapplication.R;

/**
 * Created by ariel.fabilena on 3/13/2017.
 */

public class NumberPickerDividerColor {
	public static void applyStyle(TimePicker timePicker, Context context){
		Resources system = Resources.getSystem();
		int hourNumberPickerId = system.getIdentifier("hour", "id", "android");
		int minuteNumberPickerId = system.getIdentifier("minute", "id", "android");
		int ampmNumberPickerId = system.getIdentifier("amPm", "id", "android");

		NumberPicker hourNumberPicker = (NumberPicker) timePicker.findViewById(hourNumberPickerId);
		NumberPicker minuteNumberPicker = (NumberPicker) timePicker.findViewById(minuteNumberPickerId);
		NumberPicker ampmNumberPicker = (NumberPicker) timePicker.findViewById(ampmNumberPickerId);

		setNumberPickerDividerColour(hourNumberPicker, context);
		setNumberPickerDividerColour(minuteNumberPicker, context);
		setNumberPickerDividerColour(ampmNumberPicker, context);
	}

	public static void setNumberPickerDividerColour(NumberPicker number_picker, Context context){
		final int count = number_picker.getChildCount();

		for(int i = 0; i < count; i++){

			try{
				Field dividerField = number_picker.getClass().getDeclaredField("mSelectionDivider");
				dividerField.setAccessible(true);
				ColorDrawable colorDrawable = new ColorDrawable(context.getResources().getColor(R.color.colorAccent));
				dividerField.set(number_picker,colorDrawable);

				number_picker.invalidate();
			}
			catch(NoSuchFieldException e){
				Log.w("setNumberPickerTxtClr", e);
			}
			catch(IllegalAccessException e){
				Log.w("setNumberPickerTxtClr", e);
			}
			catch(IllegalArgumentException e){
				Log.w("setNumberPickerTxtClr", e);
			}
		}
	}
}
