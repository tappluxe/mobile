package tappluxe.b28.action.aws.com.tappluxeapplication.ui.main;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import tappluxe.b28.action.aws.com.tappluxeapplication.R;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.adapter.UnitAdapter;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.ListUnitInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.session.Session;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.UnitDTO;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.service.UnitService;


public class UnitFragment extends Fragment implements ListUnitInterface {
	public final static String GO_TO_UNIT_FRAGMENT = "GO_TO_UNITFRAGMENT";
	
	@BindView(R.id.lv_view_all_units)
	ListView lvViewUnits;

	private UnitService unitService;
	private Call call;
	Session session;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		session = new Session(getActivity());
		session.setBooleanValue(Session.FIRST_LOAD, true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View view = inflater.inflate(R.layout.main_home_fragment, container, false);
		ButterKnife.bind(this, view);

		unitService = new UnitService(this.getContext());
		refresh();
		return view;
	}

//	@Override
//	public void onResume() {
//		super.onResume();
//		refresh();
//	}

	public void refresh() {
		call = unitService.getUnitList(this);
	}

	@Override
	public void setListUnit(List<UnitDTO> unit) {
		UnitAdapter unitAdapter = new UnitAdapter(unit, this.getActivity());
		lvViewUnits.setAdapter(unitAdapter);
	}

//	@OnClick(R.id.fab_add_unit)
//	public void onAddUnit() {
//		startActivity(new Intent(this.getActivity(), AddUnit.class));
//	}

	@Override
	public void getError(String message) {
		if (this.getActivity() != null){
			try {
				JsonElement element = new JsonParser().parse(message);
				if (element == null) {
					Toast.makeText(this.getActivity(), message, Toast.LENGTH_LONG).show();
				} else {
					JsonObject obj = element.getAsJsonObject();
					if (obj == null) {
						Toast.makeText(this.getActivity(), message, Toast.LENGTH_LONG).show();
					} else {
						JsonElement generalMessage = obj.get("message");
						if(generalMessage != null && !generalMessage.getAsString().isEmpty()){
							Toast.makeText(this.getActivity(), generalMessage.getAsString(), Toast.LENGTH_LONG).show();
						}
						else
							Toast.makeText(this.getActivity(), message, Toast.LENGTH_LONG).show();
					}
				}
			} catch (JsonSyntaxException | IllegalStateException err) {
				Log.e(getClass().getName(), err.getMessage());
				Toast.makeText(this.getActivity(), message, Toast.LENGTH_LONG).show();
			}
		}
	}

	@Override
	public void onStop() {
		super.onStop();
		if (call != null)
			call.cancel();
	}
}
