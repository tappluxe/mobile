package tappluxe.b28.action.aws.com.tappluxeapplication.web.dto;

/**
 * Created by jude.dassun on 3/6/2017.
 */

public class RememberMeDTO {
	private String email, password;

	public RememberMeDTO(){
	}

	public RememberMeDTO(String email, String password) {
		this.email = email;
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
