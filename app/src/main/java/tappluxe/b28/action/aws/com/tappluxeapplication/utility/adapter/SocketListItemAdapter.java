package tappluxe.b28.action.aws.com.tappluxeapplication.utility.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import org.w3c.dom.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import tappluxe.b28.action.aws.com.tappluxeapplication.R;
import tappluxe.b28.action.aws.com.tappluxeapplication.ui.custom.TappluxeSwitchCompat;
import tappluxe.b28.action.aws.com.tappluxeapplication.ui.device.TurnedOnElapsedTimeCalculator;
import tappluxe.b28.action.aws.com.tappluxeapplication.ui.sockets.UpdateSocket;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.GetDeviceInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.GetStatusSocketInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.ListViewParentInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.TurnOffDeviceInteface;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.DeviceDTO;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.UnitDTO;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.UnitSocketDTO;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.service.DeviceService;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.service.DeviceStatusService;

/**
 * Created by ariel.fabilena on 2/22/2017.
 */

public class SocketListItemAdapter extends ArrayAdapter<UnitSocketDTO> implements GetDeviceInterface, TurnOffDeviceInteface, GetStatusSocketInterface {
	private Activity parentActivity;
	private final UnitDTO unit;
	private DeviceDTO device;
	private DeviceService deviceService;
	private DeviceStatusService deviceStatusService;
	private ListViewParentInterface listViewParentInterface;

	public SocketListItemAdapter(Context context, List<UnitSocketDTO> objects, Activity parentActivity,
								 UnitDTO unit, ListViewParentInterface listViewParentInterface) {
		super(context, 0, objects);
		this.parentActivity = parentActivity;
		this.unit = unit;
		deviceService = new DeviceService(parentActivity);
		deviceStatusService = new DeviceStatusService(parentActivity);
		this.listViewParentInterface = listViewParentInterface;
	}

	@NonNull
	@Override
	public View getView(final int position, View convertView, @NonNull final ViewGroup parent) {
		final UnitSocketDTO socket = getItem(position);
		if (convertView == null) {
			convertView = LayoutInflater.from(getContext()).inflate(R.layout.socket_list_item_adapter_layout,
					parent, false);
		}
		final String unitStatus = parentActivity.getIntent().getStringExtra("unitStatus");
		Log.d("UnitStatus", parentActivity.getIntent().getStringExtra("unitStatus"));

		TextView socketName = (TextView) convertView.findViewById(R.id.device_name_text_view);
		final TextView socketNumber = (TextView) convertView.findViewById(R.id.socket_number_text_view);
		final TappluxeSwitchCompat switchSocket = (TappluxeSwitchCompat) convertView.findViewById(R.id.socket_switch);
		LinearLayout deviceHolder = (LinearLayout) convertView.findViewById(R.id.device_info_holder);
		final TextView turnedOnElapsedTime = (TextView) convertView.findViewById(R.id.socket_turned_on_elapsed_time_text_view);
		final ProgressBar switchSocketProgressBar = (ProgressBar) convertView.findViewById(R.id.device_switch_progress_bar);

		switchSocketProgressBar.setVisibility(View.GONE);
		socketNumber.setText(parentActivity.getString(R.string.socket) + " " + (position + 1));

		if(unitStatus.equals("offline")){
			switchSocket.setEnabled(false);
		}

		final boolean state = socket.getStatuses().get(0).isState();
		switchSocket.setCheckedFromCode(state);

		getSocketStatePeriodically(unit.getId(), socket.getId(), socket, switchSocket, turnedOnElapsedTime, deviceStatusService, this);

		switchSocket.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, final boolean isChecked) {
				if (!switchSocket.isFromCode()) {
					switchSocket.setEnabled(false);
					switchSocket.setCheckedFromCode(!isChecked);

					AlertDialog.Builder dialog = new AlertDialog.Builder(parentActivity);
					dialog.setCancelable(true);

					if (isChecked) {
						dialog.setTitle(R.string.confirm_turn_on_socket_title);
						dialog.setMessage(parentActivity.getString(R.string.confirm_turn_on_socket_message)
								+ "\n" + parentActivity.getString(R.string.socket) + " " + (position + 1) + "?");
					} else {
						dialog.setTitle(R.string.confirm_turn_off_socket_title);
						dialog.setMessage(parentActivity.getString(R.string.confirm_turn_off_socket_message)
								+ "\n" + parentActivity.getString(R.string.socket) + " " + (position + 1) + "?");
					}

					dialog.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							switchSocket.setVisibility(View.GONE);
							switchSocketProgressBar.setVisibility(View.VISIBLE);

							deviceStatusService.turnOffDevice(unit.getId(), socket.getId(), isChecked, switchSocket, switchSocketProgressBar, socket, turnedOnElapsedTime, SocketListItemAdapter.this);


						}
					});
					dialog.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.cancel();
						}
					});
					dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
						@Override
						public void onCancel(DialogInterface dialog) {
							switchSocket.setEnabled(true);
							switchSocket.setVisibility(View.VISIBLE);
							switchSocketProgressBar.setVisibility(View.GONE);
						}
					});
					dialog.create().show();
				}
			}
		});

//		switchSocket.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				v.setEnabled(false);
//				v.setVisibility(View.GONE);
//				switchSocketProgressBar.setVisibility(View.VISIBLE);
//				switchSocket.setChecked(!switchSocket.isChecked());
//				deviceStatusService.turnOffDevice(unit.getId(), socket.getId(),
//						!switchSocket.isChecked(), v, switchSocketProgressBar,
//						SocketListItemAdapter.this);
//			}
//		});


		deviceHolder.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(parentActivity, UpdateSocket.class);
				intent.putExtra("unit", unit);
				intent.putExtra("socket", socket);
				parentActivity.startActivity(intent);
			}
		});

		if (socket.getDeviceId() != null) {
			deviceService.getDevice(socket.getDeviceId(), this, socketName);
		} else {
			socketName.setText(R.string.no_device_attached);
		}

		numberNotification(state, socket, turnedOnElapsedTime);

		return convertView;
	}

	@Override
	public void setDevice(String message, DeviceDTO device, View socketView) {
		if (socketView != null) {
			this.device = device;
			TextView textView = (TextView) socketView;
			textView.setText(device.getName());
		}
	}

	@Override
	public void setError(String message) {
		if (parentActivity != null) {
			Toast.makeText(parentActivity, message, Toast.LENGTH_LONG).show();
		}
	}

	@Override
	public void turnOffDeviceResult(View socketSwitch, View progressBar,  final UnitSocketDTO socket, final TextView turnedOnElapsedTime) {
		TappluxeSwitchCompat sw = (TappluxeSwitchCompat) socketSwitch;
//		numberNotification(!sw.isChecked(), socket, turnedOnElapsedTime);
		if (parentActivity != null) {
			sw.setCheckedFromCode(!sw.isChecked());
			sw.setEnabled(true);
			sw.setVisibility(View.VISIBLE);
			progressBar.setVisibility(View.GONE);
			listViewParentInterface.refresh();
		}
	}

	@Override
	public void setError(String message, View socketSwitch, View progressBar) {
		if (parentActivity != null) {
			try {
				JsonElement element = new JsonParser().parse(message);
				if (element == null) {
					Toast.makeText(parentActivity, message, Toast.LENGTH_LONG).show();
				} else {
					JsonObject obj = element.getAsJsonObject();
					if (obj == null) {
						Toast.makeText(parentActivity, message, Toast.LENGTH_LONG).show();
					} else {
						JsonElement generalMessage = obj.get("message");
						if (generalMessage != null && !generalMessage.getAsString().isEmpty()) {
							Toast.makeText(parentActivity, generalMessage.getAsString(),
									Toast.LENGTH_LONG).show();
						} else
							Toast.makeText(parentActivity, message, Toast.LENGTH_LONG).show();
					}
				}
			} catch (JsonSyntaxException | IllegalStateException err) {
				Log.e(getClass().getName(), err.getMessage());
				Toast.makeText(parentActivity, message, Toast.LENGTH_LONG).show();
			}
			socketSwitch.setEnabled(true);
			socketSwitch.setVisibility(View.VISIBLE);
			progressBar.setVisibility(View.GONE);

//			listViewParentInterface.refresh();
		}
	}

	public void getSocketStatePeriodically(final Long unitId, final Long socketId, final UnitSocketDTO socket, final TappluxeSwitchCompat switchSocket, final TextView turnOnElapsedTime, final DeviceStatusService deviceStatusService, final GetStatusSocketInterface activity) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				while(true){
					try{
						Log.e("Test","test");
						deviceStatusService.requestSocketState(unitId, socketId, socket, switchSocket, turnOnElapsedTime, activity);
						Thread.sleep(30000);
					}catch(Exception e){
					}
				}
			}
		}).start();
	}

	@Override
	public void requestSocketStatus(Boolean state, final UnitSocketDTO socket, TextView turnedOnElapsedTime, TappluxeSwitchCompat switchCompat) {
//		state = socket.getStatuses().get(0).isState();
		switchCompat.setCheckedFromCode(state);
		numberNotification(state, socket, turnedOnElapsedTime);
	}

	@Override
	public void socketError(String message){
		Log.e("Test", message);
	}

	public void numberNotification(boolean state, final UnitSocketDTO socket, final TextView turnedOnElapsedTime){
		if (state) {
			String turnedOnString = socket.getStatuses().get(0).getUpdateDate();
			SimpleDateFormat simpleDateFormat =
					new SimpleDateFormat(parentActivity.getString(R.string.time_format_small_h));
			try {
				Date turnedOnDate = simpleDateFormat.parse(turnedOnString);
				TurnedOnElapsedTimeCalculator turnedOnElapsedTimeCalculator =
						new TurnedOnElapsedTimeCalculator(turnedOnDate);

				turnedOnElapsedTime.setText(turnedOnElapsedTimeCalculator.getTurnedOnElapsedTime());

			} catch (ParseException e) {
				e.printStackTrace();
			}
		} else {
			turnedOnElapsedTime.setText(R.string.turned_off);
		}
	}
}
