package tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces;

/**
 * Created by jude.dassun on 2/21/2017.
 */

public interface FormInterface {
	void clearForm();
	void enableForm(boolean enabled);
}
