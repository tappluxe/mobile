package tappluxe.b28.action.aws.com.tappluxeapplication.ui.unit;

import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import tappluxe.b28.action.aws.com.tappluxeapplication.BaseActivity;
import tappluxe.b28.action.aws.com.tappluxeapplication.R;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.SimpleCallerUserInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.session.Session;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.UnitDTO;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.service.UnitService;

public class UpdateUnit extends BaseActivity implements SimpleCallerUserInterface {
	@BindView(R.id.et_update_unitname)
	EditText etUnitName;

	@BindView(R.id.til_et_edit_unitname)
	TextInputLayout tilUnitName;

	private UnitDTO unit;
	private Session session;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.unit_update_activity);
		ButterKnife.bind(this);
		session = new Session(this);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);

		etUnitName.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				tilUnitName.setError(null);
			}
			@Override
			public void afterTextChanged(Editable s) {}
		});
		Intent intent = getIntent();
		unit = (UnitDTO) intent.getSerializableExtra("unit");
		if(unit != null){
			etUnitName.setText(unit.getName());
		}
		else
			finish();
	}

	@OnClick(R.id.btn_update_accept)
	public void onBtnAccept(){
		UnitService unitService = new UnitService(this);
		String name = etUnitName.getText().toString().trim();
		if(name.isEmpty()) {
			tilUnitName.setError("Unit Name must not be empty.");
		}
		else
			unitService.updateUnit(unit.getId(), name, this);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				session.removeKey(Session.UNIT_NAME);
				this.finish();
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

//	@OnClick(R.id.btn_update_cancel)
//	public void onBtnCancel(){
//		session.removeKey(Session.UNIT_NAME);
//		finish();
//	}

	@Override
	public void requestStatus(boolean success, String message) {
		if(success){
			Toast.makeText(this, message, Toast.LENGTH_LONG).show();
			session.setValue(Session.UNIT_NAME, etUnitName.getText().toString());
			finish();
		}
		else{
			try {
				JsonElement element = new JsonParser().parse(message);
				if (element == null) {
					Toast.makeText(this, message, Toast.LENGTH_LONG).show();
				} else {
					JsonObject obj = element.getAsJsonObject();
					if (obj == null) {
						Toast.makeText(this, message, Toast.LENGTH_LONG).show();
					} else {
						boolean hasnormalerror = false;
						JsonElement name = obj.get("name");
						JsonElement generalMessage = obj.get("message");
						if (name != null && !name.getAsString().isEmpty()) {
							tilUnitName.setError(name.getAsString());
							hasnormalerror = true;
						}
						if(generalMessage != null && !generalMessage.getAsString().isEmpty()){
							Toast.makeText(this, generalMessage.getAsString(), Toast.LENGTH_LONG).show();
							hasnormalerror = true;
						}

						if(!hasnormalerror){
							Toast.makeText(this, message, Toast.LENGTH_LONG).show();
						}
					}
				}
			} catch (JsonSyntaxException | IllegalStateException err) {
				Toast.makeText(this, message, Toast.LENGTH_LONG).show();
			}
		}
	}
}
