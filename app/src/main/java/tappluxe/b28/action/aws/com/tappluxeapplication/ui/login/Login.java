package tappluxe.b28.action.aws.com.tappluxeapplication.ui.login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import butterknife.Optional;
import tappluxe.b28.action.aws.com.tappluxeapplication.BaseActivity;
import tappluxe.b28.action.aws.com.tappluxeapplication.MainActivity;
import tappluxe.b28.action.aws.com.tappluxeapplication.R;
import tappluxe.b28.action.aws.com.tappluxeapplication.ui.forgot_password.ForgotPasswordActivity;
import tappluxe.b28.action.aws.com.tappluxeapplication.ui.main.TabHolder;
import tappluxe.b28.action.aws.com.tappluxeapplication.ui.registration.RegistrationActivity;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.SimpleCallerUserInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.session.Session;

public class Login extends BaseActivity implements SimpleCallerUserInterface {
	private Session session;

	public static final String LOGIN = "LOGIN_ACTIVITY", EMAIL = "EMAIL", PASSWORD = "PASSWORD";

	@BindView(R.id.etUsername)
	public EditText etUsername;
	@BindView(R.id.etPassword)
	public EditText etPassword;
	@BindView(R.id.tilUsername)
	public TextInputLayout tilUsername;
	@BindView(R.id.tilPassword)
	public TextInputLayout tilPassword;
	@BindView(R.id.ll_internet_options)
	public LinearLayout ll_internet_options;

	@BindView(R.id.tv_empty_field_error)
	public TextView tvEmptyField;

	@BindView(R.id.rememberMe)
	public CheckBox toRemember;
	@BindView(R.id.keepMeLoggedIn)
	public CheckBox keepMeLoggedIn;


	@BindView(R.id.btnLogin)
	public Button signIn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_login);
		ButterKnife.bind(this);
		getSupportActionBar().hide();
		session = new Session(this);
		boolean remember = session.getBooleanValue(Session.REMEMBERME);
		toRemember.setChecked(remember);

		Intent intent = getIntent();
		boolean fromlogin = intent.getBooleanExtra(LOGIN, false);
		String username = null, password = null, message = intent.getStringExtra(SplashScreen.MESSAGE);
		if (fromlogin) {
			username = intent.getStringExtra(EMAIL);
			password = intent.getStringExtra(PASSWORD);
		} else if (remember) {
			username = session.getValue(Session.USER_EMAIL);
			password = session.getValue(Session.USER_PASSWORD);
		}

		if (username != null && password != null) {
			etUsername.setText(username);
			etPassword.setText(password);
		}
		if (message != null) {
			tvEmptyField.setText(message);
		}

		keepMeLoggedIn.setChecked(session.getBooleanValue(Session.LOG_STATE));
	}

	@OnClick(R.id.tv_login_options)
	public void onShowLoginOptions() {
		if (ll_internet_options.getVisibility() == View.GONE) {
			ll_internet_options.setVisibility(View.VISIBLE);
		} else {
			ll_internet_options.setVisibility(View.GONE);
		}
	}

	@OnClick(R.id.rememberMe)
	public void onRememberMeClick() {
		String email = etUsername.getText().toString().trim();
		String password = etPassword.getText().toString();
		session.setBooleanValue(Session.REMEMBERME, toRemember.isChecked());
		if (toRemember.isChecked()) {
			if (email.isEmpty() && password.isEmpty()) {
				session.removeKey(Session.REMEMBERME);
			} else {
				session.setValue(session.USER_EMAIL, email);
				session.setValue(session.USER_PASSWORD, password);
			}
		} else {
			session.removeKey(session.USER_EMAIL);
			session.removeKey(session.USER_PASSWORD);
		}
	}

	@OnClick(R.id.btnSignUp)
	public void onClick() {
		startActivity(new Intent(this, RegistrationActivity.class));
	}

	@OnClick(R.id.btnLogin)
	public void login() {
		String email = etUsername.getText().toString().trim();
		String password = etPassword.getText().toString();
		if (email.isEmpty() || password.isEmpty()) {
			tvEmptyField.setText(R.string.login_empty_field);
		} else {
			onRememberMeClick();
			if (keepMeLoggedIn.isChecked()) {
				session.setBooleanValue(session.LOG_STATE, keepMeLoggedIn.isChecked());
			} else {
				session.removeKey(session.LOG_STATE);
			}

			Intent intent = new Intent(this, SplashScreen.class);
			intent.putExtra(LOGIN, true);
			intent.putExtra(EMAIL, email);
			intent.putExtra(PASSWORD, password);
			startActivity(intent);
			finish();
		}
	}

	@Optional
	@OnTextChanged({R.id.etUsername, R.id.etPassword})
	public void fieldChanged() {
		tvEmptyField.setText("");
	}


	@Override
	public void requestStatus(boolean success, String message) {
		if (success) {
			Intent intent = new Intent(this, TabHolder.class);
			session.setBooleanValue(Session.LOG_STATE, keepMeLoggedIn.isChecked());
			startActivity(intent);
			finish();
		} else {
			Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
		}
	}

//	public void loginEnable(boolean control) {
//		signIn.setEnabled(control);
//		signIn.setClickable(control);
//		if (control) {
//			signIn.setBackgroundResource(R.drawable.round_button);
//		} else {
//			signIn.setBackgroundResource(R.drawable.disabled_button);
//		}
//	}

//	@OnClick(R.id.imageView)
//	public void testAccount() {
//		startActivity(new Intent(this, TabHolder.class));
//		finish();
//	}

	@OnClick(R.id.forgotPassword)
	public void forgotPassword() {
		Intent intent = new Intent(this, ForgotPasswordActivity.class);
		startActivity(intent);
	}

}
