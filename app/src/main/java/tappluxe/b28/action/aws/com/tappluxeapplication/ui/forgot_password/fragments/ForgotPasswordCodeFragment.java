package tappluxe.b28.action.aws.com.tappluxeapplication.ui.forgot_password.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import tappluxe.b28.action.aws.com.tappluxeapplication.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ForgotPasswordCodeFragment.OnCodeFragmentListener interface
 * to handle interaction events.
 * Use the {@link ForgotPasswordCodeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ForgotPasswordCodeFragment extends Fragment {
    private static final String USER_EMAIL = "user_email";
    private String email;
    private OnCodeFragmentListener mListener;

    @BindView(R.id.et_pass_code) public EditText etPassCode;
    @BindView(R.id.til_pass_code) public TextInputLayout tilPassCode;
    @BindView(R.id.btn_submit_pass_code) public Button btnSubmitCode;


    public ForgotPasswordCodeFragment() {
        // Required empty public constructor
    }

    public static ForgotPasswordCodeFragment newInstance(String email) {
        ForgotPasswordCodeFragment fragment = new ForgotPasswordCodeFragment();
        Bundle args = new Bundle();
        args.putString(USER_EMAIL, email);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            email = getArguments().getString(USER_EMAIL);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.forgot_password_code_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            mListener = (OnCodeFragmentListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnSubmitPassCodeListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @OnClick(R.id.btn_submit_pass_code)
    public void submitPassCode() {
        String passCode = etPassCode.getText().toString();
        mListener.onSubmitPassCode(passCode, email);
    }

    @OnClick(R.id.tv_resend_pass_code)
    public void resendCode() {
        mListener.onResendPassCode(email);
    }


    @OnTextChanged(R.id.et_pass_code)
    public void emailTextChange() {
        tilPassCode.setErrorEnabled(false);
    }

    public void setPassCodeError(String error) {
        tilPassCode.setErrorEnabled(true);
        tilPassCode.setError(error);
    }

    public interface OnCodeFragmentListener {
        void onSubmitPassCode(String passcode, String email);
        void onResendPassCode(String email);
    }

}
