package tappluxe.b28.action.aws.com.tappluxeapplication.web.service;

import android.content.Context;
import android.util.Log;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.SimpleCallerUserInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.UserDetailsInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.session.Session;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.ResetPasswordDTO;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.UnitDTO;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.UserDTO;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.interfaces.WebServiceInterface;

/**
 * Created by ariel.fabilena on 2/21/2017.
 */

public class UserService extends BaseService {
	private static final String TAG = UserService.class.getName();

	public UserService(Context activity) {
		super(activity);
	}

	public void signUpPost(String prodkey, String unitName, String email, String password, String confirmPassword,
                           String firstName, String lastName, String phone,
                           final SimpleCallerUserInterface activity) {

		Call<UserDTO> call = webServiceInterface.postSignUp(new UserDTO(email, password,
				confirmPassword,  firstName, lastName, phone, new UnitDTO(prodkey, unitName), false));

		call.enqueue(new Callback<UserDTO>() {
			@Override
			public void onResponse(Call<UserDTO> call, Response<UserDTO> response) {
				if (response.isSuccessful()) {
					Log.d(TAG, "Success.");
					activity.requestStatus(true, "Registered Successfully.");
				} else {
					//get error message
					try {
						String str = response.errorBody().string();
						Log.e(TAG, "error code: " + response.code()+ " onResponse: " + str);
						activity.requestStatus(false, str);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			@Override
			public void onFailure(Call<UserDTO> call, Throwable t) {
				Log.e(TAG, t.getClass().getName());
				if(t.getMessage() != null)
					Log.e(TAG, t.getMessage());
				t.printStackTrace();
				activity.requestStatus(false, "Cannot connect to the server.");
			}
		});
	}

	public void loginPost(final SimpleCallerUserInterface activity, String email, String password) {
		Retrofit.Builder builder = new Retrofit.Builder().baseUrl(AUTH_URL).addConverterFactory(GsonConverterFactory.create());
		Retrofit retrofit = builder.client(httpClientBuilder.build()).build();
		webServiceInterface = retrofit.create(WebServiceInterface.class);
		Call<ResponseBody> call = webServiceInterface.postLogInJson(email, password);
		call.enqueue(new Callback<ResponseBody>() {
			@Override
			public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

				Log.e(TAG, "run during the response");
				if (response.isSuccessful()) {
					try {
						String str = response.body().string();
						Log.d("Auth", str);
						JsonObject obj = new JsonParser().parse(str).getAsJsonObject();
//						editor.putString(JSON_TOKEN, obj.getAsJsonPrimitive("access_token").getAsString());
//						editor.apply();

						session.setValue(Session.ACCESS_TOKEN, obj.getAsJsonPrimitive("access_token").getAsString());
						session.setValue(Session.REFRESH_TOKEN, obj.getAsJsonPrimitive("refresh_token").getAsString());
						Log.e(TAG, session.getValue(Session.ACCESS_TOKEN));
						Log.e(TAG, session.getValue(Session.REFRESH_TOKEN));
						activity.requestStatus(true, null);
					} catch (IOException e) {
						e.printStackTrace();
					}
				} else {
					try {
						String str = response.errorBody().string();
						Log.e(TAG, "error code: " + response.code()+ " onResponse: " + str);
						activity.requestStatus(false, "Invalid Email or Password");
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			@Override
			public void onFailure(Call<ResponseBody> call, Throwable t) {
				Log.e(TAG, t.getClass().getName());
				if(t.getMessage() != null)
					Log.e(TAG, t.getMessage());
				t.printStackTrace();
				activity.requestStatus(false, "Cannot connect to the server");
			}
		});
	}

	public void refreshTokenPost(final SimpleCallerUserInterface activity, String refreshToken){
		Retrofit.Builder builder = new Retrofit.Builder().baseUrl(AUTH_URL).addConverterFactory(GsonConverterFactory.create());
		Retrofit retrofit = builder.client(httpClientBuilder.build()).build();
		webServiceInterface = retrofit.create(WebServiceInterface.class);
		Call<ResponseBody> call = webServiceInterface.postRefreshToken(refreshToken);
		call.enqueue(new Callback<ResponseBody>() {
			@Override
			public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
				if (response.isSuccessful()) {
					String str = null;
					try {
						str = response.body().string();
						JsonObject obj = new JsonParser().parse(str).getAsJsonObject();
						session.removeKey(Session.REFRESH_TOKEN);
						session.removeKey(Session.ACCESS_TOKEN);
						session.setValue(Session.ACCESS_TOKEN, obj.getAsJsonPrimitive("access_token").getAsString());
						session.setValue(Session.REFRESH_TOKEN, obj.getAsJsonPrimitive("refresh_token").getAsString());
						Log.e(TAG, session.getValue(Session.ACCESS_TOKEN));
						Log.e(TAG, session.getValue(Session.REFRESH_TOKEN));
						activity.requestStatus(true, "Success");
						Log.e(TAG, " onResponse: " + str);
					} catch (IOException e) {
						e.printStackTrace();
					}
				} else {
					try {
						String str = response.errorBody().string();
						Log.e(TAG, "error code: " + response.code()+ " onResponse: " + str);
						activity.requestStatus(false, "Invalid Email or Password");
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			@Override
			public void onFailure(Call<ResponseBody> call, Throwable t) {
				Log.e(TAG, t.getClass().getName());
				if(t.getMessage() != null)
					Log.e(TAG, t.getMessage());
				t.printStackTrace();
				activity.requestStatus(false, "Cannot connect to the server");
			}
		});
	}

	public void userDetails(final UserDetailsInterface activity) {
        Retrofit.Builder builder = new Retrofit.Builder().baseUrl(SERVER_URL).addConverterFactory(GsonConverterFactory.create());
        Retrofit retrofit = builder.client(httpClientBuilder.build()).build();
        webServiceInterface = retrofit.create(WebServiceInterface.class);
        Call<UserDTO> call = webServiceInterface.userDetails(session.getValue(Session.ACCESS_TOKEN));
        call.enqueue(new Callback<UserDTO>() {
            @Override
            public void onResponse(Call<UserDTO> call, Response<UserDTO> response) {
                if (response.isSuccessful()) {
                    activity.userDetails(response.body());
                } else{
                    Log.e("user", response.message());
                }
            }

            @Override
            public void onFailure(Call<UserDTO> call, Throwable t) {
                Log.e("user", t.getLocalizedMessage());
            }
        });
    }

    public void updateUser(UserDTO user, final SimpleCallerUserInterface activity) {
        Retrofit.Builder builder = new Retrofit.Builder().baseUrl(SERVER_URL).addConverterFactory(GsonConverterFactory.create());
        Retrofit retrofit = builder.client(httpClientBuilder.build()).build();
        webServiceInterface = retrofit.create(WebServiceInterface.class);
        Call<UserDTO> call = webServiceInterface.updateUser(user, session.getValue(Session.ACCESS_TOKEN));
        call.enqueue(new Callback<UserDTO>() {
            @Override
            public void onResponse(Call<UserDTO> call, Response<UserDTO> response) {
                if (response.isSuccessful()) {
                    Log.d(TAG, "Success");
                    activity.requestStatus(true, "User successfully updated.");
                } else {
                    //get error message
                    try {
                        String str = response.errorBody().string();
                        JsonObject obj = new JsonParser().parse(str).getAsJsonObject();
                        Log.e(TAG, "error code: " + response.code()+ " onResponse: " + str);
                        activity.requestStatus(false, str);
                    } catch (IOException e) {
                        activity.requestStatus(false, e.getMessage());
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<UserDTO> call, Throwable t) {
                Log.e(TAG, t.getClass().getName());
                if(t.getMessage() != null)
                    Log.e(TAG, t.getMessage());
                t.printStackTrace();
            }
        });
    }


	public void checkEmail(String email, final SimpleCallerUserInterface activity) {
        Retrofit.Builder builder = new Retrofit.Builder().baseUrl(SERVER_URL).addConverterFactory(GsonConverterFactory.create());
        Retrofit retrofit = builder.client(httpClientBuilder.build()).build();
        webServiceInterface = retrofit.create(WebServiceInterface.class);
        Call<UserDTO> call = webServiceInterface.checkEmail(email);
        call.enqueue(new Callback<UserDTO>() {
            @Override
            public void onResponse(Call<UserDTO> call, Response<UserDTO> response) {
                if (response.isSuccessful()) {
                    Log.d(TAG, "Success");
                    activity.requestStatus(true, "Email is valid.");
                } else {
                    //get error message
                    try {
	                    String str = response.errorBody().string();
						JsonObject obj = new JsonParser().parse(str).getAsJsonObject();
	                    Log.e(TAG, "error code: " + response.code()+ " onResponse: " + str);
						activity.requestStatus(false, obj.get("message").getAsString());
                    } catch (IOException e) {
                        activity.requestStatus(false, e.getMessage());
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<UserDTO> call, Throwable t) {
	            Log.e(TAG, t.getClass().getName());
	            if(t.getMessage() != null)
		            Log.e(TAG, t.getMessage());
	            t.printStackTrace();
                activity.requestStatus(false, "Cannot connect to the server.");
            }
        });
	}

	public void forgotPassword(String email, Callback<ResponseBody> callback) {
        Call<ResponseBody> call = webServiceInterface.forgotPassword(email);
        call.enqueue(callback);
    }

    public void resetPassword(String email, String newPass, String confirmPass, Callback<ResponseBody> callback) {
        Call<ResponseBody> call = webServiceInterface.resetPassword(new ResetPasswordDTO(email, newPass, confirmPass));
        call.enqueue(callback);
    }

}
