package tappluxe.b28.action.aws.com.tappluxeapplication.utility.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import tappluxe.b28.action.aws.com.tappluxeapplication.R;
import tappluxe.b28.action.aws.com.tappluxeapplication.ui.unit.ViewUnit;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.GetUnitStatusInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.session.DatabaseHandler;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.session.Session;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.session.UnitStatusLedSQL;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.UnitDTO;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.service.UnitService;

/**
 * Created by ariel.fabilena on 2/22/2017.
 */

public class UnitAdapter extends ArrayAdapter<UnitDTO> implements GetUnitStatusInterface {
	private Activity parentActivity;
	UnitService unitService;
	Session session;

	public UnitAdapter(List<UnitDTO> objects, Activity parentActivity) {
		super(parentActivity, 0, objects);
		this.parentActivity = parentActivity;
	}

	@NonNull
	@Override
	public View getView(int position, @Nullable View convertView, @NonNull final ViewGroup parent) {
		final UnitDTO unit = getItem(position);
		unitService = new UnitService(this.getContext());

		if (convertView == null) {
			convertView = LayoutInflater.from(getContext()).inflate(R.layout.unit_adapter_list_layout, parent, false);
		}

		int devicesSize = 0;
		for(int i = 0; i < unit.getSockets().size(); i++){
			if(unit.getSockets().get(i).getDeviceId() != null){
				devicesSize++;
			}
		}

		LinearLayout viewUnit = (LinearLayout) convertView.findViewById(R.id.layout_view_all_unit);
		TextView unitName = (TextView) convertView.findViewById(R.id.unit_list_item_name);
		TextView unitSocket = (TextView) convertView.findViewById(R.id.unit_list_item_sockets);
		TextView unitDevices = (TextView) convertView.findViewById(R.id.unit_list_item_devices);
		final ProgressBar progressBar = (ProgressBar) convertView.findViewById(R.id.progress_led);
		final Button btnLed = (Button) convertView.findViewById(R.id.btn_led);

		getStatusPeriodically(unit, this, progressBar, btnLed);

		progressBar.setVisibility(View.VISIBLE);
		btnLed.setVisibility(View.GONE);

		unitName.setText(unit.getName());
		unitSocket.setText(unit.getSockets().size() + " total sockets");
		if (devicesSize == 0) {
			unitDevices.setText("No devices connected");
		} else if (devicesSize == 1) {
			unitDevices.setText("Only 1 device connected");
		} else {
			unitDevices.setText(devicesSize + " devices connected");
		}
		//final UnitService service = new UnitService(getContext());

		viewUnit.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(parentActivity, ViewUnit.class);
				intent.putExtra("unit", unit);
				if(btnLed.getText().toString().equals("ONLINE")){
					intent.putExtra("unitStatus", "online");
				}else{
					intent.putExtra("unitStatus", "offline");
				}
				parentActivity.startActivity(intent);
			}
		});

//		updateUnit.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				Intent intent = new Intent(parentActivity, UpdateUnit.class);
//				intent.putExtra("unit", unit);
//				parentActivity.startActivity(intent);
//			}
//		});

//		deleteUnit.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				AlertDialog.Builder dialog = new AlertDialog.Builder(parentActivity);
//				dialog.setTitle(R.string.delete_unit_title);
//
//				if (unit.getSockets().size() < 1) {
//					dialog.setMessage(R.string.delete_unit_message);
//				} else {
//					dialog.setMessage(R.string.delete_unit_message_devices);
//				}
//
//				dialog.setCancelable(true);
//
//				dialog.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
//					@Override
//					public void onClick(DialogInterface dialog, int which) {
//						service.deleteUnit(unit.getId(), callerUserInterface);
//					}
//				});
//
//				dialog.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
//					@Override
//					public void onClick(DialogInterface dialog, int which) {
//						dialog.cancel();
//					}
//				});
//
//				dialog.create().show();
//			}
//		});

		return convertView;
	}

	public void getStatusPeriodically(final UnitDTO unit, final GetUnitStatusInterface activity, final ProgressBar progressBar, final Button btnLed) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				while(true){
					try{
						Log.e("Test","test");
						unitService.getSocketStatus(unit.getId(), unit, activity, progressBar, btnLed);
						Thread.sleep(30000);
					}catch(Exception e){
					}
				}
			}
		}).start();
	}

	@Override
	public void requestUnitStatus(boolean success, String message, UnitDTO unit, ProgressBar progressBar, Button btnLed) {
		session = new Session(parentActivity);
		DatabaseHandler db = new DatabaseHandler(parentActivity);
		String setStatus = "false";
		progressBar.setVisibility(View.GONE);
		Log.e("TEST", unit.getId().toString());
		if(success && message.equals("200")){
			if(db.keyExists(unit.getId().toString())){
				db.updateStatus(new UnitStatusLedSQL(unit.getId().toString(), "true"));
			}else{
				db.addStatus(new UnitStatusLedSQL(unit.getId().toString(), "true"));
			}
			setStatus = "true";
		}else if(!message.equals("501") && !message.equals("503")) {
			if(!session.getBooleanValue(Session.FIRST_LOAD))
				Toast.makeText(parentActivity, message, Toast.LENGTH_LONG).show();
			if(db.keyExists(unit.getId().toString()))
				setStatus = db.getStatus(unit.getId().toString()).getStatus();
		}else{
			if(db.keyExists(unit.getId().toString())){
				db.updateStatus(new UnitStatusLedSQL(unit.getId().toString(), "false"));
			}else{
				db.addStatus(new UnitStatusLedSQL(unit.getId().toString(), "false"));
			}
			setStatus = "false";
		}

		if(setStatus.equals("true")){
			btnLed.setTextColor(parentActivity.getResources().getColor(android.R.color.white));
			btnLed.setBackgroundResource(R.drawable.led_online_button);
			btnLed.setText(R.string.online_led);
		}else{
			btnLed.setTextColor(parentActivity.getResources().getColor(android.R.color.black));
			btnLed.setBackgroundResource(R.drawable.led_offline_button);
			btnLed.setText(R.string.offline_led);
		}
		btnLed.setVisibility(View.VISIBLE);
	}
}
