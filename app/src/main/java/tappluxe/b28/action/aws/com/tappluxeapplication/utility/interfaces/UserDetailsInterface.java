package tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces;

import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.UserDTO;

/**
 * Created by jay.bilocura on 3/25/2017.
 */

public interface UserDetailsInterface {
    void userDetails(UserDTO user);
}
