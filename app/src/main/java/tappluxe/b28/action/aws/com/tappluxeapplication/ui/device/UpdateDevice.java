package tappluxe.b28.action.aws.com.tappluxeapplication.ui.device;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import tappluxe.b28.action.aws.com.tappluxeapplication.R;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.adapter.UnitSpinnerAdapter;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.FormInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.ListDeviceInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.ListUnitInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces.SimpleCallerUserInterface;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.DeviceDTO;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.UnitDTO;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.service.DeviceService;
import tappluxe.b28.action.aws.com.tappluxeapplication.web.service.UnitService;

public class UpdateDevice extends AppCompatActivity implements FormInterface, ListDeviceInterface,
		ListUnitInterface, SimpleCallerUserInterface{
	@BindView(R.id.spnr_select_unit_device)
	public Spinner spnrSelectUnitDevice;

	@BindView(R.id.spnr_update_socket_number)
	public Spinner spnrUpdateSocketNumber;

	@BindView(R.id.et_update_device_name)
	public EditText etUpdateDeviceName;

    @BindView(R.id.til_update_device_name)
    public TextInputLayout tilUpdateDeviceName;

	@BindView(R.id.spnr_choose_schedule)
	public Spinner spnr_choose_schedule;


	@BindView(R.id.btn_update_device)
	public Button btnUpdateDevice;

	private DeviceService deviceService;
	private UnitService unitService;
	private UnitDTO unit;
	private UnitDTO currentUnit;
	private DeviceDTO device;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.device_update_activity);
		setContentView(R.layout.device_update_activity);
		ButterKnife.bind(this);
		enableForm(true);
		deviceService = new DeviceService(this);
		unitService = new UnitService(this);

		unit = (UnitDTO) getIntent().getSerializableExtra("unit");
		currentUnit = unit;
		device = (DeviceDTO) getIntent().getSerializableExtra("device");
		if(unit == null){
			Toast.makeText(this, "Unit is null", Toast.LENGTH_LONG).show();
		}
		else if (device == null){
			Toast.makeText(this, "device is null", Toast.LENGTH_LONG).show();
		}
		else{
			setTitle(unit.getName());
			unitService.getUnitList(this);
			etUpdateDeviceName.setText(device.getName());
		}
		initNotificationSpinner();
	}

	public void initNotificationSpinner() {
		ArrayList<String> dummySched = new ArrayList<>();
		dummySched.add("Hello");
		dummySched.add("AWAY");
		dummySched.add("Coffee");
		List<String> spinnerArray = dummySched;
		ArrayAdapter<String> adapter = new ArrayAdapter<>(
				this, R.layout.schedule_notification_spinner_item, spinnerArray);
		adapter.setDropDownViewResource(R.layout.schedule_notification_spinner_item);
		Spinner sItems = (Spinner) findViewById(R.id.spnr_choose_schedule);
		sItems.setAdapter(adapter);
		sItems.setSelection(spinnerArray.size() - 1);
	}

    @OnTextChanged(R.id.et_update_device_name)
    public void deviceNameTextChanged() {
        tilUpdateDeviceName.setErrorEnabled(false);
    }

	@OnClick(R.id.btn_update_device)
	public void onSave(){
		enableForm(false);
        String deviceName = etUpdateDeviceName.getText().toString();

        if (isValidDeviceName(deviceName)) {

//			updateDevice(Long id, String deviceName, Long scheduleId, final SimpleCallerUserInterface activity)

            deviceService.updateDevice(device.getId(), device.getName(), this);
        } else {
            enableForm(true);
        }
	}

	@OnClick(R.id.btn_editdevice_cancel)
	public void onCancel(){
		finish();
	}

	@Override
	public void clearForm() {
	}

	@Override
	public void enableForm(boolean enabled) {
		spnrUpdateSocketNumber.setEnabled(enabled);
		spnrSelectUnitDevice.setEnabled(enabled);
		etUpdateDeviceName.setEnabled(enabled);
		btnUpdateDevice.setEnabled(enabled);
	}

	@Override
	public void setListDevice(List<DeviceDTO> devices) {
		ArrayAdapter<Integer> socketAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item);
		for(int i = 0, i2 = 0;i<currentUnit.getSockets().size(); i++){
			if(i2 < devices.size()){
				if(devices.get(i2).getUserUnitSocketId() == i+1){
					if(unit.getId() == currentUnit.getId() && device.getUserUnitSocketId() == i+1)
						socketAdapter.add(i+1);
					i2++;
				}
				else {
					socketAdapter.add(i+1);
				}
			}
			else{
				socketAdapter.add(i+1);
			}
		}
		spnrUpdateSocketNumber.setAdapter(socketAdapter);
		int spinnerPosition = socketAdapter.getPosition((int) (long) device.getUserUnitSocketId());
		spnrUpdateSocketNumber.setSelection(spinnerPosition);
		enableForm(true);
	}

	@Override
	public void setListUnit(List<UnitDTO> unit) {
		UnitSpinnerAdapter unitAdapter = new UnitSpinnerAdapter(unit, this);
		spnrSelectUnitDevice.setAdapter(unitAdapter);
		for(int i = 0; i < unit.size();i++){
			if(unit.get(i).getId() == currentUnit.getId()){
				spnrSelectUnitDevice.setSelection(i);
				break;
			}
		}
//		deviceService.getDeviceListSpecific(currentUnit.getId(), this);
		spnrSelectUnitDevice.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				onUnitChange();
			}
			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}
		});
	}

	public void onUnitChange(){
		UnitDTO selected = (UnitDTO) spnrSelectUnitDevice.getSelectedItem();
		if(currentUnit.getId() != selected.getId()){
			enableForm(false);
			currentUnit = selected;
//			deviceService.getDeviceListSpecific(currentUnit.getId(), this);
		}
	}

	@Override
	public void getError(String message) {
		Toast.makeText(this, message, Toast.LENGTH_LONG).show();
	}

	@Override
	public void requestStatus(boolean success, String message) {
		Toast.makeText(this, message, Toast.LENGTH_LONG).show();
		if(success){
			finish();
		}
        enableForm(true);
	}

    public boolean isValidDeviceName(String deviceName) {
        if(deviceName.isEmpty()) {
            tilUpdateDeviceName.setErrorEnabled(true);
            tilUpdateDeviceName.setError(getText(R.string.empty_device_name_message));
            return false;
        }
        return true;
    }

}
