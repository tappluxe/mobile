package tappluxe.b28.action.aws.com.tappluxeapplication.utility.interfaces;

import android.view.View;

import tappluxe.b28.action.aws.com.tappluxeapplication.web.dto.DeviceDTO;

/**
 * Created by ariel.fabilena on 2/21/2017.
 */

public interface GetDeviceInterface {
	void setDevice(String message, DeviceDTO device, View socketView);
	void setError(String message);
}
